import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'jadwalseat': {
      return [
        body('seat_id').not().isEmpty().withMessage('Jadwal Harus Diisi'),
        body('kode_seat').not().isEmpty().withMessage('Kode Jadwal Harus Diisi'),
        body('dari_jam').not().isEmpty().withMessage('Jam Mulai Harus Diisi'),
        body('sampai_jam').not().isEmpty().withMessage('Jam Berakhir Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
