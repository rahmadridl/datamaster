import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findClinicById } from '../clinic/clinic_repository.js';
import { findUserById } from '../user/user_repository.js';
import { Op } from 'sequelize';
import {
  createJadwalseat,
  findListJadwalseat,
  findOneJadwalseat,
  findJadwalseatById,
  updateJadwalseat,
  deleteJadwalseat,
  findListJadwalseattrue,
  findListJadwalseatnot
} from './jadwalseat_repository.js';

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const clinic = req.query.clinic || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (clinic) requirement.clinic = clinic;

    let jadwalseat = await findListJadwalseattrue(requirement, page, limit);
    // let jadwalseat = await findListJadwalseat(requirement);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(jadwalseat.count / limit),
      total_data: jadwalseat.count,
    };

    return ResponseHelper(res, 200, 'Data Jadwal Seat Berhasil Ditampilkan', jadwalseat.rows, meta);
    // return ResponseHelper(res, 200, 'Data Jadwal Seat Berhasil Ditampilkan', jadwalseat.rows);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Seat Gagal Ditampilkan', error.message);
  }
};

const getnot = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const clinic = req.query.clinic || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (clinic) requirement.clinic = clinic;

    let jadwalseat = await findListJadwalseatnot(requirement, page, limit);
    // let jadwalseat = await findListJadwalseat(requirement);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(jadwalseat.count / limit),
      total_data: jadwalseat.count,
    };

    return ResponseHelper(res, 200, 'Data Jadwal Seat Berhasil Ditampilkan', jadwalseat.rows, meta);
    // return ResponseHelper(res, 200, 'Data Jadwal Seat Berhasil Ditampilkan', jadwalseat.rows);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Seat Gagal Ditampilkan', error.message);
  }
};

const get2 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let jadwalseat = await findListJadwalseat(requirement, page, limit);
    // let jadwalseat = await findListJadwalseat(requirement);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(jadwalseat.count / limit),
      total_data: jadwalseat.count,
    };

    return ResponseHelper(res, 200, 'Data Jadwal Seat Berhasil Ditampilkan', jadwalseat.rows, meta);
    // return ResponseHelper(res, 200, 'Data Jadwal Seat Berhasil Ditampilkan', jadwalseat.rows);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Seat Gagal Ditampilkan', error.message);
  }
};

const find = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    // let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    // const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let jadwalseat = await findJadwalseatById(id);
    if (jadwalseat == null) {
      return ResponseHelper(res, 409, 'Data Jadwal Seat Tidak Tersedia', [
        { message: 'Data Jadwal Seat Tidak Tersedia', param: 'id' },
      ]);
    }
    // let jadwalseat = await findListJadwalseat(requirement);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(jadwalseat.count / limit),
      total_data: jadwalseat.count,
    };

    return ResponseHelper(res, 200, 'Data Jadwal Seat Berhasil Ditampilkan', jadwalseat, meta);
    // return ResponseHelper(res, 200, 'Data Jadwal Seat Berhasil Ditampilkan', jadwalseat.rows);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Seat Gagal Ditampilkan', error.message);
  }
};

const findkode = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    // let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    // const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let jadwalseat = await findOneJadwalseat({where :{kode_seat:id}});
    if (jadwalseat == null) {
      return ResponseHelper(res, 409, 'Data Jadwal Seat Tidak Tersedia', [
        { message: 'Data Jadwal Seat Tidak Tersedia', param: 'id' },
      ]);
    }
    // let jadwalseat = await findListJadwalseat(requirement);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(jadwalseat.count / limit),
      total_data: jadwalseat.count,
    };

    return ResponseHelper(res, 200, 'Data Jadwal Seat Berhasil Ditemukan', jadwalseat, meta);
    // return ResponseHelper(res, 200, 'Data Jadwal Seat Berhasil Ditampilkan', jadwalseat.rows);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Seat Gagal Ditemukan', error.message);
  }
};

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    let clin = await findClinicById(user.clinic_id)
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = clin.clinic_code
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let jadwalseat = await findListJadwalseat(requirement, page, limit);
    // let jadwalseat = await findListJadwalseat(requirement);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(jadwalseat.count / limit),
      total_data: jadwalseat.count,
    };

    return ResponseHelper(res, 200, 'Data Jadwal Seat Berhasil Ditampilkan', limit=="all"?jadwalseat:jadwalseat.rows, meta);
    // return ResponseHelper(res, 200, 'Data Jadwal Seat Berhasil Ditampilkan', jadwalseat.rows);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Seat Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);
    const clin = await findClinicById(user.clinic_id)

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    if (req.body.seat_id == null || req.body.seat_id == '' || req.body.seat_id == '-') {
      return ResponseHelper(res, 422, 'Data Jadwal Seat Harus Diisi', [
        { message: 'Data Jadwal Seat Harus Diisi', param: 'seat_id' },
      ]);
    }
    if (req.body.kode_seat == null || req.body.kode_seat == '' || req.body.kode_seat == '-') {
      return ResponseHelper(res, 422, 'Kode Jadwal Seat Harus Diisi', [
        { message: 'Kode Jadwal Seat Harus Diisi', param: 'kode_seat' },
      ]);
    }

    // Check jadwalseat is exist
    let checkJadwalseat = await findOneJadwalseat({
      where:{kode_seat : req.body.kode_seat, clinic_code:clin.clinic_code}
    });
    if (checkJadwalseat) {
      return ResponseHelper(res, 409, 'Kode Jadwal Seat Sudah Terdaftar', [
        { message: 'Kode Jadwal Seat Sudah Terdaftar', param: 'kode_seat' },
      ]);
    }

    const jadwalseat = await createJadwalseat({ ...req.body });
    await updateJadwalseat(
      {clinic_code:clin.clinic_code},
      {where: {id: jadwalseat.id}}
    )
    return ResponseHelper(res, 201, 'Data Jadwal Seat Berhasil Ditambahkan', jadwalseat);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Seat Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;
    let user = await findUserById(user_id);
    const clin = await findClinicById(user.clinic_id)

    // Check jadwalseat is exist
    let checkJadwalseat = await findJadwalseatById(id);
    if (!checkJadwalseat) {
      return ResponseHelper(res, 409, 'Data Jadwal Seat Tidak Tersedia', [
        { message: 'Data Jadwal Seat Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // if (req.body.status == null || req.body.status == '' || req.body.status == '-') {
    //   return ResponseHelper(res, 422, 'status Harus Diisi', [
    //     { message: 'status Harus Diisi', param: 'status' },
    //   ]);
    // }
    // if (req.body.seat_id == null || req.body.seat_id == '' || req.body.seat_id == '-') {
    //   return ResponseHelper(res, 422, 'seat_id Harus Diisi', [
    //     { message: 'seat_id Harus Diisi', param: 'seat_id' },
    //   ]);
    // }
    // if (req.body.kode_seat == null ||req.body.kode_seat == '' ||req.body.kode_seat == '-') {
    //   return ResponseHelper(res, 422, 'kode_seat Harus Diisi', [
    //     { message: 'kode_seat Harus Diisi', param: 'kode_seat' },
    //   ]);
    // }

    // Check jadwal seat is exist
    let checkJadwalseatcode = await findOneJadwalseat({
      where:{kode_seat : req.body.kode_seat, clinic_code:clin.clinic_code,
             id:{[Op.not]: id}}
    });
    if (checkJadwalseatcode) {
      return ResponseHelper(res, 409, 'Kode Jadwal Seat Tidak Terdaftar', [
        { message: 'Kode Jadwal Seat Tidak Terdaftar', param: 'kode_seat' },
      ]);
    }

    // Update jadwalseat
    await updateJadwalseat({ ...req.body }, { where: { id } });

    const result = await findJadwalseatById(id);

    return ResponseHelper(res, 201, 'Data Jadwal Seat Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Seat Gagal Diubah', error.message);
  }
};

const updatenotoken = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;
    let user = await findUserById(user_id);
    const clin = await findClinicById(user.clinic_id)

    // Check jadwalseat is exist
    let checkJadwalseat = await findJadwalseatById(id);
    if (!checkJadwalseat) {
      return ResponseHelper(res, 409, 'Data Jadwal Seat Tidak Tersedia', [
        { message: 'Data Jadwal Seat Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin

    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    // if (req.body.status == null || req.body.status == '' || req.body.status == '-') {
    //   return ResponseHelper(res, 422, 'status Harus Diisi', [
    //     { message: 'status Harus Diisi', param: 'status' },
    //   ]);
    // }
    // if (req.body.seat_id == null || req.body.seat_id == '' || req.body.seat_id == '-') {
    //   return ResponseHelper(res, 422, 'seat_id Harus Diisi', [
    //     { message: 'seat_id Harus Diisi', param: 'seat_id' },
    //   ]);
    // }
    // if (req.body.kode_seat == null ||req.body.kode_seat == '' ||req.body.kode_seat == '-') {
    //   return ResponseHelper(res, 422, 'kode_seat Harus Diisi', [
    //     { message: 'kode_seat Harus Diisi', param: 'kode_seat' },
    //   ]);
    // }

    // Check jadwal seat is exist
    let checkJadwalseatcode = await findOneJadwalseat({
      where:{kode_seat : req.body.kode_seat, clinic_code:clin.clinic_code,
             id:{[Op.not]: id}}
    });
    if (checkJadwalseatcode) {
      return ResponseHelper(res, 409, 'Kode Jadwal Seat Sudah Terdaftar', [
        { message: 'Kode Jadwal Seat Sudah Terdaftar', param: 'kode_seat' },
      ]);
    }

    // Update jadwalseat
    await updateJadwalseat({ ...req.body }, { where: { id } });

    const result = await findJadwalseatById(id);

    return ResponseHelper(res, 201, 'Data Jadwal Seat Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Seat Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check jadwal seat Exist Or Not
    let checkJadwalseat = await findJadwalseatById(id);
    if (!checkJadwalseat) {
      return ResponseHelper(res, 409, 'Data Jadwal Seat Tidak Tersedia', [
        { message: 'Data Jadwal Seat Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete jadwalseat
    let users = await findUserById(userId);

    await updateJadwalseat({ deletedBy: users }, { where: { id } });

    await deleteJadwalseat(id);

    return ResponseHelper(res, 201, 'Data Jadwal Seat Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Seat Gagal Dihapus', error.message);
  }
};

export { get, get1, get2, getnot, find, findkode, add, update, updatenotoken, deleted };
