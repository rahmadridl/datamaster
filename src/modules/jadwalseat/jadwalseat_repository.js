import db from '../../database/models/index.js';
const { Jadwalseat } = db;
import { Op } from 'sequelize';

// Find one jadwalseat by id
const findJadwalseatById = async (id) => {
  try {
    let result = await Jadwalseat.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findJadwalseatById', error);
    throw new Error(error);
  }
};

const findAllJadwalseat = async () => {
  try {
    let result = await Jadwalseat.findAll();
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findAllJadwalseat', error);
    throw new Error(error);
  }
};

// Find one jadwalseat by filter
const findOneJadwalseat = async (filter) => {
  try {
    let result = await Jadwalseat.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneJadwalseat', error);
    throw new Error(error);
  }
};

// Find list jadwalseat
const findListJadwalseat = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Jadwalseat.findAll({
        where: {
          kode_seat: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_code: status,
          status: true,
        },
        order: [['id', 'ASC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Jadwalseat.findAndCountAll({
        where: {
          kode_seat: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_code: status,
        },
        order: [['id', 'ASC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListJadwalseat', error);
    throw new Error(error);
  }
};

// Find list jadwalseat
const findListJadwalseattrue = async ({ search, clinic }, page, limit) => {
  try {
    let result;
    result = await Jadwalseat.findAndCountAll({
      where: {
        kode_seat: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        clinic_code: clinic ? { [Op.like]: `%${clinic}%` } : { [Op.like]: `%%` },
        status: true
      },
      order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
  });
  return result;
  } catch (error) {
    console.error('[EXCEPTION] findListJadwalseat', error);
    throw new Error(error);
  }
};

// Find list jadwalseat
const findListJadwalseatnot = async ({ search, clinic }, page, limit) => {
  try {
    let result;
    if (search) {
      result = await Jadwalseat.findAndCountAll({
        where: {
          id:{[Op.notIn]: search.split(',')} ,
          clinic_code: clinic ? { [Op.like]: `%${clinic}%` } : { [Op.like]: `%%` },
          status: true
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }else if(!search){
      result = await Jadwalseat.findAndCountAll({
        where: {
          clinic_code: clinic ? { [Op.like]: `%${clinic}%` } : { [Op.like]: `%%` },
          status: true
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
  return result;
  } catch (error) {
    console.error('[EXCEPTION] findListJadwalseat', error);
    throw new Error(error);
  }
};

// Create new jadwalseat
const createJadwalseat = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Jadwalseat.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createJadwalseat', error);
    throw new Error(error);
  }
};

// Update jadwalseat
const updateJadwalseat = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Jadwalseat.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateJadwalseat', error);
    throw new Error(error);
  }
};

//delete jadwalseat
const deleteJadwalseat = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Jadwalseat.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteJadwalseat', error);
    throw new Error(error);
  }
};

export {
  findJadwalseatById,
  findAllJadwalseat,
  findOneJadwalseat,
  findListJadwalseat,
  createJadwalseat,
  updateJadwalseat,
  deleteJadwalseat,
  findListJadwalseattrue,
  findListJadwalseatnot
};
