import { get, get1, add, update, deleted, find, findkode, get2, getnot, updatenotoken } from './jadwalseat_controller.js';
import validator from './jadwalseat_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const JadwalseatRoutes = (app, prefix) => {
  app.route(`${prefix}/jadwalseat`).get( AuthMiddleware, get);
  app.route(`${prefix}/jadwalseatnotoken`).get( get1);
  app.route(`${prefix}/jadwalseatnotnotoken`).get( getnot);
  app.route(`${prefix}/jadwalseatnotokenku`).get( get2);
  app.route(`${prefix}/jadwalseatnotokenbaru`).get( get);
  app.route(`${prefix}/jadwalseatidnotoken/:id`).get( find);
  app.route(`${prefix}/jadwalseatkodenotoken/:id`).get( findkode);
  app.route(`${prefix}/jadwalseat/add`).post(AuthMiddleware, validator('jadwalseat'), add);
  app.route(`${prefix}/jadwalseat/update/:id`).patch(AuthMiddleware, validator('jadwalseat'), update);
  app.route(`${prefix}/jadwalseatnotoken/update/:id`).patch(updatenotoken);
  app.route(`${prefix}/jadwalseat/delete/:id`).delete(AuthMiddleware, deleted);
};

export { JadwalseatRoutes };
