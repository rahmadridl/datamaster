import { validationResult } from 'express-validator';
import { Op } from 'sequelize';
import ResponseHelper from '../../helpers/response_helper.js';
import { findOneClinic } from '../clinic/clinic_repository.js';
import { findUserById } from '../user/user_repository.js';
import {
  createDiagnosis,
  findListDiagnosis,
  findOneDiagnosis,
  findDiagnosisById,
  updateDiagnosis,
  deleteDiagnosis
} from './diagnosis_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let diagnosis = await findListDiagnosis(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(diagnosis.count / limit),
      total_data: diagnosis.count,
    };

    return ResponseHelper(res, 200, 'Data Diagnosis Berhasil Ditampilkan', limit=="all"?diagnosis:diagnosis.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diagnosis Gagal Ditampilkan', error.message);
  }
};

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    // let user = await findUserById(user_id);
    // if (Number(user.role) > 3) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || 'RS10'
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';
    const clinic = await findOneClinic({where:{clinic_code:status}})
    var statusid = clinic.id

    let requirement = {};
    if (search) requirement.search = search;
    if (statusid) requirement.status = statusid;

    let diagnosis = await findListDiagnosis(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(diagnosis.count / limit),
      total_data: diagnosis.count,
    };

    return ResponseHelper(res, 200, 'Data Diagnosis Berhasil Ditampilkan', limit=="all"?diagnosis:diagnosis.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diagnosis Gagal Ditampilkan', error.message);
  }
};

const findid = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    // let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    // const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    // if (status) requirement.status = status;

    // let diagnosis = await findListDiagnosis(requirement, page, limit);
    let diagnosis = await findDiagnosisById(id)
    console.log(diagnosis);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(diagnosis.count / limit),
      total_data: diagnosis.count,
    };

    return ResponseHelper(res, 200, 'Data Diagnosis Berhasil Ditampilkan', limit=="all"?diagnosis:diagnosis, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diagnosis Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check nama_diagnosis is exist
    let checkDiagnosis = await findOneDiagnosis({
      where:{nama_diagnosis : req.body.nama_diagnosis, clinic_id:user.clinic_id}
    });
    if (checkDiagnosis) {
      return ResponseHelper(res, 409, 'Nama Diagnosis Sudah Terdaftar', [
        { message: 'Nama Diagnosis Sudah Terdaftar', param: 'nama_diagnosis' },
      ]);
    }
    // Check kode_diagnosis is exist
    let checkDiagnosiskode = await findOneDiagnosis({
      where:{kode_diagnosis : req.body.kode_diagnosis, clinic_id:user.clinic_id}
    });
    if (checkDiagnosiskode) {
      return ResponseHelper(res, 409, 'Kode Diagnosis Sudah Terdaftar', [
        { message: 'Kode Diagnosis Sudah Terdaftar', param: 'kode_diagnosis' },
      ]);
    }

    const diagnosis = await createDiagnosis({ ...req.body });
    await updateDiagnosis(
      {clinic_id:user.clinic_id},
      {where: {id: diagnosis.id}}
    )
    return ResponseHelper(res, 201, 'Data Diagnosis Berhasil Ditambahkan', diagnosis);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diagnosis Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check diagnosis is exist
    let checkDiagnosis = await findDiagnosisById(id);
    if (!checkDiagnosis) {
      return ResponseHelper(res, 409, 'Data Diagnosis Tidak Tersedia', [
        { message: 'Data Diagnosis Tidak Tersedia', param: 'id' },
      ]);
    }
    // Check nama_diagnosis is exist
    let checkDiagnosisnama = await findOneDiagnosis({
      where:{nama_diagnosis : req.body.nama_diagnosis,clinic_id:user.clinic_id,id:{[Op.not]: id}}
    });
    if (checkDiagnosisnama) {
      return ResponseHelper(res, 409, 'Nama Diagnosis Sudah Terdaftar', [
        { message: 'Nama Diagnosis Sudah Terdaftar', param: 'nama_diagnosis' },
      ]);
    }
    // Check kode_diagnosis is exist
    let checkDiagnosiskode = await findOneDiagnosis({
      where:{kode_diagnosis : req.body.kode_diagnosis,clinic_id:user.clinic_id,id:{[Op.not]: id}}
    });
    if (checkDiagnosiskode) {
      return ResponseHelper(res, 409, 'Kode Diagnosis Sudah Terdaftar', [
        { message: 'Kode Diagnosis Sudah Terdaftar', param: 'kode_diagnosis' },
      ]);
    }

    // Update diagnosis
    await updateDiagnosis({ ...req.body }, { where: { id } });

    const result = await findDiagnosisById(id);

    return ResponseHelper(res, 201, 'Data Diagnosis Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diagnosis Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check diagnosis Exist Or Not
    let checkDiagnosis = await findDiagnosisById(id);
    if (!checkDiagnosis) {
      return ResponseHelper(res, 409, 'Data Diagnosis Tidak Tersedia', [
        { message: 'Data Diagnosis Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete diagnosis
    let users = await findUserById(userId);

    await updateDiagnosis({ deletedBy: users }, { where: { id } });

    await deleteDiagnosis(id);

    return ResponseHelper(res, 201, 'Data Diagnosis Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diagnosis Gagal Dihapus', error.message);
  }
};

export { get, get1, findid, add, update, deleted };
