import { get, add, update, deleted, findid, get1 } from './diagnosis_controller.js';
import validator from './diagnosis_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const DiagnosisRoutes = (app, prefix) => {
  app.route(`${prefix}/diagnosis`).get(AuthMiddleware, get);
  app.route(`${prefix}/diagnosisnotoken`).get(get1);
  app.route(`${prefix}/diagnosisidnotoken/:id`).get(findid);
  app.route(`${prefix}/diagnosis/add`).post(AuthMiddleware, validator('diagnosis'), add);
  app.route(`${prefix}/diagnosis/update/:id`).patch(AuthMiddleware, validator('diagnosis'), update);
  app.route(`${prefix}/diagnosis/delete/:id`).delete(AuthMiddleware, deleted);
};

export { DiagnosisRoutes };
