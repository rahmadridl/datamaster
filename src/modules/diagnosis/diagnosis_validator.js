import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'diagnosis': {
      return [
        body('nama_diagnosis').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('kode_diagnosis').not().isEmpty().withMessage('Kode Harus Diisi'),
        body('deskripsi').not().isEmpty().withMessage('Deskripsi Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
