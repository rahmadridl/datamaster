import db from '../../database/models/index.js';
const { Diagnosis } = db;
import { Op } from 'sequelize';

// Find one diagnosis by id
const findDiagnosisById = async (id) => {
  try {
    let result = await Diagnosis.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findDiagnosisById', error);
    throw new Error(error);
  }
};

// Find one diagnosis by filter
const findOneDiagnosis = async (filter) => {
  try {
    let result = await Diagnosis.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneDiagnosis', error);
    throw new Error(error);
  }
};

// Find list diagnosis
const findListDiagnosis = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Diagnosis.findAll({
        where: {
          nama_diagnosis: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Diagnosis.findAndCountAll({
        where: {
          nama_diagnosis: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListDiagnosis', error);
    throw new Error(error);
  }
};

// Create new Diagnosis
const createDiagnosis = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Diagnosis.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createDiagnosis', error);
    throw new Error(error);
  }
};

// Update diagnosis
const updateDiagnosis = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Diagnosis.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateDiagnosis', error);
    throw new Error(error);
  }
};

//delete diagnosis
const deleteDiagnosis = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Diagnosis.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteDiagnosis', error);
    throw new Error(error);
  }
};

export {
  findDiagnosisById,
  findOneDiagnosis,
  findListDiagnosis,
  createDiagnosis,
  updateDiagnosis,
  deleteDiagnosis,
};
