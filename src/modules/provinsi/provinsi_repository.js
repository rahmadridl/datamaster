import db from '../../database/models/index.js';
const { Provinsi } = db;
import { Op } from 'sequelize';

// Find one provinsi by id
const findProvinsiById = async (id) => {
  try {
    let result = await Provinsi.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findProvinsiById', error);
    throw new Error(error);
  }
};

// Find one provinsi by filter
const findOneProvinsi = async (filter) => {
  try {
    let result = await Provinsi.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneProvinsi', error);
    throw new Error(error);
  }
};

// Find list provinsi
const findListProvinsi = async ({ search, status }, page, limit) => {
  try {
    let result = await Provinsi.findAndCountAll({
      where: {
        nama: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListProvinsi', error);
    throw new Error(error);
  }
};

// Create new ruangan kategori
// const createProvinsi = async (data, transaction) => {
//   const t = transaction ? transaction : await db.sequelize.transaction();
//   try {
//     let result = await Provinsi.create(data, { transaction });
//     if (!transaction) t.commit();
//     return result;
//   } catch (error) {
//     if (!transaction) t.rollback();
//     console.error('[EXCEPTION] createProvinsi', error);
//     throw new Error(error);
//   }
// };

// Update ruangan kategori
// const updateProvinsi = async (data, filter, transaction) => {
//   const t = transaction ? transaction : await db.sequelize.transaction();
//   try {
//     let result = await Provinsi.update(data, { ...filter, transaction });
//     if (!transaction) t.commit();
//     return result;
//   } catch (error) {
//     if (!transaction) t.rollback();
//     console.error('[EXCEPTION] updateProvinsi', error);
//     throw new Error(error);
//   }
// };

//delete ruangan kategori
// const deleteProvinsi = async (productId, transaction) => {
//   const t = transaction ? transaction : await db.sequelize.transaction();
//   try {
//     let result = await Provinsi.destroy({ where: { id: productId }, transaction });
//     if (!transaction) t.commit();
//     return result;
//   } catch (error) {
//     if (!transaction) t.rollback();
//     console.error('[EXCEPTION] deleteProvinsi', error);
//     throw new Error(error);
//   }
// };

export { findProvinsiById, findOneProvinsi, findListProvinsi, 
  // createProvinsi, 
  // updateProvinsi, 
  // deleteProvinsi
 };
