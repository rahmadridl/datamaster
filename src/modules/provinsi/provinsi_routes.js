import { get, add, update, deleted } from './provinsi_controller.js';
import validator from './provinsi_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const ProvinsiRoutes = (app, prefix) => {
  app.route(`${prefix}/provinsi`).get(AuthMiddleware, get);
  // app.route(`${prefix}/provinsi/add`).post(AuthMiddleware, validator('provinsi'), add);
  // app.route(`${prefix}/provinsi/update/:id`).patch(AuthMiddleware, validator('provinsi'), update);
  // app.route(`${prefix}/provinsi/delete/:id`).delete(AuthMiddleware, deleted);
};

export { ProvinsiRoutes };
