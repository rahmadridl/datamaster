import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'provinsi': {
      return [
        body('nama_kategori').not().isEmpty().withMessage('name can not be empty'),
        body('clinic_id').not().isEmpty().withMessage('clinic id can not be empty'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
