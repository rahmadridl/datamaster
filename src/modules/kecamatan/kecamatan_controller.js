import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import {
  // createProvinsi,
  findListKecamatan,
  findKecamatan,
  // findProvinsiById,
  // updateProvinsi,
  // deleteProvinsi
} from './kecamatan_repository.js';
import { findKabupatenById } from '../kabupaten/kabupaten_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'not allowed to access', [
        { message: 'not allowed to access', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let kecamatan = await findListKecamatan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(kecamatan.count / limit),
      total_data: kecamatan.count,
    };

    return ResponseHelper(res, 200, 'success get list data kecamatan', kecamatan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed get kecamatan', error.message);
  }
};

const find = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check kabupaten is exist
    let checkKabupaten = await findKabupatenById(id);
    if (!checkKabupaten) {
      return ResponseHelper(res, 409, 'Kabupaten is not exist', [
        { message: 'Kabupaten is not exist', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'not allowed to access', [
        { message: 'not allowed to access', param: 'id' },
      ]);
    }

    // const id = req.params;
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let kecamatan = await findKecamatan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(kecamatan.count / limit),
      total_data: kecamatan.count,
    };

    return ResponseHelper(res, 200, 'success get list data kecamatan', kecamatan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed get kecamatan', error.message);
  }
};

export { get, find };
