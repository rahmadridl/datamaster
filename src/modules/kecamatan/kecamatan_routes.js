import { get, find, add, update, deleted } from './kecamatan_controller.js';
import validator from './kecamatan_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const KecamatanRoutes = (app, prefix) => {
  app.route(`${prefix}/kecamatan`).get(AuthMiddleware, get);
  app.route(`${prefix}/kecamatan/:id`).get(AuthMiddleware, find);
  // app.route(`${prefix}/provinsi/add`).post(AuthMiddleware, validator('provinsi'), add);
  // app.route(`${prefix}/provinsi/update/:id`).patch(AuthMiddleware, validator('provinsi'), update);
  // app.route(`${prefix}/provinsi/delete/:id`).delete(AuthMiddleware, deleted);
};

export { KecamatanRoutes };
