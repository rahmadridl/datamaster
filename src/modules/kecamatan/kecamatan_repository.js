import db from '../../database/models/index.js';
const { Kecamatan } = db;
import { Op } from 'sequelize';

// Find one kecamatan by id
const findKecamatanById = async (id) => {
  try {
    let result = await Kecamatan.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findKecamatanById', error);
    throw new Error(error);
  }
};

// Find one kecamatan by filter
const findOneKecamatan = async (filter) => {
  try {
    let result = await Kecamatan.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneKecamatan', error);
    throw new Error(error);
  }
};

// Find list kecamatan
const findListKecamatan = async ({ search, status }, page, limit) => {
  try {
    let result = await Kecamatan.findAndCountAll({
      where: {
        nama: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListKecamatan', error);
    throw new Error(error);
  }
};

// Find kecamatan
const findKecamatan = async ({ id, status }, page, limit) => {
  try {
    let result = await Kecamatan.findAndCountAll({
      where: {
        kabupaten_id: id ? { [Op.like]: id } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListKecamatan', error);
    throw new Error(error);
  }
};

// Create new ruangan kategori
// const createProvinsi = async (data, transaction) => {
//   const t = transaction ? transaction : await db.sequelize.transaction();
//   try {
//     let result = await Provinsi.create(data, { transaction });
//     if (!transaction) t.commit();
//     return result;
//   } catch (error) {
//     if (!transaction) t.rollback();
//     console.error('[EXCEPTION] createProvinsi', error);
//     throw new Error(error);
//   }
// };

// Update ruangan kategori
// const updateProvinsi = async (data, filter, transaction) => {
//   const t = transaction ? transaction : await db.sequelize.transaction();
//   try {
//     let result = await Provinsi.update(data, { ...filter, transaction });
//     if (!transaction) t.commit();
//     return result;
//   } catch (error) {
//     if (!transaction) t.rollback();
//     console.error('[EXCEPTION] updateProvinsi', error);
//     throw new Error(error);
//   }
// };

//delete ruangan kategori
// const deleteProvinsi = async (productId, transaction) => {
//   const t = transaction ? transaction : await db.sequelize.transaction();
//   try {
//     let result = await Provinsi.destroy({ where: { id: productId }, transaction });
//     if (!transaction) t.commit();
//     return result;
//   } catch (error) {
//     if (!transaction) t.rollback();
//     console.error('[EXCEPTION] deleteProvinsi', error);
//     throw new Error(error);
//   }
// };

export { findKecamatanById, findKecamatan, findOneKecamatan, findListKecamatan, 
  // createProvinsi, 
  // updateProvinsi, 
  // deleteProvinsi
 };
