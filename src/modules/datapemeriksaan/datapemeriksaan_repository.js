import db from '../../database/models/index.js';
const { Datapemeriksaan } = db;
import { Op } from 'sequelize';

// Find one Data pemeriksaan by id
const findDatapemeriksaanById = async (id) => {
  try {
    let result = await Datapemeriksaan.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findDatapemeriksaanById', error);
    throw new Error(error);
  }
};

// Find one Data pemeriksaan by filter
const findOneDatapemeriksaan = async (filter) => {
  try {
    let result = await Datapemeriksaan.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneDatapemeriksaan', error);
    throw new Error(error);
  }
};

// Find list Data pemeriksaan
const findListDatapemeriksaan = async ({ search, status }, page, limit) => {
  try {
    let result = await Datapemeriksaan.findAndCountAll({
      where: {
        nama_pasien: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListDatapemeriksaan', error);
    throw new Error(error);
  }
};

// Create new Data pemeriksaan
const createDatapemeriksaan = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Datapemeriksaan.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createDatapemeriksaan', error);
    throw new Error(error);
  }
};

// Update Data pemeriksaan
const updateDatapemeriksaan = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Datapemeriksaan.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateDatapemeriksaan', error);
    throw new Error(error);
  }
};

//delete Data pemeriksaan
const deleteDatapemeriksaan = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Datapemeriksaan.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteDatapemeriksaan', error);
    throw new Error(error);
  }
};

export { findDatapemeriksaanById, findOneDatapemeriksaan, findListDatapemeriksaan, createDatapemeriksaan, updateDatapemeriksaan, deleteDatapemeriksaan };
