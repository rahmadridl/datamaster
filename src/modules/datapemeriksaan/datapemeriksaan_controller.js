import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import {
  createDatapemeriksaan,
  findListDatapemeriksaan,
  findDatapemeriksaanById,
  findOneDatapemeriksaan,
  updateDatapemeriksaan,
  deleteDatapemeriksaan
} from './datapemeriksaan_repository.js';
import { findOnePasien } from '../pasien/pasien_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let datapemeriksaan = await findListDatapemeriksaan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(datapemeriksaan.count / limit),
      total_data: datapemeriksaan.count,
    };

    return ResponseHelper(res, 200, 'Data Pemeriksaan Berhasil Ditampilkan', datapemeriksaan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  const {
      nomor_rm
    } = req.body;
  let check_nomor_rm = await findOnePasien({
    where: { nomor_rm },
  });
  if (check_nomor_rm) {
    return ResponseHelper(res, 409, 'Nomor Rekam Medis Sudah Terdaftar', [
      { message: 'Nomor Rekam Medis Sudah Terdaftar', param: 'nomor_rm' },
    ]);
  }
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const datapemeriksaan = await createDatapemeriksaan({ ...req.body });
    return ResponseHelper(res, 201, 'Data Pemeriksaan Berhasil Ditambahkan', datapemeriksaan);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pemeriksaan Berhasil Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check data pemeriksaan is exist
    let checkDatapemeriksaan = await findDatapemeriksaanById(id);
    if (!checkDatapemeriksaan) {
      return ResponseHelper(res, 409, 'Data Pemeriksaan Tidak Terdaftar', [
        { message: 'Data Pemeriksaan Tidak Terdaftar', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Update data pemeriksaan
    await updateDatapemeriksaan({ ...req.body }, { where: { id } });

    const result = await findDatapemeriksaanById(id);

    return ResponseHelper(res, 201, 'Data Pemeriksaan Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pemeriksaan Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check data pemeriksaan Exist Or Not
    let checkDatapemeriksaan = await findDatapemeriksaanById(id);
    if (!checkDatapemeriksaan) {
      return ResponseHelper(res, 409, 'Data Pemeriksaan Tidak Terdaftar', [
        { message: 'Data Pemeriksaan Tidak Terdaftar', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete data pemeriksaan
    let users = await findUserById(userId);

    await updateDatapemeriksaan({ deletedBy: users }, { where: { id } });

    await deleteDatapemeriksaan(id);

    return ResponseHelper(res, 201, 'Data Pemeriksaan Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pemeriksaan Gagal Dihapus', error.message);
  }
};


export { get, add, update, deleted };
