import { get, add, update, deleted } from './datapemeriksaan_controller.js';
import validator from './datapemeriksaan_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const DatapemeriksaanRoutes = (app, prefix) => {
  app.route(`${prefix}/datapemeriksaan`).get(AuthMiddleware, get);
  app.route(`${prefix}/datapemeriksaan/add`).post(AuthMiddleware, validator('datapemeriksaan'), add);
  app.route(`${prefix}/datapemeriksaan/update/:id`).patch(AuthMiddleware, validator('datapemeriksaan'), update);
  app.route(`${prefix}/datapemeriksaan/delete/:id`).delete(AuthMiddleware, deleted);
};

export { DatapemeriksaanRoutes };
