import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'datapemeriksaan': {
      return [
        body('pasien_id').not().isEmpty().withMessage('Pasien Harus Diisi'),
        body('dokter_id').not().isEmpty().withMessage('Dokter Harus Diisi'),
        body('no_test').not().isEmpty().withMessage('No Test Harus Diisi'),
        body('clinic_id').not().isEmpty().withMessage('Klinik Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
