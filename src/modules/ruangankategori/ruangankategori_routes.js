import { get, add, update, deleted } from './ruangankategori_controller.js';
import validator from './ruangankategori_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const RuangankategoriRoutes = (app, prefix) => {
  app.route(`${prefix}/ruangankategori`).get(AuthMiddleware, get);
  app.route(`${prefix}/ruangankategori/add`).post(AuthMiddleware, validator('ruangankategori'), add);
  app.route(`${prefix}/ruangankategori/update/:id`).patch(AuthMiddleware, validator('ruangankategori'), update);
  app.route(`${prefix}/ruangankategori/delete/:id`).delete(AuthMiddleware, deleted);
};

export { RuangankategoriRoutes };
