import db from '../../database/models/index.js';
const { Ruangankategori } = db;
import { Op } from 'sequelize';

// Find one ruangan kategori by id
const findRuangankategoriById = async (id) => {
  try {
    let result = await Ruangankategori.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findRuangankategoriById', error);
    throw new Error(error);
  }
};

// Find one ruangan kategori by filter
const findOneRuangankategori = async (filter) => {
  try {
    let result = await Ruangankategori.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneRuangankategori', error);
    throw new Error(error);
  }
};

// Find list ruangan kategori
const findListRuangankategori = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Ruangankategori.findAll({
        where: {
          nama_kategori: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Ruangankategori.findAndCountAll({
        where: {
          nama_kategori: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListRuangankategori', error);
    throw new Error(error);
  }
};

// Create new ruangan kategori
const createRuangankategori = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Ruangankategori.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createRuangankategori', error);
    throw new Error(error);
  }
};

// Update ruangan kategori
const updateRuangankategori = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Ruangankategori.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateRuangankategori', error);
    throw new Error(error);
  }
};

//delete ruangan kategori
const deleteRuangankategori = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Ruangankategori.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteRuangankategori', error);
    throw new Error(error);
  }
};

export {
  findRuangankategoriById,
  findOneRuangankategori,
  findListRuangankategori,
  createRuangankategori,
  updateRuangankategori,
  deleteRuangankategori,
};
