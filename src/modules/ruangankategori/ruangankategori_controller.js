import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import {
  createRuangankategori,
  findListRuangankategori,
  findRuangankategoriById,
  updateRuangankategori,
  deleteRuangankategori
} from './ruangankategori_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let ruangankategori = await findListRuangankategori(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(ruangankategori.count / limit),
      total_data: ruangankategori.count,
    };

    return ResponseHelper(res, 200, 'Data Kategori Ruangan Berhasil Ditampilkan', limit=="all"?ruangankategori:ruangankategori.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Kategori Ruangan Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const ruangankategori = await createRuangankategori({ ...req.body });
    await updateRuangankategori(
      {clinic_id:user.clinic_id},
      {where: {id: ruangankategori.id}}
    )
    return ResponseHelper(res, 201, 'Data Kategori Ruangan Berhasil Ditambahkan', ruangankategori);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Kategori Ruangan Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check ruangan kategori is exist
    let checkRuangankategori = await findRuangankategoriById(id);
    if (!checkRuangankategori) {
      return ResponseHelper(res, 409, 'Ruangankategori is not exist', [
        { message: 'Ruangankategori is not exist', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Update ruangan kategori
    await updateRuangankategori({ ...req.body }, { where: { id } });

    const result = await findRuangankategoriById(id);

    return ResponseHelper(res, 201, 'Data Kategori Ruangan Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Kategori Ruangan Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check ruangan kategori Exist Or Not
    let checkRuangankategori = await findRuangankategoriById(id);
    if (!checkRuangankategori) {
      return ResponseHelper(res, 409, 'Data Kategori Ruangan Tidak Tersedia', [
        { message: 'Data Kategori Ruangan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete ruangan kategori
    let users = await findUserById(userId);

    await updateRuangankategori({ deletedBy: users }, { where: { id } });

    await deleteRuangankategori(id);

    return ResponseHelper(res, 201, 'Data Kategori Ruangan Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Kategori Ruangan Gagal Dihapus', error.message);
  }
};

export { get, add, update, deleted };
