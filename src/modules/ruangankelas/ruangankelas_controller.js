import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import {
  createRuangankelas,
  findListRuangankelas,
  findRuangankelasById,
  updateRuangankelas,
  deleteRuangankelas
} from './ruangankelas_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let ruangankelas = await findListRuangankelas(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(ruangankelas.count / limit),
      total_data: ruangankelas.count,
    };

    return ResponseHelper(res, 200, 'Data Kelas Ruangan Berhasil Ditampilkan', limit=="all"?ruangankelas:ruangankelas.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Kelas Ruangan Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const ruangankelas = await createRuangankelas({ ...req.body });
    await updateRuangankelas(
      {clinic_id:user.clinic_id},
      {where: {id: ruangankelas.id}}
    )
    return ResponseHelper(res, 201, 'Data Kelas Ruangan Berhasil Ditambahkan', ruangankelas);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Kelas Ruangan Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check Kelas Ruangan is exist
    let checkRuangankelas = await findRuangankelasById(id);
    if (!checkRuangankelas) {
      return ResponseHelper(res, 409, 'Data Kelas Ruangan Tidak Tersedia', [
        { message: 'Data Kelas Ruangan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Update Kelas Ruangan
    await updateRuangankelas({ ...req.body }, { where: { id } });

    const result = await findRuangankelasById(id);

    return ResponseHelper(res, 201, 'Data Kelas Ruangan Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Kelas Ruangan Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check Kelas Ruangan Exist Or Not
    let checkRuangankelas = await findRuangankelasById(id);
    if (!checkRuangankelas) {
      return ResponseHelper(res, 409, 'Data Kelas Ruangan Tidak Tersedia', [
        { message: 'Data Kelas Ruangan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete Kelas Ruangan
    let users = await findUserById(userId);

    await updateRuangankelas({ deletedBy: users }, { where: { id } });

    await deleteRuangankelas(id);

    return ResponseHelper(res, 201, 'Data Kelas Ruangan Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Kelas Ruangan Gagal Dihapus', error.message);
  }
};

export { get, add, update, deleted };
