import { get, add, update, deleted } from './ruangankelas_controller.js';
import validator from './ruangankelas_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const RuangankelasRoutes = (app, prefix) => {
  app.route(`${prefix}/ruangankelas`).get(AuthMiddleware, get);
  app.route(`${prefix}/ruangankelas/add`).post(AuthMiddleware, validator('ruangankelas'), add);
  app.route(`${prefix}/ruangankelas/update/:id`).patch(AuthMiddleware, validator('ruangankelas'), update);
  app.route(`${prefix}/ruangankelas/delete/:id`).delete(AuthMiddleware, deleted);
};

export { RuangankelasRoutes };
