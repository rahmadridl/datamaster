import db from '../../database/models/index.js';
const { Ruangankelas } = db;
import { Op } from 'sequelize';

// Find one ruangan kelas by id
const findRuangankelasById = async (id) => {
  try {
    let result = await Ruangankelas.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findRuangankelasById', error);
    throw new Error(error);
  }
};

// Find one ruangan kelas by filter
const findOneRuangankelas = async (filter) => {
  try {
    let result = await Ruangankelas.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneRuangankelas', error);
    throw new Error(error);
  }
};

// Find list ruangan kelas
const findListRuangankelas = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Ruangankelas.findAll({
        where: {
          nama_kelas: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Ruangankelas.findAndCountAll({
        where: {
          nama_kelas: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListRuangankelas', error);
    throw new Error(error);
  }
};

// Create new ruangan kelas
const createRuangankelas = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Ruangankelas.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createRuangankelas', error);
    throw new Error(error);
  }
};

// Update ruangan kelas
const updateRuangankelas = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Ruangankelas.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateRuangankelas', error);
    throw new Error(error);
  }
};

//delete ruangan kelas
const deleteRuangankelas = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Ruangankelas.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteRuangankelas', error);
    throw new Error(error);
  }
};

export {
  findRuangankelasById,
  findOneRuangankelas,
  findListRuangankelas,
  createRuangankelas,
  updateRuangankelas,
  deleteRuangankelas,
};
