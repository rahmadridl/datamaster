import db from '../../database/models/index.js';
const { Poli } = db;
import { Op } from 'sequelize';

// Find one poli by id
const findPoliById = async (id) => {
  try {
    let result = await Poli.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findPoliById', error);
    throw new Error(error);
  }
};

const findAllPoli = async () => {
  try {
    let result = await Poli.findAll();
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findAllPoli', error);
    throw new Error(error);
  }
};

// Find one poli by filter
const findOnePoli = async (filter) => {
  try {
    let result = await Poli.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOnePoli', error);
    throw new Error(error);
  }
};

// Find list poli
const findListPoli = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Poli.findAll({
        where: {
          nama_poli: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Poli.findAndCountAll({
        where: {
          nama_poli: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPoli', error);
    throw new Error(error);
  }
};

// Create new poli
const createPoli = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Poli.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createPoli', error);
    throw new Error(error);
  }
};

// Update poli
const updatePoli = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Poli.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updatePoli', error);
    throw new Error(error);
  }
};

//delete poli
const deletePoli = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Poli.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deletePoli', error);
    throw new Error(error);
  }
};

export { findPoliById, findAllPoli, findOnePoli, findListPoli, createPoli, updatePoli, deletePoli };
