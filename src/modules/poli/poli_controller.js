import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findSeatById, updateSeat, findOneSeat } from '../seat/seat_repository.js';
import { findJadwalseatById } from '../jadwalseat/jadwalseat_repository.js';
import { findUserById } from '../user/user_repository.js';
import { Op } from 'sequelize';
import {
  createPoli,
  findListPoli,
  findAllPoli,
  findPoliById,
  findOnePoli,
  updatePoli,
  deletePoli
} from './poli_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let poli = await findListPoli(requirement, page, limit);

    const pol = await findAllPoli();
        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(poli.count / limit),
      total_data: poli.count,
    };

    return ResponseHelper(res, 200, 'Data Poli Berhasil Ditampilkan', limit=="all"?poli:poli.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Poli Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    const {
      id, nama_poli, kode_poli, label_antrian, clinic_id, creator_id, import_id, status
    } = req.body;

    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }
    
    // Check poli is exist
    let checkPolinama = await findOnePoli({
      where:{nama_poli : req.body.nama_poli, clinic_id:user.clinic_id}
    });
    if (checkPolinama) {
      return ResponseHelper(res, 409, 'Nama Poli Sudah Terdaftar', [
        { message: 'Nama Poli Sudah Terdaftar', param: 'nama_poli' },
      ]);
    }
    // Check poli is exist
    let checkPoli = await findOnePoli({
      where:{kode_poli : req.body.kode_poli, clinic_id:user.clinic_id}
    });
    if (checkPoli) {
      return ResponseHelper(res, 409, 'Kode Poli Sudah Terdaftar', [
        { message: 'Kode Poli Sudah Terdaftar', param: 'kode_poli' },
      ]);
    }
    const poli = await createPoli({ 
      id, nama_poli, kode_poli, label_antrian, clinic_id, creator_id, import_id, status
    });
    await updatePoli(
      {clinic_id:user.clinic_id},
      {where: {id: poli.id}}
    )
    
    //cek seat
    // const kursi = await findSeatById(poli.seat_id);
    // const jadkur = await findJadwalseatById(kursi.jadwal_id);
    // const statkur = kursi.status;
    // let check_seat_id = await findOneSeat({
    //   where: { id: seat_id},
    // });
    // if(check_seat_id){
    //   if(statkur==true){
    //     await updateSeat(
    //       { status: false },
    //       { where: { id: poli.seat_id } }
    //     );
    //   }
    //   if(statkur==false){
    //     return ResponseHelper(res, 409, 'seat not available', [
    //       { message: 'seat not available', param: 'seat' },
    //     ]);
    //   }
    // }if (!errors.isEmpty()) {
    //   return ResponseHelper(res, 422, 'Validation Error', errors.array());
    // }

    // const jadwalseat ={
    //   "dari_jam" : jadkur.dari_jam,
    //   "sampai_jam" : jadkur.sampai_jam
    // }

    // const seat ={
    //   "nama_seat" : kursi.nama_seat,
    //   "kode_seat" : kursi.kode_seat,
    //   "jadwal_seat" : jadwalseat
    // }

    const hasil = {
      "nama_poli" : poli.nama_poli,
      "kode_poli" : poli.kode_poli,
      "label_antrian" : poli.label_antrian,
      // "seat" : seat
    }

    return ResponseHelper(res, 201, 'Data Poli Berhasil Ditambahkan', hasil);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Poli Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check poli is exist
    let checkPoli = await findPoliById(id);
    if (!checkPoli) {
      return ResponseHelper(res, 409, 'Data Poli Tidak Tersedia', [
        { message: 'Data Poli Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check poli is exist
    let checkPolinama = await findOnePoli({
      where:{nama_poli : req.body.nama_poli, clinic_id:user.clinic_id,
        id:{[Op.not]: id}}
    });
    if (checkPolinama) {
      return ResponseHelper(res, 409, 'Nama Poli Sudah Terdaftar', [
        { message: 'Nama Poli Sudah Terdaftar', param: 'nama_poli' },
      ]);
    }
    // Check poli code is exist
    let checkPolikode = await findOnePoli({
      where:{kode_poli : req.body.kode_poli, clinic_id:user.clinic_id,
        id:{[Op.not]: id}}
    });
    if (checkPolikode) {
      return ResponseHelper(res, 409, 'Kode Poli Sudah Terdaftar', [
        { message: 'Kode Poli Sudah Terdaftar', param: 'kode_poli' },
      ]);
    }

    // Update poli
    await updatePoli({ ...req.body }, { where: { id } });

    const result = await findPoliById(id);

    return ResponseHelper(res, 201, 'Data Poli Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Poli Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check POli Exist Or Not
    let checkPoli = await findPoliById(id);
    if (!checkPoli) {
      return ResponseHelper(res, 409, 'Data Poli Tidak Tersedia', [
        { message: 'Data Poli Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete poli
    let users = await findUserById(userId);

    await updatePoli({ deletedBy: users }, { where: { id } });

    await deletePoli(id);

    return ResponseHelper(res, 201, 'Data Poli Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Poli Gagal Dihapus', error.message);
  }
};

export { get, add, update, deleted };
