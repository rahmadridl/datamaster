import { get, add, update, deleted } from './poli_controller.js';
import validator from './poli_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const PoliRoutes = (app, prefix) => {
  app.route(`${prefix}/poli`).get(AuthMiddleware, get);
  app.route(`${prefix}/poli/add`).post(AuthMiddleware, validator('poli'), add);
  app.route(`${prefix}/poli/update/:id`).patch(AuthMiddleware, validator('poli'), update);
  app.route(`${prefix}/poli/delete/:id`).delete(AuthMiddleware, deleted);
};

export { PoliRoutes };
