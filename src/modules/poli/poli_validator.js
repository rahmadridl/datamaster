import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'poli': {
      return [
        body('kode_poli').not().isEmpty().withMessage('Kode Harus Diisi'),
        body('nama_poli').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('label_antrian').not().isEmpty().withMessage('Label Antrian Harus Diisi'),
        // body('clinic_id').not().isEmpty().withMessage('clinic id Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
