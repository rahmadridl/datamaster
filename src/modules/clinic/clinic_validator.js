import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'clinic': {
      return [
        body('clinic_code').not().isEmpty().withMessage('Kode Harus Diisi'),
        body('clinic_name').not().isEmpty().withMessage('Nama Klinik Harus Diisi'),
        body('account_type').not().isEmpty().withMessage('Tipe Akun Harus Diisi'),
        body('phone').not().isEmpty().withMessage('Nomor Telfon Harus Diisi'),
        body('alamat').not().isEmpty().withMessage('Alamat Harus Diisi'),
        body('email').not().isEmpty().withMessage('Email Harus Diisi'),
        body('email').isEmail().withMessage('Format Email Salah'),
        body('gambar_header').not().isEmpty().withMessage('Gambar Header Harus Diisi'),
        body('body_background').not().isEmpty().withMessage('Body BackGround Harus Diisi'),
        body('footer').not().isEmpty().withMessage('Footer Harus Diisi'),
        body('api_key').not().isEmpty().withMessage('Api Key Harus Diisi'),
        body('page_size').not().isEmpty().withMessage('Page Size Harus Diisi'),
        // body('remarks').not().isEmpty().withMessage('name Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
