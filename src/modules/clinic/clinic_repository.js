import db from '../../database/models/index.js';
const { Clinic } = db;
const { Setting } = db;
import { Op } from 'sequelize';

// Find one clinic by id
const findClinicById = async (id) => {
  try {
    let result = await Clinic.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findClinicById', error);
    throw new Error(error);
  }
};

// Find one setting by id
const findSettingById = async (id) => {
  try {
    let result = await Setting.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findSettingById', error);
    throw new Error(error);
  }
};

// Find one clinic by filter
const findOneClinic = async (filter) => {
  try {
    let result = await Clinic.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneClinic', error);
    throw new Error(error);
  }
};

// Find one setting by filter
const findOneSetting = async (filter) => {
  try {
    let result = await Setting.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneSetting', error);
    throw new Error(error);
  }
};

// Find list clinic
const findListClinic = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Clinic.findAll({
        where: {
          clinic_name: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          // clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Clinic.findAndCountAll({
        where: {
          clinic_name: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListClinic', error);
    throw new Error(error);
  }
};

// Find list setting
const findListSetting = async (status, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Setting.findAll({
        where: {
          // clinic_name: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Setting.findAndCountAll({
        where: {
          // clinic_name: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id:status
          // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListSetting', error);
    throw new Error(error);
  }
};

// Create new clinic
const createClinic = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Clinic.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createClinic', error);
    throw new Error(error);
  }
};

// Create new setting
const createSetting = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Setting.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createSetting', error);
    throw new Error(error);
  }
};

// Update clinik
const updateClinic = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Clinic.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateClinic', error);
    throw new Error(error);
  }
};

// Update setting
const updateSetting = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Setting.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateSetting', error);
    throw new Error(error);
  }
};

//delete clinic
const deleteClinic = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Clinic.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteClinic', error);
    throw new Error(error);
  }
};

export { findClinicById, findSettingById, findOneClinic, findOneSetting, findListClinic, findListSetting, createClinic, createSetting, updateClinic, updateSetting, deleteClinic };
