import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import { Op } from 'sequelize';
import {
  createClinic,
  findListClinic,
  findOneClinic,
  findClinicById,
  updateClinic,
  deleteClinic,
  createSetting,
  updateSetting,
  findOneSetting,
} from './clinic_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let clinic = await findListClinic(requirement, page, limit);
    // console.log('clinic ', clinic.rows.length);
    let data = limit == 'all' ? clinic : clinic.rows;
    // let newPayload = [];==========================================================
    // data.forEach(async (item) => {
    //   let setting = (await findOneSetting({ where: { clinic_id: item.id } })) || null;
    //   newPayload.push({
    //     ...item,
    //     setting,
    //   });
    // });
    // console.log('newPayload', newPayload);========================================
    // console.log(clinic.rows[9]);
    var setting = null;
    var payload = [];
    if (limit == 'all') {
      for (let i = 0; i < clinic.length; i++) {
        // console.log('clinic.id ', clinic[i]);
        // console.log('clinic.rows.id', clinic.rows[i].id);
        console.log('i', i);
        setting = (await findOneSetting({ where: { clinic_id: clinic[i].id } })) || null;
        // clinic[i]['setting'] = setting
        payload.push({
          id:clinic[i].id,
          clinic_code:clinic[i].clinic_code,
          clinic_name:clinic[i].clinic_name,
          logo:clinic[i].logo,
          email:clinic[i].email,
          phone:clinic[i].phone,
          alamat:clinic[i].alamat,
          license_type:clinic[i].license_type,
          license_duration:clinic[i].license_duration,
          account_type:clinic[i].account_type,
          created_by:clinic[i].created_by,
          status:clinic[i].status,
          created_at:clinic[i].created_at,
          updated_at:clinic[i].updated_at,
          deleted_at:clinic[i].deleted_at,
          setting
        });
      }
    } else {
      for (let i = 0; i < clinic.rows.length; i++) {
        // console.log('clinic.id ', clinic[i]);
        // console.log('clinic.rows.id', clinic.rows[i].id);
        console.log('i', i);
        setting = (await findOneSetting({ where: { clinic_id: clinic.rows[i].id } })) || null;
        // clinic.rows[i]['setting'] = setting
        payload.push({
          id:clinic.rows[i].id,
          clinic_code:clinic.rows[i].clinic_code,
          clinic_name:clinic.rows[i].clinic_name,
          logo:clinic.rows[i].logo,
          email:clinic.rows[i].email,
          alamat:clinic.rows[i].alamat,
          phone:clinic.rows[i].phone,
          license_type:clinic.rows[i].license_type,
          license_duration:clinic.rows[i].license_duration,
          account_type:clinic.rows[i].account_type,
          created_by:clinic.rows[i].created_by,
          status:clinic.rows[i].status,
          created_at:clinic.rows[i].created_at,
          updated_at:clinic.rows[i].updated_at,
          deleted_at:clinic.rows[i].deleted_at,
          setting
        });
      }
    }

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(clinic.count / limit),
      total_data: clinic.count,
    };

    return ResponseHelper(res, 200, 'Data Klinik Berhasil Ditampilkan', payload, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Klinik Gagal Ditampilkan', error.message);
  }
};

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    // let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let clinic = await findListClinic(requirement, page, limit);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(clinic.count / limit),
      total_data: clinic.count,
    };

    return ResponseHelper(
      res,
      200,
      'Data Klinik Berhasil Ditampilkan',
      limit == 'all' ? clinic : clinic.rows,
      meta
    );
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Klinik Gagal Ditampilkan', error.message);
  }
};

const findid = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    // let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    // let clinic = await findListClinic(requirement, page, limit);
    let clinic = await findClinicById(id);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(clinic.count / limit),
      total_data: clinic.count,
    };

    return ResponseHelper(res, 200, 'Data Klinik Berhasil Ditampilkan', clinic, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Klinik Gagal Ditampilkan', error.message);
  }
};

const findidtoken = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    // let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    // let clinic = await findListClinic(requirement, page, limit);

    function getDateTime(date) {
      // var date = new Date();
      if (!date) {
        return null
      }else{
        var hour = date.getHours();
        hour = (hour < 10 ? "0" : "") + hour;
        var min  = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;
        var sec  = date.getSeconds();
        sec = (sec < 10 ? "0" : "") + sec;
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;
        var day  = date.getDate();
        day = (day < 10 ? "0" : "") + day;
        return year + "-" + month + "-" + day
      }
    }
    
    function getDateTimeexpired(date) {
      // var date = new Date();
      if (!date) {
        return null
      } else{
        var hour = date.getHours();
        hour = (hour < 10 ? "0" : "") + hour;
        var min  = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;
        var sec  = date.getSeconds();
        sec = (sec < 10 ? "0" : "") + sec;
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;
        var day  = date.getDate();
        day = (day < 10 ? "0" : "") + day;
        return (month + "/" + day + "/" + year)
      }
    }

    let clinic = await findClinicById(id);
    // let nowlicense = 
    if (clinic.renewal_license) {//===================================== expired
      var expired = new Date(clinic.renewal_license);
      if (clinic.license_type == 'day') {
        expired.setDate(expired.getDate() + clinic.license_duration);
      }
      if (clinic.license_type == 'month') {
        expired.setMonth(expired.getMonth() + clinic.license_duration);
      }
      if (clinic.license_type == 'year') {
        expired.setFullYear(expired.getFullYear() + clinic.license_duration);
      }
    }

    function getAge(dateString) {//======================>>format-->>(bulan/tanggal/tahun)======================
      var now = new Date();
      var today = new Date(now.getYear(),now.getMonth(),now.getDate());
    
      var yearNow = now.getYear();
      var monthNow = now.getMonth();
      var dateNow = now.getDate();
    
      var dob = new Date(dateString);
    
      var yearDob = dob.getYear();
      var monthDob = dob.getMonth();
      var dateDob = dob.getDate();
      var age = {};
      var ageString = "";
      var yearString = "";
      var monthString = "";
      var dayString = "";
    
    
      var yearAge = yearDob - yearNow;
    
      if (monthDob >= monthNow)
        var monthAge = monthDob - monthNow;
      else {
        yearAge--;
        var monthAge = 12 + monthDob -monthNow;
      }
    
      if (dateDob >= dateNow)
        var dateAge = dateDob - dateNow;
      else {
        monthAge--;
        var dateAge = 31 + dateDob - dateNow;
    
        if (monthAge < 0) {
          monthAge = 11;
          yearAge--;
        }
      }
    
      age = {
          years: yearAge,
          months: monthAge,
          days: dateAge
          };
    
      if ( age.years > 1 ) yearString = " tahun";
      else yearString = " tahun";
      if ( age.months> 1 ) monthString = " bulan";
      else monthString = " bulan";
      if ( age.days > 1 ) dayString = " hari";
      else dayString = " hari";
    
    
      if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
        ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString ;
      else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
        ageString = "Sisa " + age.days + dayString + "!";
      else if ( (age.years == 0) && (age.months == 0) && (age.days == 0) )
        ageString = "Hari terakhir!!";
      else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
        ageString = age.years + yearString ;
      else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
        ageString = age.years + yearString + " and " + age.months + monthString ;
      else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
        ageString = age.months + monthString + " and " + age.days + dayString ;
      else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
        ageString = age.years + yearString + " and " + age.days + dayString ;
      else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
        ageString = age.months + monthString ;
      else if ( !dateString )
        ageString = "Oops! Belum bisa dihitung!";
      else ageString = "Masa lisensi habis!";
    
      return ageString;
    }

    let payload = {
      'id':clinic.id,
      'clinic_code':clinic.clinic_code,
      'clinic_name':clinic.clinic_name,
      'logo':clinic.logo,
      'alamat':clinic.alamat,
      'remarks':clinic.remarks,
      'email':clinic.email,
      'phone':clinic.phone,
      'license_type':clinic.license_type,
      'license_duration':clinic.license_duration,
      'account_type':clinic.account_type,
      'created_by':clinic.created_by,
      'status':clinic.status,
      'start_license':getDateTime(clinic.start_license),
      'renewal_license':getDateTime(clinic.renewal_license) || null,
      'expired':getDateTime(expired) || null,
      'expired_duration':getAge(getDateTimeexpired(expired)),
      'created_at':clinic.created_at,
      'updated_at':clinic.updated_at,
      'deleted_at':clinic.deleted_at,
    }

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(clinic.count / limit),
      total_data: clinic.count,
    };

    return ResponseHelper(res, 200, 'Data Klinik Berhasil Ditampilkan', payload, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Klinik Gagal Ditampilkan', error.message);
  }
};

const findkode = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    // let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    // let clinic = await findListClinic(requirement, page, limit);
    let clinic = await findOneClinic({where:{clinic_code:id}});

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(clinic.count / limit),
      total_data: clinic.count,
    };

    return ResponseHelper(res, 200, 'Data Klinik Berhasil Ditampilkan', clinic, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Klinik Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check cliniccode is exist
    let checkClinicemail = await findOneClinic({
      where: { email: req.body.email },
    });
    if (checkClinicemail) {
      return ResponseHelper(res, 409, 'Email Sudah Terdaftar', [
        { message: 'Email Sudah Terdaftar', param: 'email' },
      ]);
    }
    // Check cliniccode is exist
    let checkClinic = await findOneClinic({
      where: { clinic_code: req.body.clinic_code },
    });
    if (checkClinic) {
      return ResponseHelper(res, 409, 'Kode Klinik Sudah Terdaftar', [
        { message: 'Kode Klinik Sudah Terdaftar', param: 'clinic_code' },
      ]);
    }
    // Check clinic_name is exist
    // let checkClinicname = await findOneClinic({
    //   where: { clinic_name: req.body.clinic_name },
    // });
    // if (checkClinicname) {
    //   return ResponseHelper(res, 409, 'clinic_name already registered', [
    //     { message: 'clinic_name already registered', param: 'clinic_name' },
    //   ]);
    // }

    const clinic = await createClinic({ ...req.body, created_by:user_id });
    const setting = await createSetting({
      ...req.body,
      clinic_id: clinic.id,
    });
    return ResponseHelper(res, 201, 'Data Klinik Berhasil Ditambahkan', clinic);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Klinik Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;
    // Check clinic is exist
    let checkClinic = await findClinicById(id);
    if (!checkClinic) {
      return ResponseHelper(res, 409, 'Data Klinik Tidak Tersedia', [
        { message: 'Data Klinik Tidak Tersedia', param: 'id' },
      ]);
    }
    // Check clinic email is exist
    let checkClinicemail = await findOneClinic({
      where: { email: req.body.email, id: { [Op.not]: id } },
    });
    if (checkClinicemail) {
      return ResponseHelper(res, 409, 'Email Sudah Terdaftar', [
        { message: 'Email Sudah Terdaftar', param: 'email' },
      ]);
    }
    // Check cliniccode is exist
    let checkCliniccode = await findOneClinic({
      where: { clinic_code: req.body.clinic_code, id: { [Op.not]: id } },
    });
    if (checkCliniccode) {
      return ResponseHelper(res, 409, 'Kode Klinik Sudah Terdaftar', [
        { message: 'Kode Klinik Sudah Terdaftar', param: 'clinic_code' },
      ]);
    }
    // Check clinic_name is exist
    // let checkClinicname = await findOneClinic({
    //   where: { clinic_name: req.body.clinic_name, id: { [Op.not]: id } },
    // });
    // if (checkClinicname) {
    //   return ResponseHelper(res, 409, 'clinic_name already registered', [
    //     { message: 'clinic_name already registered', param: 'clinic_name' },
    //   ]);
    // }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Update clinic
    const clinic = await findClinicById(id);
    await updateClinic({ ...req.body }, { where: { id } });
    await updateSetting({ ...req.body }, { where: { clinic_id: clinic.id } });

    const result = await findClinicById(id);

    return ResponseHelper(res, 201, 'Data Klinik Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Klinik Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check clinic Exist Or Not
    let checkClinic = await findClinicById(id);
    if (!checkClinic) {
      return ResponseHelper(res, 409, 'Data Klinik Tidak Tersedia', [
        { message: 'Data Klinik Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete clinic
    let users = await findUserById(userId);

    await updateClinic({ deletedBy: users }, { where: { id } });

    await deleteClinic(id);

    return ResponseHelper(res, 201, 'Data Klinik Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Klinik Gagal Dihapus', error.message);
  }
};

export { get, get1, findid, findidtoken, findkode, add, update, deleted };
