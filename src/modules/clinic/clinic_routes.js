import { get, add, update, deleted, get1, findid, findkode, findidtoken } from './clinic_controller.js';
import validator from './clinic_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const ClinicRoutes = (app, prefix) => {
  app.route(`${prefix}/clinic`).get(AuthMiddleware, get);
  app.route(`${prefix}/clinicnotoken`).get( get1);
  app.route(`${prefix}/clinicidnotoken/:id`).get( findid);
  app.route(`${prefix}/clinicid/:id`).get(AuthMiddleware, findidtoken);
  app.route(`${prefix}/clinickodenotoken/:id`).get( findkode);
  app.route(`${prefix}/clinic/add`).post(AuthMiddleware, validator('clinic'), add);
  app.route(`${prefix}/clinic/update/:id`).patch(AuthMiddleware, validator('clinic'), update);
  app.route(`${prefix}/clinic/delete/:id`).delete(AuthMiddleware, deleted);
};

export { ClinicRoutes };
