import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import {
  createRekanan,
  findListRekanan,
  findRekananById,
  updateRekanan,
  deleteRekanan
} from './rekanan_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'not allowed to access', [
        { message: 'not allowed to access', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let rekanan = await findListRekanan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(rekanan.count / limit),
      total_data: rekanan.count,
    };

    return ResponseHelper(res, 200, 'success get list data rekanan', rekanan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed get perujuk', error.message);
  }
};

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'not allowed to access', [
    //     { message: 'not allowed to access', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let rekanan = await findListRekanan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(rekanan.count / limit),
      total_data: rekanan.count,
    };

    return ResponseHelper(res, 200, 'success get list data rekanan', rekanan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed get perujuk', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'not allowed to access', [
    //     { message: 'not allowed to access', param: 'id' },
    //   ]);
    // }

    const rekanan = await createRekanan({ ...req.body });
    await updateRekanan(
      {clinic_id:user.clinic_id},
      {where: {id: rekanan.id}}
    )
    return ResponseHelper(res, 201, 'success create new rekanan', rekanan);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed create new rekanan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check rekanan is exist
    let checkRekanan = await findRekananById(id);
    if (!checkRekanan) {
      return ResponseHelper(res, 409, 'Rekanan is not exist', [
        { message: 'Rekanan is not exist', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'not allowed to access', [
        { message: 'not allowed to access', param: 'id' },
      ]);
    }

    // Update rekanan
    await updateRekanan({ ...req.body }, { where: { id } });

    const result = await findRekananById(id);

    return ResponseHelper(res, 201, 'success updated selected Rekanan', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed updated selected Rekanan', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check rekanan Exist Or Not
    let checkRekanan = await findRekananById(id);
    if (!checkRekanan) {
      return ResponseHelper(res, 409, 'Rekanan Is Not Exist In Database', [
        { message: 'Rekanan Is Not Exist In Database', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'not allowed to access', [
        { message: 'not allowed to access', param: 'id' },
      ]);
    }

    // Delete rekanan
    let users = await findUserById(userId);

    await updateRekanan({ deletedBy: users }, { where: { id } });

    await deleteRekanan(id);

    return ResponseHelper(res, 201, 'Success Deleted Selected Rekanan', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Failed Deleted Selected Rekanan', error.message);
  }
};

export { get, get1, add, update, deleted };
