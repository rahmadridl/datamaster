import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'rekanan': {
      return [
        body('clinic_id').not().isEmpty().withMessage('clinic id can not be empty'),
        body('nama_rekanan').not().isEmpty().withMessage('name can not be empty'),
        body('telp').not().isEmpty().withMessage('telp can not be empty'),
        body('alamat').not().isEmpty().withMessage('alamat can not be empty'),
        body('email').not().isEmpty().withMessage('email can not be empty'),
        body('creator_id').not().isEmpty().withMessage('creator id can not be empty'),
        body('import_id').not().isEmpty().withMessage('import id can not be empty'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
