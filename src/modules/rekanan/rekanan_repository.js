import db from '../../database/models/index.js';
const { Rekanan } = db;
import { Op } from 'sequelize';

// Find one rekanan by id
const findRekananById = async (id) => {
  try {
    let result = await Rekanan.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findRekananById', error);
    throw new Error(error);
  }
};

// Find one rekanan by filter
const findOneRekanan = async (filter) => {
  try {
    let result = await Rekanan.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneRekanan', error);
    throw new Error(error);
  }
};

// Find list rekanan
const findListRekanan = async ({ search, status }, page, limit) => {
  try {
    let result = await Rekanan.findAndCountAll({
      where: {
        nama_rekanan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        clinic_id: status 
      },order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListRekanan', error);
    throw new Error(error);
  }
};

// Create new rekanan
const createRekanan = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Rekanan.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createRekanan', error);
    throw new Error(error);
  }
};

// Update rekanan
const updateRekanan = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Rekanan.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateRekanan', error);
    throw new Error(error);
  }
};

//delete rekanan
const deleteRekanan = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Rekanan.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteRekanan', error);
    throw new Error(error);
  }
};

export { findRekananById, findOneRekanan, findListRekanan, createRekanan, updateRekanan, deleteRekanan };
