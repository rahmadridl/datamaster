import { get, get1, add, update, deleted } from './rekanan_controller.js';
import validator from './rekanan_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const RekananRoutes = (app, prefix) => {
  app.route(`${prefix}/rekanan`).get(AuthMiddleware, get);
  app.route(`${prefix}/rekanannotoken`).get( get1);
  app.route(`${prefix}/rekanan/add`).post(AuthMiddleware, validator('rekanan'), add);
  app.route(`${prefix}/rekanan/update/:id`).patch(AuthMiddleware, validator('rekanan'), update);
  app.route(`${prefix}/rekanan/delete/:id`).delete(AuthMiddleware, deleted);
};

export { RekananRoutes };
