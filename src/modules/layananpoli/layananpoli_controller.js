import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import { Op } from 'sequelize';
import {
  createLayananpoli,
  findListLayananpoli,
  findLayananpoliById,
  findOneLayananpoli,
  updateLayananpoli,
  deleteLayananpoli
} from './layananpoli_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let layananpoli = await findListLayananpoli(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(layananpoli.count / limit),
      total_data: layananpoli.count,
    };

    return ResponseHelper(res, 200, 'Data Layanan Poli Berhasil Ditampilkan', layananpoli.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Layanan Poli Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    // Check layanan poli nama is exist
    let checkLayananpoli = await findOneLayananpoli({
      where:{nama_layanan_poli : req.body.nama_layanan_poli}
    });
    if (checkLayananpoli) {
      return ResponseHelper(res, 409, 'Nama Layanan Poli Sudah Terdaftar', [
        { message: 'Nama Layanan Poli Sudah Terdaftar', param: 'nama_layanan_poli' },
      ]);
    }
    // Check layanan poli kode is exist
    let checkLayananpolikode = await findOneLayananpoli({
      where:{kode_layanan_poli : req.body.kode_layanan_poli}
    });
    if (checkLayananpolikode) {
      return ResponseHelper(res, 409, 'Kode Layanan Poli Sudah Terdaftar', [
        { message: 'Kode Layanan Poli Sudah Terdaftar', param: 'kode_layanan_poli' },
      ]);
    }

    const layananpoli = await createLayananpoli({ ...req.body });
    await updateLayananpoli(
      {clinic_id:user.clinic_id},
      {where: {id: layananpoli.id}}
    )
    return ResponseHelper(res, 201, 'Data Layanan Poli Berhasil Ditambahkan', layananpoli);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Layanan Poli Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check layananpoli is exist
    let checkLayananpoli = await findLayananpoliById(id);
    if (!checkLayananpoli) {
      return ResponseHelper(res, 409, 'Data Layanan Poli Tidak Tersedia', [
        { message: 'Data Layanan Poli Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check layanan poli nama is exist
    let checkLayananpolinama = await findOneLayananpoli({
      where:{nama_layanan_poli : req.body.nama_layanan_poli,
        id:{[Op.not]: id}}
    });
    if (checkLayananpolinama) {
      return ResponseHelper(res, 409, 'Nama Layanan Poli Sudah Terdaftar', [
        { message: 'Nama Layanan Poli Sudah Terdaftar', param: 'nama_layanan_poli' },
      ]);
    }
    // Check layanan poli kode is exist
    let checkLayananpolikode = await findOneLayananpoli({
      where:{kode_layanan_poli : req.body.kode_layanan_poli,
        id:{[Op.not]: id}}
    });
    if (checkLayananpolikode) {
      return ResponseHelper(res, 409, 'Kode Layanan Poli Sudah Terdaftar', [
        { message: 'Kode Layanan Poli Sudah Terdaftar', param: 'kode_layanan_poli' },
      ]);
    }

    // Update layananpoli
    await updateLayananpoli({ ...req.body }, { where: { id } });

    const result = await findLayananpoliById(id);

    return ResponseHelper(res, 201, 'Data Layanan Poli Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Layanan Poli Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check layananpoli Exist Or Not
    let checkLayananpoli = await findLayananpoliById(id);
    if (!checkLayananpoli) {
      return ResponseHelper(res, 409, 'Data Layanan Poli Tidak Tersedia', [
        { message: 'Data Layanan Poli Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete layananpoli
    let users = await findUserById(userId);

    await updateLayananpoli({ deletedBy: users }, { where: { id } });

    await deleteLayananpoli(id);

    return ResponseHelper(res, 201, 'Data Layanan Poli Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Layanan Poli Gagal Dihapus', error.message);
  }
};

export { get, add, update, deleted };
