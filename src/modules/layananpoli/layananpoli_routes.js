import { get, add, update, deleted } from './layananpoli_controller.js';
import validator from './layananpoli_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const LayananpoliRoutes = (app, prefix) => {
  app.route(`${prefix}/layananpoli`).get(AuthMiddleware, get);
  app.route(`${prefix}/layananpoli/add`).post(AuthMiddleware, validator('layananpoli'), add);
  app.route(`${prefix}/layananpoli/update/:id`).patch(AuthMiddleware, validator('layananpoli'), update);
  app.route(`${prefix}/layananpoli/delete/:id`).delete(AuthMiddleware, deleted);
};

export { LayananpoliRoutes };
