import db from '../../database/models/index.js';
const { Layananpoli } = db;
import { Op } from 'sequelize';

// Find one layananpoli by id
const findLayananpoliById = async (id) => {
  try {
    let result = await Layananpoli.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findLayananpoliById', error);
    throw new Error(error);
  }
};

// Find one layananpoli by filter
const findOneLayananpoli = async (filter) => {
  try {
    let result = await Layananpoli.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneLayananpoli', error);
    throw new Error(error);
  }
};

// Find list layananpoli
const findListLayananpoli = async ({ search, status }, page, limit) => {
  try {
    let result = await Layananpoli.findAndCountAll({
      where: {
        nama_layanan_poli: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListLayananpoli', error);
    throw new Error(error);
  }
};

// Create new layananpoli
const createLayananpoli = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Layananpoli.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createLayananpoli', error);
    throw new Error(error);
  }
};

// Update layananpoli
const updateLayananpoli = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Layananpoli.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateLayananpoli', error);
    throw new Error(error);
  }
};

//delete layananpoli
const deleteLayananpoli = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Layananpoli.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteLayananpoli', error);
    throw new Error(error);
  }
};

export { findLayananpoliById, findOneLayananpoli, findListLayananpoli, createLayananpoli, updateLayananpoli, deleteLayananpoli };
