import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'layananpoli': {
      return [
        body('nama_layanan_poli').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('kode_layanan_poli').not().isEmpty().withMessage('Kode Harus Diisi'),
        // body('creator_id').not().isEmpty().withMessage('creator id Harus Diisi'),
        // body('clinic_id').not().isEmpty().withMessage('clinic id Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
