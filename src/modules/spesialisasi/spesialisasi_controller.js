import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import xlsx from 'node-xlsx';
import { Op } from 'sequelize';
import {
  createSpesialisasi,
  findListSpesialisasi,
  findSpesialisasiById,
  updateSpesialisasi,
  deleteSpesialisasi,
  findOneSpesialisasi
} from './spesialisasi_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status||status==0) requirement.status = status;

    let spesialisasi = await findListSpesialisasi(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(spesialisasi.count / limit),
      total_data: spesialisasi.count,
    };

    return ResponseHelper(res, 200, 'Data Spesialisasi Berhasil Ditampilkan', limit=="all"?spesialisasi:spesialisasi.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Spesialisasi Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    console.log(user_id);
    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check spesialisasi is exist
    let checkSpesialisasi = await findOneSpesialisasi({
      where:{nama_spesialisasi : req.body.nama_spesialisasi, clinic_id : user.clinic_id}
    });
    if (checkSpesialisasi) {
      return ResponseHelper(res, 409, 'Nama Spesialisasi Sudah Terdaftar', [
        { message: 'Nama Spesialisasi Sudah Terdaftar', param: 'nama_spesialisasi' },
      ]);
    }
    console.log(user);
    const spesialisasi = await createSpesialisasi({ ...req.body });
    await updateSpesialisasi(
      {clinic_id:user.clinic_id},
      {where: {id: spesialisasi.id}}
    )
    return ResponseHelper(res, 201, 'Data Spesialisasi Berhasil Ditambahkan', spesialisasi);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Spesialisasi Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check spesialisasi is exist
    let checkSpesialisasi = await findSpesialisasiById(id);
    if (!checkSpesialisasi) {
      return ResponseHelper(res, 409, 'Data Spesialisasi Tidak Tersedia', [
        { message: 'Data Spesialisasi Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check spesialisasi is exist
    let checkSpesialisasinama = await findOneSpesialisasi({
      where:{nama_spesialisasi : req.body.nama_spesialisasi, clinic_id : user.clinic_id,
        id:{[Op.not]: id}}
    });
    if (checkSpesialisasinama) {
      return ResponseHelper(res, 409, 'Nama Spesialisasi Sudah Terdaftar', [
        { message: 'Nama Spesialisasi Sudah Terdaftar', param: 'nama_spesialisasi' },
      ]);
    }

    // Update spesialisasi
    await updateSpesialisasi({ ...req.body }, { where: { id } });

    const result = await findSpesialisasiById(id);

    return ResponseHelper(res, 201, 'Data Spesialisasi Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Spesialisasi Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check spesialisasi Exist Or Not
    let checkSpesialisasi = await findSpesialisasiById(id);
    if (!checkSpesialisasi) {
      return ResponseHelper(res, 409, 'Data Spesialisasi Tidak Tersedia', [
        { message: 'Data Spesialisasi Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete spesialisasi
    let users = await findUserById(userId);

    await updateSpesialisasi({ deletedBy: users }, { where: { id } });

    await deleteSpesialisasi(id);

    return ResponseHelper(res, 201, 'Data Spesialisasi Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Spesialisasi Gagal Dihapus', error.message);
  }
};

const upload = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    
    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }
    
    // console.log(path.dirname(__filename)+"\\myFile.xlsx");
    const workSheetsFromFile = xlsx.parse(req.files.import.data);
    // console.log(workSheetsFromFile[0].data);
    var arr = []
    workSheetsFromFile.forEach((row) => {
      row.data.forEach(async (data, index) => {
        let object = {};
        if (index > 8) {
          row.data[8].forEach(async (header, index) => {
            object[header.replace('*','')] = data[index];
          });
          if (object.nama_spesialisasi) {
            arr.push(object)
          }
        }
      });
      // console.log(arr);
    });
    for (let i = 0; i < arr.length; i++) {
      const cek = await findOneSpesialisasi({where:{nama_spesialisasi:arr[i].nama_spesialisasi, clinic_id:user.clinic_id}})
      if (cek) {
        return ResponseHelper(res, 409, 'Error: baris ke '+(i+2)+', nama spesialisasi '+arr[i].nama_spesialisasi+' sudah terdaftar', arr[i].nama_spesialisasi);
      }else{
        const tes = await createSpesialisasi(
          {nama_spesialisasi:arr[i].nama_spesialisasi, clinic_id:user.clinic_id}
        )
      }
    }
    return ResponseHelper(res, 201, 'Data Spesialisasi Berhasil Diimport');
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Spesialisasi Gagal Diimport', error.message);
  }
};

export { get, add, update, deleted, upload };
