import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'spesialisasi': {
      return [
        body('nama_spesialisasi').not().isEmpty().withMessage('Nama Harus Diisi'),
        // body('clinic_id').not().isEmpty().withMessage('clinic id Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
