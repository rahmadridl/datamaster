import { get, add, update, deleted, upload } from './spesialisasi_controller.js';
import validator from './spesialisasi_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const SpesialisasiRoutes = (app, prefix) => {
  app.route(`${prefix}/spesialisasi`).get(AuthMiddleware, get);
  app.route(`${prefix}/spesialisasi/import`).post(AuthMiddleware, upload);
  app.route(`${prefix}/spesialisasi/add`).post(AuthMiddleware ,validator('spesialisasi'), add);
  app.route(`${prefix}/spesialisasi/update/:id`).patch(AuthMiddleware, validator('spesialisasi'), update);
  app.route(`${prefix}/spesialisasi/delete/:id`).delete(AuthMiddleware, deleted);
};

export { SpesialisasiRoutes };
