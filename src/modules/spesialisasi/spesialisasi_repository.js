import db from '../../database/models/index.js';
const { Spesialisasi } = db;
import { Op } from 'sequelize';

// Find one spesialisasi by id
const findSpesialisasiById = async (id) => {
  try {
    let result = await Spesialisasi.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findSpesialisasiById', error);
    throw new Error(error);
  }
};

// Find one spesialisasi by filter
const findOneSpesialisasi = async (filter) => {
  try {
    let result = await Spesialisasi.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneSpesialisasi', error);
    throw new Error(error);
  }
};

// Find list spesialisasi
const findListSpesialisasi = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Spesialisasi.findAll({
        where: {
          nama_spesialisasi: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Spesialisasi.findAndCountAll({
        where: {
          nama_spesialisasi: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListSpesialisasi', error);
    throw new Error(error);
  }
};

// Create new spesialisasi
const createSpesialisasi = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Spesialisasi.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createSpesialisasi', error);
    throw new Error(error);
  }
};

// Update spesialisasi
const updateSpesialisasi = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Spesialisasi.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateSpesialisasi', error);
    throw new Error(error);
  }
};

//delete spesialisasi
const deleteSpesialisasi = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Spesialisasi.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteSpesialisasi', error);
    throw new Error(error);
  }
};

export {
  findSpesialisasiById,
  findOneSpesialisasi,
  findListSpesialisasi,
  createSpesialisasi,
  updateSpesialisasi,
  deleteSpesialisasi,
};
