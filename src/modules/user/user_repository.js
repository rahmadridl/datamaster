import db from '../../database/models/index.js';
const { Users } = db;
import { Op } from 'sequelize';

// Find one user by id
const findUserById = async (id) => {
  try {
    let result = await Users.findByPk(id, {
      raw: true,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findUserById', error);
    throw new Error(error);
  }
};

// Find One User By Filter
const findOneUser = async (filter) => {
  try {
    let result = await Users.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneUser', error);
    throw new Error(error);
  }
};

// Find list user
const findListUser = async ({ search, status }, page, limit) => {
  try {
    let result = await Users.findAndCountAll({
      where: {
        name: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListUsers', error);
    throw new Error(error);
  }
};

// Update User
const updateUser = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Users.update(data, { ...filter, transaction: t });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateUser', error);
    throw new Error(error);
  }
};

//delete User
const deleteUser = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Users.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteUser', error);
    throw new Error(error);
  }
};

export { findUserById, findOneUser, findListUser, updateUser, deleteUser };
