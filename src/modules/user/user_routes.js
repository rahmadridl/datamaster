import { user, update, updateAdmin, deleted, listuser } from './user_controller.js';
import validator from './user_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const UserRoutes = (app, prefix) => {
  app.route(`${prefix}/profile`).get(AuthMiddleware, user);
  app.route(`${prefix}/listprofile`).get(AuthMiddleware, listuser);
  app.route(`${prefix}/profile`).patch(AuthMiddleware, validator('update'), update);
  app.route(`${prefix}/admin/profile/:id`).patch(AuthMiddleware, validator('update'), updateAdmin);
  app.route(`${prefix}/profile/delete/:id`).delete(AuthMiddleware, deleted);
};

export { UserRoutes };
