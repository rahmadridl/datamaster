import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'update': {
      return [
        body('name').not().isEmpty().withMessage('name Harus Diisi'),
        // body('status').not().isEmpty().withMessage('status Harus Diisi'),
        // body('phone').not().isEmpty().withMessage('phone number Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
