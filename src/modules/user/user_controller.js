import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findClinicById } from '../clinic/clinic_repository.js';
import { findOneUser, findUserById, findListUser,updateUser, deleteUser } from './user_repository.js';

// Get Detail User
const user = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const user = await findUserById(user_id);
    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;
    let userlist = await findListUser(requirement, page, limit)
    if (!user) {
      return ResponseHelper(res, 404, 'User Tidak Ditemukan', [
        { message: 'User Tidak Ditemukan', param: 'id' },
      ]);
    }
    
    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(user.count / limit),
      total_data: user.count,
    };

    const clinic = await findClinicById(user.clinic_id)
    delete user.password;
    const userclinic = {...user, clinic}

    return ResponseHelper(res, 200, 'Detail User Berhasil Ditampilkan', userclinic, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Detail User Gagal Ditampilkan', error.message);
  }
};

const listuser = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const user = await findUserById(user_id);
    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;
    let userlist = await findListUser(requirement, page, limit)
    if (!user) {
      return ResponseHelper(res, 404, 'User Tidak Ditemukan', [
        { message: 'User Tidak Ditemukan', param: 'id' },
      ]);
    }
    
    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(user.count / limit),
      total_data: user.count,
    };

    const clinic = await findClinicById(user.clinic_id)
    const userclinic = {...user, clinic}

    delete user.password;
    return ResponseHelper(res, 200, 'Detail User Berhasil Ditampilkan', userlist.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Detail User Gagal Ditampilkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 442, 'validation error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    const { name, status, phone } = req.body;

    const user = await findUserById(user_id);

    // Check user already exist in database or not
    if (!user) {
      return ResponseHelper(res, 409, 'User Tidak Ditemukan', [
        { message: 'User Tidak Ditemukan', param: 'id' },
      ]);
    }

    let check_phone = await findOneUser({
      where: { phone },
    });

    // Check phone registered
    if (check_phone) {
      if (user_id !== check_phone.id && check_phone.phone === phone) {
        return ResponseHelper(res, 409, 'Nomor Telepon Sudah Terdaftar', [
          { message: 'Nomor Telepon Sudah Terdaftar', param: 'phone' },
        ]);
      }
    }

    // Can only update
    await updateUser(
      { name, status, phone },
      { where: { id: user_id } }
    );

    const result = await findUserById(user_id);

    delete result.password;

    return ResponseHelper(res, 201, 'Data User Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data User Gagal Diubah', error.message);
  }
};

const updateAdmin = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 442, 'validation error', errors.array());
  }

  try {
    const { name, status, phone } = req.body;
    const { id } = req.params;
    const { user_id } = req.app.locals;

    const user = await findUserById(id);
    const admin = await findUserById(user_id);

    if (!user) {
      return ResponseHelper(res, 409, 'User Tidak Ditemukan', [
        { message: 'User Tidak Ditemukan', param: 'id' },
      ]);
    }

    // Check only admin can update user
    if (admin.role > 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    let check_phone = await findOneUser({
      where: { phone },
    });

    // Check phone registered
    if (check_phone) {
      if (Number(id) !== check_phone.id && check_phone.phone === phone) {
        return ResponseHelper(res, 409, 'Nomor Telepon Sudah Terdaftar', [
          { message: 'Nomor Telepon Sudah Terdaftar', param: 'phone' },
        ]);
      }
    }

    await updateUser(
      { name, status, phone },
      { where: { id: id } }
    );

    const result = await findUserById(id);

    delete result.password;

    return ResponseHelper(res, 201, 'Data User Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data User Berhasil Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check user Exist Or Not
    let checkUser = await findUserById(id);
    console.log(checkUser)
    if (!checkUser) {
      return ResponseHelper(res, 409, 'User Belum Terdaftar', [
        { message: 'User Belum Terdaftar', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete user
    let users = await findUserById(userId);

    await updateUser({ deletedBy: users }, { where: { id } });

    await deleteUser(id);

    return ResponseHelper(res, 201, 'Data User Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data User Gagal Dihapus', error.message);
  }
};

export { user, listuser, update, updateAdmin, deleted };
