import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import {
  createSubitemjenispemeriksaan,
  findListSubitemjenispemeriksaan,
  findSubitemjenispemeriksaanById,
  updateSubitemjenispemeriksaan,
  deleteSubitemjenispemeriksaan
} from './subitemjenispemeriksaan_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let subitemjenispemeriksaan = await findListSubitemjenispemeriksaan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(subitemjenispemeriksaan.count / limit),
      total_data: subitemjenispemeriksaan.count,
    };

    return ResponseHelper(res, 200, 'Data Sub Item Jenis Pemeriksaan Berhasil Ditampilkan', subitemjenispemeriksaan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Sub Item Jenis Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const subitemjenispemeriksaan = await createSubitemjenispemeriksaan({ ...req.body });
    return ResponseHelper(res, 201, 'Data Sub Item Jenis Pemeriksaan Berhasil Ditambahkan', subitemjenispemeriksaan);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Sub Item Jenis Pemeriksaan Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check sub item jenis pemeriksaan is exist
    let checkSubitemjenispemeriksaan = await findSubitemjenispemeriksaanById(id);
    if (!checkSubitemjenispemeriksaan) {
      return ResponseHelper(res, 409, 'Data Sub Item Jenis Pemeriksaan Tidak Tersedia', [
        { message: 'Data Sub Item Jenis Pemeriksaan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Update sub item jenis pemeriksaan
    await updateSubitemjenispemeriksaan({ ...req.body }, { where: { id } });

    const result = await findSubitemjenispemeriksaanById(id);

    return ResponseHelper(res, 201, 'Data Sub Item Jenis Pemeriksaan Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Sub Item Jenis Pemeriksaan Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check sub item pemeriksaan Exist Or Not
    let checkSubitemjenispemeriksaan = await findSubitemjenispemeriksaanById(id);
    if (!checkSubitemjenispemeriksaan) {
      return ResponseHelper(res, 409, 'Data Sub Item Jenis Pemeriksaan Tidak Tersedia', [
        { message: 'Data Sub Item Jenis Pemeriksaan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete subitem jenis pemeriksaan
    let users = await findUserById(userId);

    await updateSubitemjenispemeriksaan({ deletedBy: users }, { where: { id } });

    await deleteSubitemjenispemeriksaan(id);

    return ResponseHelper(res, 201, 'Data Sub Item Jenis Pemeriksaan Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Sub Item Jenis Pemeriksaan Gagal Dihapus', error.message);
  }
};
    
export { get, add, update, deleted };
