import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'subitemjenisperiksaan': {
      return [
        body('jenis_id').not().isEmpty().withMessage('Jenis Pemeriksaan Harus Diisi'),
        body('item_id').not().isEmpty().withMessage('Item Pemeriksaan Harus Diisi'),
        body('item').not().isEmpty().withMessage('Item Harus Diisi'),
        body('nilai_rujukan').not().isEmpty().withMessage('Nilai Rujukan Harus Diisi'),
        body('satuan').not().isEmpty().withMessage('Satuan Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
