import db from '../../database/models/index.js';
const { Subitemjenispemeriksaan } = db;
import { Op } from 'sequelize';

// Find one jenis pemeriksaan by id
const findSubitemjenispemeriksaanById = async (id) => {
  try {
    let result = await Subitemjenispemeriksaan.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findSubitemjenispemeriksaanById', error);
    throw new Error(error);
  }
};

// Find one Subitemjenispemeriksaan by filter
const findOneSubitemjenispemeriksaan = async (filter) => {
  try {
    let result = await Subitemjenispemeriksaan.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneSubitemjenispemeriksaan', error);
    throw new Error(error);
  }
};

// Find list Jenis pemeriksaan
const findListSubitemjenispemeriksaan = async ({ search, status }, page, limit) => {
  try {
    let result = await Subitemjenispemeriksaan.findAndCountAll({
      where: {
        item: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListSubitemjenispemeriksaan', error);
    throw new Error(error);
  }
};

// Create new Subitemjenispemeriksaan
const createSubitemjenispemeriksaan = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Subitemjenispemeriksaan.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createSubitemjenispemeriksaan', error);
    throw new Error(error);
  }
};

// Update jenis pemeriksaan
const updateSubitemjenispemeriksaan = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Subitemjenispemeriksaan.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateSubitemjenispemeriksaan', error);
    throw new Error(error);
  }
};

// delete jenis pemeriksaan
const deleteSubitemjenispemeriksaan = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Subitemjenispemeriksaan.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteSubitemjenispemeriksaan', error);
    throw new Error(error);
  }
};

export { findSubitemjenispemeriksaanById, findOneSubitemjenispemeriksaan, findListSubitemjenispemeriksaan, createSubitemjenispemeriksaan, updateSubitemjenispemeriksaan, deleteSubitemjenispemeriksaan };
