import { get, add, update, deleted } from './subitemjenispemeriksaan_controller.js';
import validator from './subitemjenispemeriksaan_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const SubitemjenispemeriksaanRoutes = (app, prefix) => {
  app.route(`${prefix}/subitemjenispemeriksaan`).get(AuthMiddleware, get);
  app.route(`${prefix}/subitemjenispemeriksaan/add`).post(AuthMiddleware, validator('subitemjenispemeriksaan'), add);
  app.route(`${prefix}/subitemjenispemeriksaan/update/:id`).patch(AuthMiddleware, validator('subitemjenispemeriksaan'), update);
  app.route(`${prefix}/subitemjenispemeriksaan/delete/:id`).delete(AuthMiddleware, deleted);
};

export { SubitemjenispemeriksaanRoutes };
