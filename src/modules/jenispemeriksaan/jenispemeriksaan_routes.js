import { get, findid, find, findkode, add, update, deleted, get1, upload, getlog } from './jenispemeriksaan_controller.js';
import validator from './jenispemeriksaan_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const JenispemeriksaanRoutes = (app, prefix) => {
  app.route(`${prefix}/jenispemeriksaan`).get(AuthMiddleware, get);
  app.route(`${prefix}/jenispemeriksaannotoken`).get(get1);
  app.route(`${prefix}/jenispemeriksaan/logistik`).get(getlog);
  app.route(`${prefix}/jenispemeriksaan/findid/:id`).get(findid);
  app.route(`${prefix}/jenispemeriksaan/find/:id`).get(find);
  app.route(`${prefix}/jenispemeriksaan/import`).post(upload);
  app.route(`${prefix}/jenispemeriksaan/findkode/:id`).get(findkode);
  app.route(`${prefix}/jenispemeriksaan/add`).post(AuthMiddleware, validator('jenispemeriksaan'), add);
  app.route(`${prefix}/jenispemeriksaan/update/:id`).patch(AuthMiddleware, validator('jenispemeriksaan'), update);
  app.route(`${prefix}/jenispemeriksaan/delete/:id`).delete(AuthMiddleware, deleted);
};

export { JenispemeriksaanRoutes };
