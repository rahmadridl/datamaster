import db from '../../database/models/index.js';
const { Jenispemeriksaan } = db;
import { Op } from 'sequelize';

// Find one jenis pemeriksaan by id
const findJenispemeriksaanById = async (id) => {
  try {
    let result = await Jenispemeriksaan.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findJenispemeriksaanById', error);
    throw new Error(error);
  }
};

// Find one Jenispemeriksaan by filter
const findOneJenispemeriksaan = async (filter) => {
  try {
    let result = await Jenispemeriksaan.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneJenispemeriksaan', error);
    throw new Error(error);
  }
};

// Find list Jenis pemeriksaan
const findListJenispemeriksaan = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Jenispemeriksaan.findAll({
        where: {
          nama_jenis_pemeriksaan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Jenispemeriksaan.findAndCountAll({
        where: {
          nama_jenis_pemeriksaan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
          clinic_id: status
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListJenispemeriksaan', error);
    throw new Error(error);
  }
};

// Find list Jenis pemeriksaan
const findListJenispemeriksaannotoken = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Jenispemeriksaan.findAll({
        where: {
          nama_jenis_pemeriksaan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Jenispemeriksaan.findAndCountAll({
        where: {
          nama_jenis_pemeriksaan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
          // clinic_id: status
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListJenispemeriksaan', error);
    throw new Error(error);
  }
};

// Find list Jenis pemeriksaan
const findListJenispemeriksaanlogistik = async ({ search, status, ids }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Jenispemeriksaan.findAll({
        where: {
          nama_jenis_pemeriksaan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          id: {[Op.in]: ids},
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Jenispemeriksaan.findAndCountAll({
        where: {
          nama_jenis_pemeriksaan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
          // clinic_id: status
          id: {[Op.in]: ids},
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListJenispemeriksaan', error);
    throw new Error(error);
  }
};

// Find list Jenis pemeriksaan
const findListJenispemeriksaankode = async ({ id, status }, page, limit) => {
  try {
    let result = await Jenispemeriksaan.findAndCountAll({
      where: {
        kode_jenis_pemeriksaan: id ? { [Op.like]: id } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListJenispemeriksaan', error);
    throw new Error(error);
  }
};

// Find Jenis pemeriksaan
const findJenispemeriksaan = async ({ id, status }, page, limit) => {
  try {
    let result = await Jenispemeriksaan.findAndCountAll({
      where: {
        nama_jenis_pemeriksaan: id ? { [Op.like]: `%${id}%` } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListJenispemeriksaan', error);
    throw new Error(error);
  }
};

// Create new jenispemeriksaan
const createJenispemeriksaan = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Jenispemeriksaan.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createJenispemeriksaan', error);
    throw new Error(error);
  }
};

// Update jenis pemeriksaan
const updateJenispemeriksaan = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Jenispemeriksaan.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateJenispemeriksaan', error);
    throw new Error(error);
  }
};

// delete jenis pemeriksaan
const deleteJenispemeriksaan = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Jenispemeriksaan.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteJenispemeriksaan', error);
    throw new Error(error);
  }
};

export {
  findJenispemeriksaanById,
  findOneJenispemeriksaan,
  findListJenispemeriksaan,
  findListJenispemeriksaankode,
  createJenispemeriksaan,
  updateJenispemeriksaan,
  deleteJenispemeriksaan,
  findJenispemeriksaan,
  findListJenispemeriksaannotoken,
  findListJenispemeriksaanlogistik
};
