import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import xlsx from 'node-xlsx';
import {
  createItemjenispemeriksaan,
  findItemjenispemeriksaan,
  findItemjenispemeriksaanById,
} from '../itemjenispemeriksaan/itemjenispemeriksaan_repository.js';
import {
  createPaketpemeriksaan,
  deletePaketpemeriksaan,
  findListPaketpemeriksaan,
} from '../paketpemeriksaan/paketpemeriksaan_repository.js';
import { findUserById } from '../user/user_repository.js';
import { Op } from 'sequelize';
import {
  createJenispemeriksaan,
  findListJenispemeriksaan,
  findJenispemeriksaanById,
  updateJenispemeriksaan,
  deleteJenispemeriksaan,
  findOneJenispemeriksaan,
  findJenispemeriksaan,
  findListJenispemeriksaankode,
  findListJenispemeriksaannotoken,
  findListJenispemeriksaanlogistik,
} from './jenispemeriksaan_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let jenispemeriksaan = await findListJenispemeriksaan(requirement, page, limit);
    if (limit == 'all') {
      var hasil = [];
      var paket = [];
      var pakett = [];
      var pk = [];
      for (let j = 0; j < jenispemeriksaan.length; j++) {
        paket.push(await findListPaketpemeriksaan(jenispemeriksaan[j].id));
        pakett.push(await findListPaketpemeriksaan(jenispemeriksaan[j].id));
      }
      for (let i = 0; i < jenispemeriksaan.length; i++) {
        for (let j = 0; j < paket[i].length; j++) {
          pakett[i].push(paket[i][j].m_item_pemeriksaan_id);
        }
        pakett[i].splice(0, paket[i].length);
      }
      for (let i = 0; i < jenispemeriksaan.length; i++) {
        pk = [];
        for (let j = 0; j < pakett[i].length; j++) {
          pk.push(await findItemjenispemeriksaanById(pakett[i][j]));
        }
        hasil.push({
          id: jenispemeriksaan[i].id,
          nama_jenis_pemeriksaan: jenispemeriksaan[i].nama_jenis_pemeriksaan,
          kode_jenis_pemeriksaan: jenispemeriksaan[i].kode_jenis_pemeriksaan,
          tarif: jenispemeriksaan[i].tarif,
          clinic_id: jenispemeriksaan[i].clinic_id,
          status: jenispemeriksaan[i].status,
          pemeriksaan: pk,
        });
      }

      const meta = {
        limit: limit,
        page: page,
        total_page: Math.ceil(jenispemeriksaan.count / limit),
        total_data: jenispemeriksaan.count,
      };

      return ResponseHelper(res, 200, 'Data Jenis Pemeriksaan Berhasil Ditampilkan', hasil, meta);
    } else {
      var hasil = [];
      var paket = [];
      var pakett = [];
      var pk = [];
      for (let j = 0; j < jenispemeriksaan.rows.length; j++) {
        paket.push(await findListPaketpemeriksaan(jenispemeriksaan.rows[j].id));
        pakett.push(await findListPaketpemeriksaan(jenispemeriksaan.rows[j].id));
      }
      for (let i = 0; i < jenispemeriksaan.rows.length; i++) {
        for (let j = 0; j < paket[i].length; j++) {
          pakett[i].push(paket[i][j].m_item_pemeriksaan_id);
        }
        pakett[i].splice(0, paket[i].length);
      }
      for (let i = 0; i < jenispemeriksaan.rows.length; i++) {
        pk = [];
        for (let j = 0; j < pakett[i].length; j++) {
          pk.push(await findItemjenispemeriksaanById(pakett[i][j]));
        }
        hasil.push({
          id: jenispemeriksaan.rows[i].id,
          nama_jenis_pemeriksaan: jenispemeriksaan.rows[i].nama_jenis_pemeriksaan,
          kode_jenis_pemeriksaan: jenispemeriksaan.rows[i].kode_jenis_pemeriksaan,
          tarif: jenispemeriksaan.rows[i].tarif,
          clinic_id: jenispemeriksaan.rows[i].clinic_id,
          status: jenispemeriksaan.rows[i].status,
          pemeriksaan: pk,
        });
      }

      const meta = {
        limit: parseInt(limit),
        page: page,
        total_page: Math.ceil(jenispemeriksaan.count / limit),
        total_data: jenispemeriksaan.count,
      };

      return ResponseHelper(res, 200, 'Data Jenis Pemeriksaan Berhasil Ditampilkan', hasil, meta);
    }
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jenis Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    const id = req.query.id || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let jenispemeriksaan = await findListJenispemeriksaannotoken(requirement, page, limit);
    if (limit == 'all') {
      var hasil = [];
      var paket = [];
      var pakett = [];
      var pk = [];
      for (let j = 0; j < jenispemeriksaan.length; j++) {
        paket.push(await findListPaketpemeriksaan(jenispemeriksaan[j].id));
        pakett.push(await findListPaketpemeriksaan(jenispemeriksaan[j].id));
      }
      for (let i = 0; i < jenispemeriksaan.length; i++) {
        for (let j = 0; j < paket[i].length; j++) {
          pakett[i].push(paket[i][j].m_item_pemeriksaan_id);
        }
        pakett[i].splice(0, paket[i].length);
      }
      for (let i = 0; i < jenispemeriksaan.length; i++) {
        pk = [];
        for (let j = 0; j < pakett[i].length; j++) {
          pk.push(await findItemjenispemeriksaanById(pakett[i][j]));
        }
        hasil.push({
          id: jenispemeriksaan[i].id,
          nama_jenis_pemeriksaan: jenispemeriksaan[i].nama_jenis_pemeriksaan,
          kode_jenis_pemeriksaan: jenispemeriksaan[i].kode_jenis_pemeriksaan,
          tarif: jenispemeriksaan[i].tarif,
          clinic_id: jenispemeriksaan[i].clinic_id,
          status: jenispemeriksaan[i].status,
          pemeriksaan: pk,
        });
      }

      const meta = {
        limit: limit,
        page: page,
        total_page: Math.ceil(jenispemeriksaan.count / limit),
        total_data: jenispemeriksaan.count,
      };

      return ResponseHelper(res, 200, 'Data Jenis Pemeriksaan Berhasil Ditampilkan', hasil, meta);
    } else {
      var hasil = [];
      var paket = [];
      var pakett = [];
      var pk = [];
      for (let j = 0; j < jenispemeriksaan.rows.length; j++) {
        paket.push(await findListPaketpemeriksaan(jenispemeriksaan.rows[j].id));
        pakett.push(await findListPaketpemeriksaan(jenispemeriksaan.rows[j].id));
      }
      for (let i = 0; i < jenispemeriksaan.rows.length; i++) {
        for (let j = 0; j < paket[i].length; j++) {
          pakett[i].push(paket[i][j].m_item_pemeriksaan_id);
        }
        pakett[i].splice(0, paket[i].length);
      }
      for (let i = 0; i < jenispemeriksaan.rows.length; i++) {
        pk = [];
        for (let j = 0; j < pakett[i].length; j++) {
          pk.push(await findItemjenispemeriksaanById(pakett[i][j]));
        }
        hasil.push({
          id: jenispemeriksaan.rows[i].id,
          nama_jenis_pemeriksaan: jenispemeriksaan.rows[i].nama_jenis_pemeriksaan,
          kode_jenis_pemeriksaan: jenispemeriksaan.rows[i].kode_jenis_pemeriksaan,
          tarif: jenispemeriksaan.rows[i].tarif,
          clinic_id: jenispemeriksaan.rows[i].clinic_id,
          status: jenispemeriksaan.rows[i].status,
          pemeriksaan: pk,
        });
      }

      const meta = {
        limit: parseInt(limit),
        page: page,
        total_page: Math.ceil(jenispemeriksaan.count / limit),
        total_data: jenispemeriksaan.count,
      };

      return ResponseHelper(res, 200, 'Data Jenis Pemeriksaan Berhasil Ditampilkan', hasil, meta);
    }
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jenis Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const getlog = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    const ids = JSON.parse(req.query.ids) || [1];
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (ids.length) requirement.ids = ids;
    if (status) requirement.status = status;

    let jenispemeriksaan = await findListJenispemeriksaanlogistik(requirement, page, limit);
    if (limit == 'all') {
      var hasil = [];
      var paket = [];
      var pakett = [];
      var pk = [];
      for (let j = 0; j < jenispemeriksaan.length; j++) {
        paket.push(await findListPaketpemeriksaan(jenispemeriksaan[j].id));
        pakett.push(await findListPaketpemeriksaan(jenispemeriksaan[j].id));
      }
      for (let i = 0; i < jenispemeriksaan.length; i++) {
        for (let j = 0; j < paket[i].length; j++) {
          pakett[i].push(paket[i][j].m_item_pemeriksaan_id);
        }
        pakett[i].splice(0, paket[i].length);
      }
      for (let i = 0; i < jenispemeriksaan.length; i++) {
        pk = [];
        for (let j = 0; j < pakett[i].length; j++) {
          pk.push(await findItemjenispemeriksaanById(pakett[i][j]));
        }
        hasil.push({
          id: jenispemeriksaan[i].id,
          nama_jenis_pemeriksaan: jenispemeriksaan[i].nama_jenis_pemeriksaan,
          kode_jenis_pemeriksaan: jenispemeriksaan[i].kode_jenis_pemeriksaan,
          tarif: jenispemeriksaan[i].tarif,
          clinic_id: jenispemeriksaan[i].clinic_id,
          status: jenispemeriksaan[i].status,
          pemeriksaan: pk,
        });
      }

      const meta = {
        limit: limit,
        page: page,
        total_page: Math.ceil(jenispemeriksaan.count / limit),
        total_data: jenispemeriksaan.count,
      };

      return ResponseHelper(res, 200, 'Data Jenis Pemeriksaan Berhasil Ditampilkan', hasil, meta);
    } else {
      var hasil = [];
      var paket = [];
      var pakett = [];
      var pk = [];
      for (let j = 0; j < jenispemeriksaan.rows.length; j++) {
        paket.push(await findListPaketpemeriksaan(jenispemeriksaan.rows[j].id));
        pakett.push(await findListPaketpemeriksaan(jenispemeriksaan.rows[j].id));
      }
      for (let i = 0; i < jenispemeriksaan.rows.length; i++) {
        for (let j = 0; j < paket[i].length; j++) {
          pakett[i].push(paket[i][j].m_item_pemeriksaan_id);
        }
        pakett[i].splice(0, paket[i].length);
      }
      for (let i = 0; i < jenispemeriksaan.rows.length; i++) {
        pk = [];
        for (let j = 0; j < pakett[i].length; j++) {
          pk.push(await findItemjenispemeriksaanById(pakett[i][j]));
        }
        hasil.push({
          id: jenispemeriksaan.rows[i].id,
          nama_jenis_pemeriksaan: jenispemeriksaan.rows[i].nama_jenis_pemeriksaan,
          kode_jenis_pemeriksaan: jenispemeriksaan.rows[i].kode_jenis_pemeriksaan,
          tarif: jenispemeriksaan.rows[i].tarif,
          clinic_id: jenispemeriksaan.rows[i].clinic_id,
          status: jenispemeriksaan.rows[i].status,
          pemeriksaan: pk,
        });
      }

      const meta = {
        limit: parseInt(limit),
        page: page,
        total_page: Math.ceil(jenispemeriksaan.count / limit),
        total_data: jenispemeriksaan.count,
      };

      return ResponseHelper(res, 200, 'Data Jenis Pemeriksaan Berhasil Ditampilkan', hasil, meta);
    }
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jenis Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const findid = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    // const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let jenis = await findJenispemeriksaanById(id);

    // let jenispemeriksaan = await findJenispemeriksaan(requirement, page, limit);
    // // check jenis pemeriksaan is exist
    // if (jenispemeriksaan.rows.length == 0) {
    //   return ResponseHelper(res, 409, 'Jenis_pemeriksaan Tidak Tersedia', [
    //     { message: 'Jenis_pemeriksaan Tidak Tersedia', param: 'nama_jenis_pemeriksaan' },
    //   ]);
    // }

    // var hasil = []
    // var paket = []
    // var pakett = []
    // var pk = []
    // for (let j = 0; j < jenispemeriksaan.rows.length; j++) {
    //   paket.push(await findListPaketpemeriksaan(jenispemeriksaan.rows[j].id))
    //   pakett.push(await findListPaketpemeriksaan(jenispemeriksaan.rows[j].id))
    // }
    // for (let i = 0; i < jenispemeriksaan.rows.length; i++) {
    //   for (let j = 0; j < paket[i].length; j++) {
    //     pakett[i].push(paket[i][j].m_item_pemeriksaan_id)
    //   }
    //   pakett[i].splice(0,paket[i].length)
    // }
    // for (let i = 0; i < jenispemeriksaan.rows.length; i++) {
    //   pk = []
    //   for (let j = 0; j < pakett[i].length; j++) {
    //     pk.push(await findItemjenispemeriksaanById(pakett[i][j]))
    //   }
    //   hasil.push({
    //     "id":jenispemeriksaan.rows[i].id,
    //     "nama_jenis_pemeriksaan":jenispemeriksaan.rows[i].nama_jenis_pemeriksaan,
    //     "kode_jenis_pemeriksaan":jenispemeriksaan.rows[i].kode_jenis_pemeriksaan,
    //     "tarif":jenispemeriksaan.rows[i].tarif,
    //     "clinic_id":jenispemeriksaan.rows[i].clinic_id,
    //     "status":jenispemeriksaan.rows[i].status,
    //     "pemeriksaan":pk
    //   })
    // }

    // const meta = {
    //   limit: limit,
    //   page: page,
    //   total_page: Math.ceil(jenispemeriksaan.count / limit),
    //   total_data: jenispemeriksaan.count,
    // };

    return ResponseHelper(res, 200, 'Data Jenis Pemeriksaan Berhasil Ditampilkan', jenis);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jenis Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const find = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    // const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let jenispemeriksaan = await findJenispemeriksaan(requirement, page, limit);
    if (limit == 'all') {
      // check jenis pemeriksaan is exist
      if (jenispemeriksaan.length == 0) {
        return ResponseHelper(res, 409, 'Data Jenis Pemeriksaan Tidak Tersedia', [
          { message: 'Data Jenis Pemeriksaan Tidak Tersedia', param: 'nama_jenis_pemeriksaan' },
        ]);
      }

      var hasil = [];
      var paket = [];
      var pakett = [];
      var pk = [];
      for (let j = 0; j < jenispemeriksaan.length; j++) {
        paket.push(await findListPaketpemeriksaan(jenispemeriksaan[j].id));
        pakett.push(await findListPaketpemeriksaan(jenispemeriksaan[j].id));
      }
      for (let i = 0; i < jenispemeriksaan.length; i++) {
        for (let j = 0; j < paket[i].length; j++) {
          pakett[i].push(paket[i][j].m_item_pemeriksaan_id);
        }
        pakett[i].splice(0, paket[i].length);
      }
      for (let i = 0; i < jenispemeriksaan.length; i++) {
        pk = [];
        for (let j = 0; j < pakett[i].length; j++) {
          pk.push(await findItemjenispemeriksaanById(pakett[i][j]));
        }
        hasil.push({
          id: jenispemeriksaan[i].id,
          nama_jenis_pemeriksaan: jenispemeriksaan[i].nama_jenis_pemeriksaan,
          kode_jenis_pemeriksaan: jenispemeriksaan[i].kode_jenis_pemeriksaan,
          tarif: jenispemeriksaan[i].tarif,
          clinic_id: jenispemeriksaan[i].clinic_id,
          status: jenispemeriksaan[i].status,
          pemeriksaan: pk,
        });
      }

      const meta = {
        limit: limit,
        page: page,
        total_page: Math.ceil(jenispemeriksaan.count / limit),
        total_data: jenispemeriksaan.count,
      };

      return ResponseHelper(res, 200, 'Data Jenis Pemeriksaan Berhasil Ditampilkan', hasil, meta);
    } else {
      // check jenis pemeriksaan is exist
      if (jenispemeriksaan.rows.length == 0) {
        return ResponseHelper(res, 409, 'Data Jenis Pemeriksaan Tidak Tersedia', [
          { message: 'Data Jenis Pemeriksaan Tidak Tersedia', param: 'nama_jenis_pemeriksaan' },
        ]);
      }

      var hasil = [];
      var paket = [];
      var pakett = [];
      var pk = [];
      for (let j = 0; j < jenispemeriksaan.rows.length; j++) {
        paket.push(await findListPaketpemeriksaan(jenispemeriksaan.rows[j].id));
        pakett.push(await findListPaketpemeriksaan(jenispemeriksaan.rows[j].id));
      }
      for (let i = 0; i < jenispemeriksaan.rows.length; i++) {
        for (let j = 0; j < paket[i].length; j++) {
          pakett[i].push(paket[i][j].m_item_pemeriksaan_id);
        }
        pakett[i].splice(0, paket[i].length);
      }
      for (let i = 0; i < jenispemeriksaan.rows.length; i++) {
        pk = [];
        for (let j = 0; j < pakett[i].length; j++) {
          pk.push(await findItemjenispemeriksaanById(pakett[i][j]));
        }
        hasil.push({
          id: jenispemeriksaan.rows[i].id,
          nama_jenis_pemeriksaan: jenispemeriksaan.rows[i].nama_jenis_pemeriksaan,
          kode_jenis_pemeriksaan: jenispemeriksaan.rows[i].kode_jenis_pemeriksaan,
          tarif: jenispemeriksaan.rows[i].tarif,
          clinic_id: jenispemeriksaan.rows[i].clinic_id,
          status: jenispemeriksaan.rows[i].status,
          pemeriksaan: pk,
        });
      }

      const meta = {
        limit: parseInt(limit),
        page: page,
        total_page: Math.ceil(jenispemeriksaan.count / limit),
        total_data: jenispemeriksaan.count,
      };

      return ResponseHelper(res, 200, 'Data Jenis Pemeriksaan Berhasil Ditampilkan', hasil, meta);
    }
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jenis Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const findkode = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    // const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let jenispemeriksaan = await findListJenispemeriksaankode(requirement, page, limit);
    // check jenis pemeriksaan is exist
    if (jenispemeriksaan.rows.length == 0) {
      return ResponseHelper(res, 409, 'Data Jenis Pemeriksaan Tidak Tersedia', [
        { message: 'Data Jenis Pemeriksaan Tidak Tersedia', param: 'nama_jenis_pemeriksaan' },
      ]);
    }

    var hasil = [];
    var paket = [];
    var pakett = [];
    var pk = [];
    for (let j = 0; j < jenispemeriksaan.rows.length; j++) {
      paket.push(await findListPaketpemeriksaan(jenispemeriksaan.rows[j].id));
      pakett.push(await findListPaketpemeriksaan(jenispemeriksaan.rows[j].id));
    }
    for (let i = 0; i < jenispemeriksaan.rows.length; i++) {
      for (let j = 0; j < paket[i].length; j++) {
        pakett[i].push(paket[i][j].m_item_pemeriksaan_id);
      }
      pakett[i].splice(0, paket[i].length);
    }
    for (let i = 0; i < jenispemeriksaan.rows.length; i++) {
      pk = [];
      for (let j = 0; j < pakett[i].length; j++) {
        pk.push(await findItemjenispemeriksaanById(pakett[i][j]));
      }
      hasil.push({
        id: jenispemeriksaan.rows[i].id,
        nama_jenis_pemeriksaan: jenispemeriksaan.rows[i].nama_jenis_pemeriksaan,
        kode_jenis_pemeriksaan: jenispemeriksaan.rows[i].kode_jenis_pemeriksaan,
        tarif: jenispemeriksaan.rows[i].tarif,
        clinic_id: jenispemeriksaan.rows[i].clinic_id,
        status: jenispemeriksaan.rows[i].status,
        pemeriksaan: pk,
      });
    }

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(jenispemeriksaan.count / limit),
      total_data: jenispemeriksaan.count,
    };

    return ResponseHelper(res, 200, 'Data Jenis Pemeriksaan Berhasil Ditampilkan', hasil, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jenis Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    var user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    function isNumber(n) {
      return !isNaN(parseFloat(n)) && !isNaN(n - 0);
    }
    if (req.body.tarif <0 ) {
      return ResponseHelper(res, 422, 'Tarif Harus Lebih Dari 0', [
        { message: 'Tarif Harus Lebih Dari 0', param: 'tarif' },
      ]);
    }
    if (!isNumber(req.body.tarif)) {
      return ResponseHelper(res, 422, 'Tarif Harus Diisi Angka', [
        { message: 'Tarif Harus Diisi Angka', param: 'tarif' },
      ]);
    }

    // Check jenis pemeriksaan is exist
    let checkJenispemeriksaannama = await findOneJenispemeriksaan({
      where: { nama_jenis_pemeriksaan: req.body.nama_jenis_pemeriksaan, clinic_id: user.clinic_id },
    });
    if (checkJenispemeriksaannama) {
      return ResponseHelper(res, 409, 'Nama Jenis Pemeriksaan Sudah Terdaftar', [
        { message: 'Nama Jenis Pemeriksaan Sudah Terdaftar', param: 'nama_jenis_pemeriksaan' },
      ]);
    }
    // Check code is exist
    let checkJenispemeriksaankode = await findOneJenispemeriksaan({
      where: { kode_jenis_pemeriksaan: req.body.kode_jenis_pemeriksaan, clinic_id: user.clinic_id },
    });
    if (checkJenispemeriksaankode) {
      return ResponseHelper(res, 409, 'Kode Jenis Pemeriksaan Sudah Terdaftar', [
        { message: 'Kode Jenis Pemeriksaan Sudah Terdaftar', param: 'kode_jenis_pemeriksaan' },
      ]);
    }
    // check null nama item
    for (let i = 0; i < req.body.items.length; i++) {
      if(req.body.items[i].nama_item == null || req.body.items[i].nama_item == '' || req.body.items[i].nama_item == '-'){
        return ResponseHelper(res, 422, 'Nama Item Pemeriksaan Harus Diisi', [
          { message: 'Nama Item Pemeriksaan Harus Diisi', param: 'nama_item' },
        ]);
      }
      if(req.body.items[i].kode_item == null || req.body.items[i].kode_item == '' || req.body.items[i].kode_item == '-'){
        return ResponseHelper(res, 422, 'Kode Item Pemeriksaan Harus Diisi', [
          { message: 'Kode Item Pemeriksaan Harus Diisi', param: 'kode_item' },
        ]);
      }
      if(req.body.items[i].nilai_rujukan == null || req.body.items[i].nilai_rujukan == '' ){
        return ResponseHelper(res, 422, 'Nilai Rujukan Harus Diisi', [
          { message: 'Nilai Rujukan Harus Diisi', param: 'nilai_rujukan' },
        ]);
      }
      if(req.body.items[i].satuan == null || req.body.items[i].satuan == '' ){
        return ResponseHelper(res, 422, 'Satuan Harus Diisi', [
          { message: 'Satuan Harus Diisi', param: 'satuan' },
        ]);
      }
    }

    console.log(req.body.items);
    let itempemeriksaan;
    let mapping;
    const jenispemeriksaan = await createJenispemeriksaan({
      ...req.body,
      clinic_id: user.clinic_id,
    });
    for (let i = 0; i < req.body.items.length; i++) {
      if (req.body.items[i].id) {
        mapping = await createPaketpemeriksaan({
          m_item_pemeriksaan_id: req.body.items[i].id,
          m_jenis_pemeriksaan_id: jenispemeriksaan.id,
        });
      } else {
        itempemeriksaan = await createItemjenispemeriksaan({
          item: req.body.items[i].nama_item,
          kode: req.body.items[i].kode_item,
          nilai_rujukan: req.body.items[i].nilai_rujukan,
          satuan: req.body.items[i].satuan,
          clinic_id: user.clinic_id,
        });
        mapping = await createPaketpemeriksaan({
          m_item_pemeriksaan_id: itempemeriksaan.id,
          m_jenis_pemeriksaan_id: jenispemeriksaan.id,
          clinic_id: user.clinic_id,
        });
      }
    }

    return ResponseHelper(res, 201, 'Data Jenis Pemeriksaan Berhasil Ditambahkan', jenispemeriksaan);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jenis Pemeriksaan Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check jenis pemeriksaan is exist
    let checkJenispemeriksaan = await findJenispemeriksaanById(id);
    if (!checkJenispemeriksaan) {
      return ResponseHelper(res, 409, 'Data Jenis Pemeriksaan Tidak Tersedia', [
        { message: 'Data Jenis Pemeriksaan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    
    function isNumber(n) {
      return !isNaN(parseFloat(n)) && !isNaN(n - 0);
    }
    // check tarif
    if (req.body.tarif <0 ) {
      return ResponseHelper(res, 422, 'Tarif Harus Lebih Dari 0', [
        { message: 'Tarif Harus Lebih Dari 0', param: 'tarif' },
      ]);
    }
    if (!isNumber(req.body.tarif)) {
      return ResponseHelper(res, 422, 'Tarif Harus Diisi Angka', [
        { message: 'Tarif Harus Diisi Angka', param: 'tarif' },
      ]);
    }

    // Check jenis pemeriksaan is exist
    let checkJenispemeriksaannama = await findOneJenispemeriksaan({
      where: {
        nama_jenis_pemeriksaan: req.body.nama_jenis_pemeriksaan,
        clinic_id: user.clinic_id,
        id: { [Op.not]: id },
      },
    });
    if (checkJenispemeriksaannama) {
      return ResponseHelper(res, 409, 'Nama Jenis Pemeriksaan Sudah Terdaftar', [
        { message: 'Nama Jenis Pemeriksaan Sudah Terdaftar', param: 'nama_jenis_pemeriksaan' },
      ]);
    }
    // Check code is exist
    let checkJenispemeriksaankode = await findOneJenispemeriksaan({
      where: {
        kode_jenis_pemeriksaan: req.body.kode_jenis_pemeriksaan,
        clinic_id: user.clinic_id,
        id: { [Op.not]: id },
      },
    });
    if (checkJenispemeriksaankode) {
      return ResponseHelper(res, 409, 'Kode Jenis Pemeriksaan Sudah Terdaftar', [
        { message: 'Kode Jenis Pemeriksaan Sudah Terdaftar', param: 'kode_jenis_pemeriksaan' },
      ]);
    }
    // check null nama item
    for (let i = 0; i < req.body.items.length; i++) {
      if(req.body.items[i].nama_item == null || req.body.items[i].nama_item == '' || req.body.items[i].nama_item == '-'){
        return ResponseHelper(res, 422, 'Nama Item Pemeriksaan Harus Diisi', [
          { message: 'Nama Item Pemeriksaan Harus Diisi', param: 'nama_item' },
        ]);
      }
      if(req.body.items[i].kode_item == null || req.body.items[i].kode_item == '' || req.body.items[i].kode_item == '-'){
        return ResponseHelper(res, 422, 'Kode Item Pemeriksaan Harus Diisi', [
          { message: 'Kode Item Pemeriksaan Harus Diisi', param: 'kode_item' },
        ]);
      }
      if(req.body.items[i].nilai_rujukan == null || req.body.items[i].nilai_rujukan == '' ){
        return ResponseHelper(res, 422, 'Nilai Rujukan Harus Diisi', [
          { message: 'Nilai Rujukan Harus Diisi', param: 'nilai_rujukan' },
        ]);
      }
      if(req.body.items[i].satuan == null || req.body.items[i].satuan == '' ){
        return ResponseHelper(res, 422, 'Satuan Harus Diisi', [
          { message: 'Satuan Harus Diisi', param: 'satuan' },
        ]);
      }
    }

    const pak = await findListPaketpemeriksaan(id);
    for (let i = 0; i < pak.length; i++) {
      await deletePaketpemeriksaan(pak[i].id);
    }

    // console.log(pak.length);

    let itempemeriksaan;
    let mapping;
    // Update jenis pemeriksaan
    await updateJenispemeriksaan({ ...req.body }, { where: { id } });
    for (let i = 0; i < req.body.items.length; i++) {
      if (req.body.items[i].id) {
        mapping = await createPaketpemeriksaan({
          m_item_pemeriksaan_id: req.body.items[i].id,
          m_jenis_pemeriksaan_id: id,
        });
      } else {
        itempemeriksaan = await createItemjenispemeriksaan({
          item: req.body.items[i].nama_item,
          kode: req.body.items[i].kode_item,
          nilai_rujukan: req.body.items[i].nilai_rujukan,
          satuan: req.body.items[i].satuan,
          clinic_id: user.clinic_id,
        });
        mapping = await createPaketpemeriksaan({
          m_item_pemeriksaan_id: itempemeriksaan.id,
          m_jenis_pemeriksaan_id: id,
          clinic_id: user.clinic_id,
        });
      }
    }

    const result = await findJenispemeriksaanById(id);

    return ResponseHelper(res, 201, 'Data Jenis Pemeriksaan Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jenis Pemeriksaan Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check jadwal dokter Exist Or Not
    let checkJenispemeriksaan = await findJenispemeriksaanById(id);
    if (!checkJenispemeriksaan) {
      return ResponseHelper(res, 409, 'Data Jenis Pemeriksaan Tidak Tersedia', [
        { message: 'Data Jenis Pemeriksaan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete jenis pemeriksaan
    let users = await findUserById(userId);

    await updateJenispemeriksaan({ deletedBy: users }, { where: { id } });

    await deleteJenispemeriksaan(id);

    return ResponseHelper(res, 201, 'Data Jenis Pemeriksaan Berhasil Dihapus', {
      id: Number(id),
    });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jenis Pemeriksaan Gagal Dihapus', error.message);
  }
};

const upload = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    console.log(req.files.import);

    // const mantap = req.files.fileName
    // const mantatap = mantap
    // console.log(mantatap.toString('utf8'));
    
    // Check role admin
    let user = await findUserById(user_id);

    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }
    
    // console.log(path.dirname(__filename)+"\\myFile.xlsx");
    const workSheetsFromFile = xlsx.parse(req.files.import.data);
    // console.log(workSheetsFromFile[0].data);
    var arr = []
    workSheetsFromFile.forEach((row) => {
      // console.log(row);
      row.data.forEach(async (data, index) => {
        // console.log(index);
        // console.log(data);
        let object = {};
        if (index > 8) {
          row.data[8].forEach(async (header, index) => {
            object[header.replace('*','')] = data[index];
          });
          // console.log('object', object);
          if (object.kode_jenis_pemeriksaan && object.tarif) {
            arr.push(object)
          }
          //   const tes = await createCobaImport(object);
          // const tes = updateJenispemeriksaan(
          //   { tarif: object.tarif },
          //   { where: { kode_jenis_pemeriksaan: object.kode_jenis_pemeriksaan } }
          // );
          // arr.push(tes)
        }
      });
      console.log(arr);
      //   console.log(row);
      //   console.log(object);
    });
    // console.log(object);
    // console.log(arr[0].kode_jenis_pemeriksaan, arr[0].tarif);
    for (let i = 0; i < arr.length; i++) {
      console.log(arr[i].kode_jenis_pemeriksaan);
      const cek = await findOneJenispemeriksaan({where:{kode_jenis_pemeriksaan:arr[i].kode_jenis_pemeriksaan, clinic_id:user.clinic_id}})
      // console.log(cek);
      if (!cek) {
        return ResponseHelper(res, 409, 'Error: baris ke '+(i+2)+', kode '+arr[i].kode_jenis_pemeriksaan+' tidak Ditampilkan', arr[i].kode_jenis_pemeriksaan);
      }
    }
    for (let i = 0; i < arr.length; i++) {
      const tes = await updateJenispemeriksaan(
        {tarif:arr[i].tarif},{where:{kode_jenis_pemeriksaan:arr[i].kode_jenis_pemeriksaan, clinic_id:user.clinic_id}}
      )
    }
    return ResponseHelper(res, 201, 'Data Jenis Pemeriksaan Berhasil Diimport');
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jenis Pemeriksaan Gagal Diimport', error.message);
  }
};

export { get, get1, getlog, findid, find, findkode, add, update, deleted, upload };
