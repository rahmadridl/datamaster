import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'jenispemeriksaan': {
      return [
        body('nama_jenis_pemeriksaan').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('kode_jenis_pemeriksaan').not().isEmpty().withMessage('Kode Harus Diisi'),
        // body('items.kode_item').not().isEmpty().withMessage('kode_item Harus Diisi'),
        // body('items.nama_item').not().isEmpty().withMessage('nama_item Harus Diisi'),
        body('tarif').not().isEmpty().withMessage('Tarif Harus Diisi'),
        // body('tarif').isNumeric().withMessage('Tarif Harus Diisi Angka'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
