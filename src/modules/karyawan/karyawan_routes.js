import { get, add, update, deleted } from './karyawan_controller.js';
import validator from './karyawan_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const KaryawanRoutes = (app, prefix) => {
  app.route(`${prefix}/karyawan`).get(AuthMiddleware, get);
  app.route(`${prefix}/karyawan/add`).post(AuthMiddleware, validator('karyawan'), add);
  app.route(`${prefix}/karyawan/update/:id`).patch(AuthMiddleware, validator('karyawan'), update);
  app.route(`${prefix}/karyawan/delete/:id`).delete(AuthMiddleware, deleted);
};

export { KaryawanRoutes };
