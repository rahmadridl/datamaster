import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import { Op } from 'sequelize';
import {
  createKaryawan,
  findListKaryawan,
  findKaryawanById,
  updateKaryawan,
  deleteKaryawan,
  findOneKaryawan
} from './karyawan_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'not allowed to access', [
        { message: 'not allowed to access', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let karyawan = await findListKaryawan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(karyawan.count / limit),
      total_data: karyawan.count,
    };

    return ResponseHelper(res, 200, 'success get list data karyawan', karyawan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed get karyawan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'not allowed to access', [
    //     { message: 'not allowed to access', param: 'id' },
    //   ]);
    // }
    // Check karyawan is exist
    let checkKaryawanemail = await findOneKaryawan({
      where:{email : req.body.email, clinic_id : user.clinic_id}
    });
    if (checkKaryawanemail) {
      return ResponseHelper(res, 409, 'email already registered', [
        { message: 'email already registered', param: 'email' },
      ]);
    }
    // Check karyawan is exist
    let checkKaryawan = await findOneKaryawan({
      where:{nama_karyawan : req.body.nama_karyawan, clinic_id : user.clinic_id}
    });
    if (checkKaryawan) {
      return ResponseHelper(res, 409, 'nama_karyawan already registered', [
        { message: 'nama_karyawan already registered', param: 'nama_karyawan' },
      ]);
    }
    // Check karyawan code is exist
    let checkKaryawankode = await findOneKaryawan({
      where:{kode_karyawan : req.body.kode_karyawan, clinic_id : user.clinic_id}
    });
    if (checkKaryawankode) {
      return ResponseHelper(res, 409, 'kode_karyawan already registered', [
        { message: 'kode_karyawan already registered', param: 'kode_karyawan' },
      ]);
    }
    const karyawan = await createKaryawan({ ...req.body });
    await updateKaryawan(
      {clinic_id:user.clinic_id},
      {where: {id: karyawan.id}}
    )
    return ResponseHelper(res, 201, 'success create new karyawan', karyawan);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed create new karyawan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check karyawan is exist
    let checkKaryawan = await findKaryawanById(id);
    if (!checkKaryawan) {
      return ResponseHelper(res, 409, 'Karyawan is not exist', [
        { message: 'Karyawan is not exist', param: 'id' },
      ]);
    }
    
    // Check role admin
    const user = await findUserById(user_id);
    
    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'not allowed to access', [
        { message: 'not allowed to access', param: 'id' },
      ]);
    }
    
    // Check karyawan email is exist
    let checkKaryawanemail = await findOneKaryawan({
      where:{email : req.body.email, clinic_id : user.clinic_id,
        id:{[Op.not]: id}}
    });
    if (checkKaryawanemail) {
      return ResponseHelper(res, 409, 'email already registered', [
        { message: 'email already registered', param: 'email' },
      ]);
    }
    // Check karyawan is exist
    let checkKaryawannama = await findOneKaryawan({
      where:{nama_karyawan : req.body.nama_karyawan, clinic_id : user.clinic_id,
        id:{[Op.not]: id}}
    });
    if (checkKaryawannama) {
      return ResponseHelper(res, 409, 'nama_karyawan already registered', [
        { message: 'nama_karyawan already registered', param: 'nama_karyawan' },
      ]);
    }
    // Check karyawan code is exist
    let checkKaryawankode = await findOneKaryawan({
      where:{kode_karyawan : req.body.kode_karyawan, clinic_id : user.clinic_id,
        id:{[Op.not]: id}}
    });
    if (checkKaryawankode) {
      return ResponseHelper(res, 409, 'kode_karyawan already registered', [
        { message: 'kode_karyawan already registered', param: 'kode_karyawan' },
      ]);
    }
    
    // Update karyawan
    await updateKaryawan({ ...req.body }, { where: { id } });

    const result = await findKaryawanById(id);

    return ResponseHelper(res, 201, 'success updated selected Karyawan', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed updated selected Karyawan', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check karyawan Exist Or Not
    let checkKaryawan = await findKaryawanById(id);
    if (!checkKaryawan) {
      return ResponseHelper(res, 409, 'Karyawan Is Not Exist In Database', [
        { message: 'Karyawan Is Not Exist In Database', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'not allowed to access', [
        { message: 'not allowed to access', param: 'id' },
      ]);
    }

    // Delete karyawan
    let users = await findUserById(userId);

    await updateKaryawan({ deletedBy: users }, { where: { id } });

    await deleteKaryawan(id);

    return ResponseHelper(res, 201, 'Success Deleted Selected Karyawan', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Failed Deleted Selected Karyawan', error.message);
  }
};

export { get, add, update, deleted };
