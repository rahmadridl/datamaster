import db from '../../database/models/index.js';
const { Karyawan } = db;
import { Op } from 'sequelize';

// Find one karyawan by id
const findKaryawanById = async (id) => {
  try {
    let result = await Karyawan.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findKaryawanById', error);
    throw new Error(error);
  }
};

// Find one karyawan by filter
const findOneKaryawan = async (filter) => {
  try {
    let result = await Karyawan.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneKaryawan', error);
    throw new Error(error);
  }
};

// Find list karyawan
const findListKaryawan = async ({ search, status }, page, limit) => {
  try {
    let result = await Karyawan.findAndCountAll({
      where: {
        nama_karyawan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        clinic_id: status
      },order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListKaryawan', error);
    throw new Error(error);
  }
};

// Create new karyawan
const createKaryawan = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Karyawan.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createKaryawan', error);
    throw new Error(error);
  }
};

// Update karyawan
const updateKaryawan = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Karyawan.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateKaryawan', error);
    throw new Error(error);
  }
};

//delete karyawan
const deleteKaryawan = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Karyawan.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteKaryawan', error);
    throw new Error(error);
  }
};

export { findKaryawanById, findOneKaryawan, findListKaryawan, createKaryawan, updateKaryawan, deleteKaryawan };
