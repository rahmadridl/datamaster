import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'karyawan': {
      return [
        body('kode_karyawan').not().isEmpty().withMessage('kode can not be empty'),
        body('nama_karyawan').not().isEmpty().withMessage('name can not be empty'),
        body('email').not().isEmpty().withMessage('email can not be empty'),
        body('email').isEmail().withMessage('email does not match the format'),
        body('bidang').not().isEmpty().withMessage('bidang can not be empty'),
        // body('clinic_id').not().isEmpty().withMessage('clinic id can not be empty'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
