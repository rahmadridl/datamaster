import db from '../../database/models/index.js';
const { Seat } = db;
import { Op } from 'sequelize';

// Find one seat by id
const findSeatById = async (id) => {
  try {
    let result = await Seat.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findSeatById', error);
    throw new Error(error);
  }
};

// Find one Seat by filter
const findOneSeat = async (filter) => {
  try {
    let result = await Seat.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneSeat', error);
    throw new Error(error);
  }
};

// Find list Seat
const findListSeat = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Seat.findAll({
        where: {
          nama_seat: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Seat.findAndCountAll({
        where: {
          nama_seat: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListSeat', error);
    throw new Error(error);
  }
};

// Find list Seat
const findListSeatnotoken = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Seat.findAll({
        where: {
          nama_seat: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          // clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Seat.findAndCountAll({
        where: {
          nama_seat: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          // clinic_id: status,
          // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListSeat', error);
    throw new Error(error);
  }
};

// Create new Seat
const createSeat = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Seat.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createSeat', error);
    throw new Error(error);
  }
};

// Update Seat
const updateSeat = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Seat.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateSeat', error);
    throw new Error(error);
  }
};

//delete Seat
const deleteSeat = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Seat.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteSeat', error);
    throw new Error(error);
  }
};

export { findSeatById, findOneSeat, findListSeat, findListSeatnotoken, createSeat, updateSeat, deleteSeat };
