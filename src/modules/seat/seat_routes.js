import { get, add, update, deleted, getnotoken } from './seat_controller.js';
import validator from './seat_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const SeatRoutes = (app, prefix) => {
  app.route(`${prefix}/seat`).get(AuthMiddleware, get);
  app.route(`${prefix}/seatnotoken`).get(getnotoken);
  app.route(`${prefix}/seat/add`).post(AuthMiddleware, validator('seat'), add);
  app.route(`${prefix}/seat/update/:id`).patch(AuthMiddleware, validator('seat'), update);
  app.route(`${prefix}/seat/delete/:id`).delete(AuthMiddleware, deleted);
};

export { SeatRoutes };
