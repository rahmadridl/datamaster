import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findClinicById, findOneClinic } from '../clinic/clinic_repository.js';
import { findUserById } from '../user/user_repository.js';
import {
  createSeat,
  findListSeat,
  findSeatById,
  updateSeat,
  deleteSeat,
  findListSeatnotoken
} from './seat_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }
    
    const search = req.query.search || '';
    const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let seat = await findListSeat(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(seat.count / limit),
      total_data: seat.count,
    };

    return ResponseHelper(res, 200, 'Data Seat Berhasil Ditampilkan', limit=="all"?seat:seat.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Seat Gagal Ditampilkan', error.message);
  }
};

const getnotoken = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }
    console.log(user_id);
    const search = req.query.search || '';
    // const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    // if (status) requirement.status = status;

    let seat = await findListSeatnotoken(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(seat.count / limit),
      total_data: seat.count,
    };

    return ResponseHelper(res, 200, 'Data Seat Berhasil Ditampilkan', limit=="all"?seat:seat.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Seat Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // const clin = await findClinicById(user.clinic_id)

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }
    const seat = await createSeat({ ...req.body, clinic_id:user.clinic_id });
    // await updateSeat(
    //   {clinic_id:user.clinic_id},
    //   {where: {id: seat.id}}
    // )
    return ResponseHelper(res, 201, 'Data Seat Berhasil Ditambahkan', seat);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Seat Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check seat is exist
    let checkSeat = await findSeatById(id);
    if (!checkSeat) {
      return ResponseHelper(res, 409, 'Seat Tidak Tersedia', [
        { message: 'Seat Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Update seat
    await updateSeat({ ...req.body }, { where: { id } });

    const result = await findSeatById(id);

    return ResponseHelper(res, 201, 'Data Seat Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Seat Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check seat Exist Or Not
    let checkSeat = await findSeatById(id);
    if (!checkSeat) {
      return ResponseHelper(res, 409, 'Seat Tidak Tersedia', [
        { message: 'Seat Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete seat
    let users = await findUserById(userId);

    await updateSeat({ deletedBy: users }, { where: { id } });

    await deleteSeat(id);

    return ResponseHelper(res, 201, 'Data Seat Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Seat Gagal Dihapus', error.message);
  }
};

export { get, getnotoken, add, update, deleted };
