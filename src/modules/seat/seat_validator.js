import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'seat': {
      return [
        body('nama_seat').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('poli_id').not().isEmpty().withMessage('Poli id Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
