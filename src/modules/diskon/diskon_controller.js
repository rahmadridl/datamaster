import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import xlsx from 'node-xlsx';
import { Op } from 'sequelize';
import {
  createDiskon,
  findListDiskon,
  findDiskonById,
  updateDiskon,
  deleteDiskon,
  findOneDiskon,
  createDiskonpemeriksaan,
  findListDiskonpemeriksaan,
  findListPaketDiskonpemeriksaan,
  deleteDiskonpemeriksaan,
} from './diskon_repository.js';
import {
  findJenispemeriksaanById,
  findOneJenispemeriksaan,
} from '../jenispemeriksaan/jenispemeriksaan_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10'

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let dis = [];
    let jenis = [];
    let jenisnama = [];
    let pem;
    let jen;
    let diskon = await findListDiskon(requirement, page, limit);
    if (limit == 'all') {
      for (let i = 0; i < diskon.length; i++) {
        jen = JSON.parse(diskon[i].jenis_pemeriksaan_id);
        for (let j = 0; j < jen.length; j++) {
          pem = await findJenispemeriksaanById(jen[j]);
          if (pem.status == true) {
            jenis.push(pem.kode_jenis_pemeriksaan);
            jenisnama.push(pem.nama_jenis_pemeriksaan);
          }
        }
        dis.push({
          id: diskon[i].id,
          nama: diskon[i].nama,
          kode: diskon[i].kode,
          nominal: diskon[i].nominal,
          status: diskon[i].status,
          start: diskon[i].start,
          expired: diskon[i].expired,
          clinic_id: diskon[i].clinic_id,
          jenis_pemeriksaan_id: jenis,
          nama_jenis_pemeriksaan: jenisnama,
        });
        jenis = [];
        jenisnama = [];
      }

      const meta = {
        limit: limit,
        page: page,
        total_page: Math.ceil(diskon.count / limit),
        total_data: diskon.count,
      };

      return ResponseHelper(res, 200, 'Data Diskon Berhasil Ditampilkan', dis, meta);
    } else {
      for (let i = 0; i < diskon.rows.length; i++) {
        jen = JSON.parse(diskon.rows[i].jenis_pemeriksaan_id);
        for (let j = 0; j < jen.length; j++) {
          pem = await findJenispemeriksaanById(jen[j]);
          if (pem.status == true) {
            jenis.push(pem.kode_jenis_pemeriksaan);
            jenisnama.push(pem.nama_jenis_pemeriksaan);
          }
        }
        dis.push({
          id: diskon.rows[i].id,
          nama: diskon.rows[i].nama,
          kode: diskon.rows[i].kode,
          nominal: diskon.rows[i].nominal,
          status: diskon.rows[i].status,
          start: diskon.rows[i].start,
          expired: diskon.rows[i].expired,
          clinic_id: diskon.rows[i].clinic_id,
          jenis_pemeriksaan_id: jenis,
          nama_jenis_pemeriksaan: jenisnama,
        });
        jenis = [];
        jenisnama = [];
      }

      const meta = {
        limit: parseInt(limit),
        page: page,
        total_page: Math.ceil(diskon.count / limit),
        total_data: diskon.count,
      };

      return ResponseHelper(res, 200, 'Data Diskon Berhasil Ditampilkan', dis, meta);
    }
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diskon Gagal Ditampilkan', error.message);
  }
};

const findid = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    // const search = req.query.search || '';
    // const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (id) requirement.id = id;
    // if (status) requirement.status = status;

    let biy = [];
    let jenis = [];
    let jenisnama = [];
    let pem;
    let bi;
    // let biayalain = await findListBiayalain(requirement, page, limit);
    let diskon = await findDiskonById(id);

    // const meta = {
    //   limit: limit,
    //   page: page,
    //   total_page: Math.ceil(biayalain.count / limit),
    //   total_data: biayalain.count,
    // };

    return ResponseHelper(res, 200, 'Data Diskon Berhasil Ditampilkan', diskon);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diskon Gagal Ditampilkan', error.message);
  }
};

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let diskonpemeriksaan = await findListDiskonpemeriksaan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(diskonpemeriksaan.count / limit),
      total_data: diskonpemeriksaan.count,
    };

    return ResponseHelper(
      res,
      200,
      'Data Diskon Pemeriksaan Berhasil Ditampilkan',
      diskonpemeriksaan.rows,
      meta
    );
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diskon Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    function isNumber(n) {
      return !isNaN(parseFloat(n)) && !isNaN(n - 0);
    }
    if (req.body.nominal == '') {
      return ResponseHelper(res, 422, 'Nominal Harus Diisi', [
        { message: 'Nominal Harus Diisi', param: 'nominal' },
      ]);
    }
    if (req.body.nominal <0) {
      return ResponseHelper(res, 422, 'Nominal Harus Lebih Dari 0', [
        { message: 'Nominal Harus Lebih Dari 0', param: 'nominal' },
      ]);
    }
    if (!isNumber(req.body.nominal)) {
      return ResponseHelper(res, 422, 'Nominal Harus Diisi Angka', [
        { message: 'Nominal Harus Diisi Angka', param: 'nominal' },
      ]);
    }

    // Check diskon is exist
    let checkDiskonnama = await findOneDiskon({
      where: { nama: req.body.nama, clinic_id: user.clinic_id },
    });
    if (checkDiskonnama) {
      return ResponseHelper(res, 409, 'Nama Diskon Sudah Terdaftar', [
        { message: 'Nama Diskon Sudah Terdaftar', param: 'nama' },
      ]);
    }
    // Check code is exist
    let checkDiskonkode = await findOneDiskon({
      where: { kode: req.body.kode, clinic_id: user.clinic_id },
    });
    if (checkDiskonkode) {
      return ResponseHelper(res, 409, 'Kode Diskon Sudah Terdaftar', [
        { message: 'Kode Diskon Sudah Terdaftar', param: 'kode' },
      ]);
    }
    var arrkode = [];
    var diskon;
    for (let i = 0; i < req.body.pemeriksaan.length; i++) {
      const idkode = await findOneJenispemeriksaan({
        where: { kode_jenis_pemeriksaan: req.body.pemeriksaan[i].kode, clinic_id: user.clinic_id },
      });
      if (!idkode) {
        return ResponseHelper(res, 409, 'Kode Pemeriksaan Belum Terdaftar', [
        { message: 'Kode Pemeriksaan Belum Terdaftar', param: 'pemeriksaan.kode' }])
      }
      arrkode.push(idkode.id);
    }

    diskon = await createDiskon({
      ...req.body,
      jenis_pemeriksaan_id: arrkode,
      clinic_id: user.clinic_id,
    });

    //create paket diskon
    var dispaket;
    for (let i = 0; i < req.body.pemeriksaan.length; i++) {
      const idkode = await findOneJenispemeriksaan({
        where: { kode_jenis_pemeriksaan: req.body.pemeriksaan[i].kode, clinic_id: user.clinic_id },
      });
      dispaket = await createDiskonpemeriksaan({
        m_diskon_id: diskon.id,
        m_jenis_pemeriksaan_id: idkode.id,
      });
    }
    return ResponseHelper(res, 201, 'Data Diskon Berhasil Ditambahkan', diskon);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diskon Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check diskon is exist
    let checkDiskon = await findDiskonById(id);
    if (!checkDiskon) {
      return ResponseHelper(res, 409, 'Data Diskon Tidak Tersedia', [
        { message: 'Data Diskon Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    function isNumber(n) {
      return !isNaN(parseFloat(n)) && !isNaN(n - 0);
    }
    if (req.body.nominal == '') {
      return ResponseHelper(res, 422, 'Nominal Harus Diisi', [
        { message: 'Nominal Harus Diisi', param: 'nominal' },
      ]);
    }
    if (req.body.nominal <0 ) {
      return ResponseHelper(res, 422, 'Nominal Harus Lebih Dari 0', [
        { message: 'Nominal Harus Lebih Dari 0', param: 'nominal' },
      ]);
    }
    if (!isNumber(req.body.nominal)) {
      return ResponseHelper(res, 422, 'Nominal Harus Diisi Angka', [
        { message: 'Nominal Harus Diisi Angka', param: 'nominal' },
      ]);
    }


    // Check diskon is exist
    let checkDiskonnama = await findOneDiskon({
      where: { nama: req.body.nama, clinic_id: user.clinic_id, id: { [Op.not]: id } },
    });
    if (checkDiskonnama) {
      return ResponseHelper(res, 409, 'Nama Diskon Sudah Terdaftar', [
        { message: 'Nama Diskon Sudah Terdaftar', param: 'nama' },
      ]);
    }
    // Check code is exist
    let checkDiskonkode = await findOneDiskon({
      where: { kode: req.body.kode, clinic_id: user.clinic_id, id: { [Op.not]: id } },
    });
    if (checkDiskonkode) {
      return ResponseHelper(res, 409, 'Kode Diskon Sudah Terdaftar', [
        { message: 'Kode Diskon Sudah Terdaftar', param: 'kode' },
      ]);
    }

    var arrkode = [];
    for (let i = 0; i < req.body.pemeriksaan.length; i++) {
      const idkode = await findOneJenispemeriksaan({
        where: { kode_jenis_pemeriksaan: req.body.pemeriksaan[i].kode, clinic_id: user.clinic_id },
      });
      if (!idkode) {
        return ResponseHelper(res, 409, 'Kode Pemeriksaan Belum Terdaftar', [
        { message: 'Kode Pemeriksaan Belum Terdaftar', param: 'pemeriksaan.kode' }])
      }
      arrkode.push(idkode.id);
    }

    const pak = await findListPaketDiskonpemeriksaan(id);
    for (let i = 0; i < pak.length; i++) {
      await deleteDiskonpemeriksaan(pak[i].id);
    }
    let mapping;
    // Update diskon
    await updateDiskon({ ...req.body, jenis_pemeriksaan_id: arrkode }, { where: { id } });
    for (let i = 0; i < req.body.pemeriksaan.length; i++) {
      const idkode = await findOneJenispemeriksaan({
        where: { kode_jenis_pemeriksaan: req.body.pemeriksaan[i].kode, clinic_id: user.clinic_id },
      });
      mapping = await createDiskonpemeriksaan({
        m_diskon_id: id,
        m_jenis_pemeriksaan_id: idkode.id,
      });
    }
    const result = await findDiskonById(id);

    return ResponseHelper(res, 201, 'Data Diskon Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diskon Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check Diskon Exist Or Not
    let checkDiskon = await findDiskonById(id);
    if (!checkDiskon) {
      return ResponseHelper(res, 409, 'Data Diskon Tidak Tersedia', [
        { message: 'Data Diskon Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete diskon
    let users = await findUserById(userId);

    await updateDiskon({ deletedBy: users }, { where: { id } });

    await deleteDiskon(id);

    return ResponseHelper(res, 201, 'Data Diskon Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diskon Gagal Dihapus', error.message);
  }
};

const upload = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    // console.log(req.files.import);

    // const mantap = req.files.fileName
    // const mantatap = mantap
    // console.log(mantatap.toString('utf8'));
    
    // Check role admin
    let user = await findUserById(user_id);

    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }
    
    // console.log(path.dirname(__filename)+"\\myFile.xlsx");
    const workSheetsFromFile = xlsx.parse(req.files.import.data);
    // console.log(workSheetsFromFile[0].data);
    var arr = []
    workSheetsFromFile.forEach((row) => {
      // console.log(row);
      row.data.forEach(async (data, index) => {
        // console.log(index);
        // console.log(data);
        let object = {};
        if (index > 8) {
          row.data[8].forEach(async (header, index) => {
            object[header.replace('*','')] = data[index];
          });
          // console.log('object', object);
          if (object.kode && object.nominal) {
            arr.push(object)
          }
        }
      });
      console.log(arr);
    });
    for (let i = 0; i < arr.length; i++) {
      console.log(arr[i].kode);
      const cek = await findOneDiskon({where:{kode:arr[i].kode, clinic_id:user.clinic_id}})
      if (!cek) {
        return ResponseHelper(res, 409, 'Error: baris ke '+(i+2)+', kode '+arr[i].kode+' tidak ditemukan', arr[i].kode);
      }
    }
    for (let i = 0; i < arr.length; i++) {
      const tes = await updateDiskon(
        {nominal:arr[i].nominal},{where:{kode:arr[i].kode, clinic_id:user.clinic_id}}
      )
    }
    return ResponseHelper(res, 201, 'Data Diskon Berhasil Diubah');
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Diskon Gagal Diubah', error.message);
  }
};

export { get, findid, get1, add, update, deleted, upload };
