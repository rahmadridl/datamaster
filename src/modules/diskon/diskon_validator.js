import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'diskon': {
      return [
        body('kode').not().isEmpty().withMessage('Kode Harus Diisi'),
        body('nama').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('nominal').not().isEmpty().withMessage('Nominal Harus Diisi'),
        // body('nominal').isNumeric().withMessage('nominal must number'),
        body('expired').not().isEmpty().withMessage('Tanggal Berakhir Harus Diisi'),
        body('start').not().isEmpty().withMessage('Tanggal Mulai Harus Diisi'),
        body('pemeriksaan').not().isEmpty().withMessage('Pemeriksaan Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
