import { get, get1, add, update, deleted, findid, upload } from './diskon_controller.js';
import validator from './diskon_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const DiskonRoutes = (app, prefix) => {
  app.route(`${prefix}/diskon`).get(AuthMiddleware, get);
  app.route(`${prefix}/diskonpaketnotoken`).get(get1);
  app.route(`${prefix}/diskon/findid/:id`).get(findid);
  app.route(`${prefix}/diskon/import`).post(AuthMiddleware, upload);
  app.route(`${prefix}/diskon/add`).post(AuthMiddleware, validator('diskon'), add);
  app.route(`${prefix}/diskon/update/:id`).patch(AuthMiddleware, validator('diskon'), update);
  app.route(`${prefix}/diskon/delete/:id`).delete(AuthMiddleware, deleted);
};

export { DiskonRoutes };
