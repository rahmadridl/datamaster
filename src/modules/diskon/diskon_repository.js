import db from '../../database/models/index.js';
const { Diskon } = db;
const { Diskonpemeriksaan } = db;
import { Op } from 'sequelize';

// Find one diskon by id
const findDiskonById = async (id) => {
  try {
    let result = await Diskon.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findDiskonById', error);
    throw new Error(error);
  }
};

// Find one diskon by filter
const findOneDiskon = async (filter) => {
  try {
    let result = await Diskon.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneDiskon', error);
    throw new Error(error);
  }
};

// Find list diskon
const findListDiskon = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Diskon.findAll({
        where: {
          nama: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Diskon.findAndCountAll({
        where: {
          nama: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListDiskon', error);
    throw new Error(error);
  }
};

// Find list diskonpemeriksaan
const findListDiskonpemeriksaan = async (filter, page, limit) => {
  try {
    let result = await Diskonpemeriksaan.findAndCountAll({
      ...filter,
      order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPaketpemeriksaan', error);
    throw new Error(error);
  }
};

// Create new diskon
const createDiskon = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Diskon.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createDiskon', error);
    throw new Error(error);
  }
};

// Find list Biayalainpemeriksaan
const findListPaketDiskonpemeriksaan = async (id) => {
  try {
    let result = await Diskonpemeriksaan.findAll({
      where: {
        m_diskon_id: id,
        // clinic_id: status
      },
      order: [['id', 'DESC']],
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPaketpemeriksaan', error);
    throw new Error(error);
  }
};

// Create new paket diskon
const createDiskonpemeriksaan = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Diskonpemeriksaan.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createDiskon', error);
    throw new Error(error);
  }
};

// Update diskon
const updateDiskon = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Diskon.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateDiskon', error);
    throw new Error(error);
  }
};

//delete diskon
const deleteDiskon = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Diskon.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteDiskon', error);
    throw new Error(error);
  }
};

//delete diskon
const deleteDiskonpemeriksaan = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Diskonpemeriksaan.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteDiskon', error);
    throw new Error(error);
  }
};

export {
  findDiskonById,
  findOneDiskon,
  findListDiskon,
  findListPaketDiskonpemeriksaan,
  findListDiskonpemeriksaan,
  createDiskonpemeriksaan,
  createDiskon,
  updateDiskon,
  deleteDiskon,
  deleteDiskonpemeriksaan,
};
