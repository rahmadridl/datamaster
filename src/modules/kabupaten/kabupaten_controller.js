import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import {
  // createProvinsi,
  findListKabupaten,
  findKabupaten,
  // findKabupatenById,
  // updateProvinsi,
  // deleteProvinsi
} from './kabupaten_repository.js';
import { findProvinsiById } from '../provinsi/provinsi_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let kabupaten = await findListKabupaten(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(kabupaten.count / limit),
      total_data: kabupaten.count,
    };

    return ResponseHelper(res, 200, 'Data Kabupaten Berhasil Ditampilkan', kabupaten.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Kabupaten Gagal Ditampilkan', error.message);
  }
};

const find = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check provinsi is exist
    let checkProvinsi = await findProvinsiById(id);
    if (!checkProvinsi) {
      return ResponseHelper(res, 409, 'Data Provinsi Tidak Tersedia', [
        { message: 'Data Provinsi Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // const id = req.params;
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let kabupaten = await findKabupaten(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(kabupaten.count / limit),
      total_data: kabupaten.count,
    };

    return ResponseHelper(res, 200, 'Data Kabupaten Berhasil Ditampilkan', kabupaten.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Kabupaten Gagal Ditampilkan', error.message);
  }
};




    
export { get, find };
