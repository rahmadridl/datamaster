import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'kabupaten': {
      return [
        body('nama').not().isEmpty().withMessage('Nama Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
