import db from '../../database/models/index.js';
const { Kabupaten } = db;
import { Op } from 'sequelize';

// Find one kabupaten by id
const findKabupatenById = async (id) => {
  try {
    let result = await Kabupaten.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findKabupatenById', error);
    throw new Error(error);
  }
};

// Find one kabupaten by filter
const findOneKabupaten = async (filter) => {
  try {
    let result = await Kabupaten.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneKabupaten', error);
    throw new Error(error);
  }
};

// Find list kabupaten
const findListKabupaten = async ({ search, status }, page, limit) => {
  try {
    let result = await Kabupaten.findAndCountAll({
      where: {
        nama: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListKabupaten', error);
    throw new Error(error);
  }
};

// Find kabupaten
const findKabupaten = async ({ id, status }, page, limit) => {
  try {
    let result = await Kabupaten.findAndCountAll({
      where: {
        provinsi_id: id ? { [Op.like]: id } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListKabupaten', error);
    throw new Error(error);
  }
};

// Create new ruangan kategori
// const createKabupaten = async (data, transaction) => {
//   const t = transaction ? transaction : await db.sequelize.transaction();
//   try {
//     let result = await Provinsi.create(data, { transaction });
//     if (!transaction) t.commit();
//     return result;
//   } catch (error) {
//     if (!transaction) t.rollback();
//     console.error('[EXCEPTION] createProvinsi', error);
//     throw new Error(error);
//   }
// };

// Update ruangan kategori
// const updateProvinsi = async (data, filter, transaction) => {
//   const t = transaction ? transaction : await db.sequelize.transaction();
//   try {
//     let result = await Provinsi.update(data, { ...filter, transaction });
//     if (!transaction) t.commit();
//     return result;
//   } catch (error) {
//     if (!transaction) t.rollback();
//     console.error('[EXCEPTION] updateProvinsi', error);
//     throw new Error(error);
//   }
// };

//delete ruangan kategori
// const deleteProvinsi = async (productId, transaction) => {
//   const t = transaction ? transaction : await db.sequelize.transaction();
//   try {
//     let result = await Provinsi.destroy({ where: { id: productId }, transaction });
//     if (!transaction) t.commit();
//     return result;
//   } catch (error) {
//     if (!transaction) t.rollback();
//     console.error('[EXCEPTION] deleteProvinsi', error);
//     throw new Error(error);
//   }
// };

export { findKabupatenById, findOneKabupaten, findListKabupaten, findKabupaten,
  // createProvinsi, 
  // updateProvinsi, 
  // deleteProvinsi
 };
