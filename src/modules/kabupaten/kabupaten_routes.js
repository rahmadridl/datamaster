import { get,find, add, update, deleted } from './kabupaten_controller.js';
import validator from './kabupaten_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const KabupatenRoutes = (app, prefix) => {
  app.route(`${prefix}/kabupaten`).get(AuthMiddleware, get);
  app.route(`${prefix}/kabupaten/:id`).get(AuthMiddleware, find);
  // app.route(`${prefix}/provinsi/add`).post(AuthMiddleware, validator('provinsi'), add);
  // app.route(`${prefix}/provinsi/update/:id`).patch(AuthMiddleware, validator('provinsi'), update);
  // app.route(`${prefix}/provinsi/delete/:id`).delete(AuthMiddleware, deleted);
};

export { KabupatenRoutes };
