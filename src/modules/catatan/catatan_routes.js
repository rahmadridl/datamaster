import { get, get1, add, update, deleted, find, upload } from './catatan_controller.js';
import validator from './catatan_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const CatatanRoutes = (app, prefix) => {
  app.route(`${prefix}/catatan`).get(AuthMiddleware, get);
  app.route(`${prefix}/catatan/import`).post(AuthMiddleware, upload);
  app.route(`${prefix}/catatannotoken`).get(get1);
  app.route(`${prefix}/catatanidnotoken/:id`).get(find);
  app.route(`${prefix}/catatan/add`).post(AuthMiddleware, validator('catatan'), add);
  app.route(`${prefix}/catatan/update/:id`).patch(AuthMiddleware, validator('catatan'), update);
  app.route(`${prefix}/catatan/delete/:id`).delete(AuthMiddleware, deleted);
};

export { CatatanRoutes };
