import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import xlsx from 'node-xlsx';
import { Op } from 'sequelize';
import {
  createCatatan,
  findListCatatan,
  findCatatanById,
  updateCatatan,
  deleteCatatan,
  findOneCatatan
} from './catatan_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let catatan = await findListCatatan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(catatan.count / limit),
      total_data: catatan.count,
    };

    return ResponseHelper(res, 200, 'Data Catatan Berhasil Ditampilkan', limit=="all"?catatan:catatan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Catatan Gagal Ditampilkan', error.message);
  }
};

const find = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    // const search = req.query.search || '';
    // const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (id) requirement.id = id;
    // if (status) requirement.status = status;

    // let catatan = await findListCatatan(requirement, page, limit);
    let catatid = await findCatatanById(id)

    // const meta = {
    //   limit: limit,
    //   page: page,
    //   total_page: Math.ceil(catatan.count / limit),
    //   total_data: catatan.count,
    // };

    return ResponseHelper(res, 200, 'Data Catatan Berhasil Ditampilkan', catatid);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Catatan Gagal Ditampilkan', error.message);
  }
};

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    // let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let catatan = await findListCatatan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(catatan.count / limit),
      total_data: catatan.count,
    };

    return ResponseHelper(res, 200, 'Data Catatan Berhasil Ditampilkan', catatan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Catatan Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }
    
    const catatan = await createCatatan({ ...req.body });
    await updateCatatan(
      {clinic_id:user.clinic_id},
      {where: {id: catatan.id}}
    )
    return ResponseHelper(res, 201, 'Data Catatan Berhasil Ditambahkan', catatan);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Catatan Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check catatan is exist
    let checkCatatan = await findCatatanById(id);
    if (!checkCatatan) {
      return ResponseHelper(res, 409, 'Data Catatan Tidak Tersedia', [
        { message: 'Data Catatan Tidak Tersedia', param: 'id' },
      ]);
    }
    
    // Check role admin
    const user = await findUserById(user_id);
    
    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }
    
    // Update catatan
    await updateCatatan({ ...req.body }, { where: { id } });

    const result = await findCatatanById(id);

    return ResponseHelper(res, 201, 'Data Catatan Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Catatan Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check catatan Exist Or Not
    let checkCatatan = await findCatatanById(id);
    if (!checkCatatan) {
      return ResponseHelper(res, 409, 'Data Catatan Tidak Terdaftar', [
        { message: 'Data Catatan Tidak Terdaftar', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete catatan
    let users = await findUserById(userId);

    await updateCatatan({ deletedBy: users }, { where: { id } });

    await deleteCatatan(id);

    return ResponseHelper(res, 201, 'Data Catatan Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Catatan Gagal Dihapus', error.message);
  }
};

const upload = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    
    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }
    
    // console.log(path.dirname(__filename)+"\\myFile.xlsx");
    const workSheetsFromFile = xlsx.parse(req.files.import.data);
    // console.log(workSheetsFromFile[0].data);
    var arr = []
    workSheetsFromFile.forEach((row) => {
      row.data.forEach(async (data, index) => {
        let object = {};
        if (index > 8) {
          row.data[8].forEach(async (header, index) => {
            object[header.replace('*','')] = data[index];
          });
          if (object.catatan) {
            arr.push(object)
          }
        }
      });
      // console.log(arr);
    });
    for (let i = 0; i < arr.length; i++) {
      const cek = await findOneCatatan({where:{catatan:arr[i].catatan, clinic_id:user.clinic_id}})
      if (cek) {
        return ResponseHelper(res, 409, 'Error: baris ke '+(i+2)+', nama catatan '+arr[i].catatan+' sudah terdaftar', arr[i].catatan);
      }else{
        const tes = await createCatatan(
          {catatan:arr[i].catatan, clinic_id:user.clinic_id}
        )
      }
    }
    return ResponseHelper(res, 201, 'Data Catatan Berhasil Ditambahkan');
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Catatan Gagal Ditambahkan', error.message);
  }
};

export { get, get1, find, add, update, deleted, upload };
