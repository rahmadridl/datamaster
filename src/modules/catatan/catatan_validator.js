import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'catatan': {
      return [
        body('catatan').not().isEmpty().withMessage('Catatan Harus Diisi'),
        // body('nama_karyawan').not().isEmpty().withMessage('name Harus Diisi'),
        // body('email').not().isEmpty().withMessage('email Harus Diisi'),
        // body('email').isEmail().withMessage('email does not match the format'),
        // body('bidang').not().isEmpty().withMessage('bidang Harus Diisi'),
        // body('clinic_id').not().isEmpty().withMessage('clinic id Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
