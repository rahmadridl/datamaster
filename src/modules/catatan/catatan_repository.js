import db from '../../database/models/index.js';
const { Catatan } = db;
import { Op } from 'sequelize';

// Find one catatan by id
const findCatatanById = async (id) => {
  try {
    let result = await Catatan.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findCatatanById', error);
    throw new Error(error);
  }
};

// Find one catatan by filter
const findOneCatatan = async (filter) => {
  try {
    let result = await Catatan.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneCatatan', error);
    throw new Error(error);
  }
};

// Find list catatan
const findListCatatan = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Catatan.findAll({
        where: {
          catatan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Catatan.findAndCountAll({
        where: {
          catatan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListCatatan', error);
    throw new Error(error);
  }
};

// Create new catatan
const createCatatan = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Catatan.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createCatatan', error);
    throw new Error(error);
  }
};

// Update catatan
const updateCatatan = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Catatan.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateCatatan', error);
    throw new Error(error);
  }
};

//delete catatan
const deleteCatatan = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Catatan.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteCatatan', error);
    throw new Error(error);
  }
};

export {
  findCatatanById,
  findOneCatatan,
  findListCatatan,
  createCatatan,
  updateCatatan,
  deleteCatatan,
};
