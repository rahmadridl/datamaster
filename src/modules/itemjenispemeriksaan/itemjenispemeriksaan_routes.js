import { get, add, update, deleted, find } from './itemjenispemeriksaan_controller.js';
import validator from './itemjenispemeriksaan_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const ItemjenispemeriksaanRoutes = (app, prefix) => {
  app.route(`${prefix}/itemjenispemeriksaan`).get(AuthMiddleware, get);
  app.route(`${prefix}/itemjenispemeriksaan/:id`).get(AuthMiddleware, find);
  app.route(`${prefix}/itemjenispemeriksaan/add`).post(AuthMiddleware, validator('itemjenispemeriksaan'), add);
  app.route(`${prefix}/itemjenispemeriksaan/update/:id`).patch(AuthMiddleware, validator('itemjenispemeriksaan'), update);
  app.route(`${prefix}/itemjenispemeriksaan/delete/:id`).delete(AuthMiddleware, deleted);
};

export { ItemjenispemeriksaanRoutes };
