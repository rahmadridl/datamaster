import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import {
  createItemjenispemeriksaan,
  findListItemjenispemeriksaan,
  findItemjenispemeriksaanById,
  updateItemjenispemeriksaan,
  deleteItemjenispemeriksaan,
  findItemjenispemeriksaan
} from './itemjenispemeriksaan_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let itemjenispemeriksaan = await findListItemjenispemeriksaan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(itemjenispemeriksaan.count / limit),
      total_data: itemjenispemeriksaan.count,
    };

    return ResponseHelper(res, 200, 'Data Item Jenis Pemeriksaan Berhasil Ditampilkan', itemjenispemeriksaan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Item Jenis Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const find = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let itemjenispemeriksaan = await findItemjenispemeriksaan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(itemjenispemeriksaan.count / limit),
      total_data: itemjenispemeriksaan.count,
    };

    return ResponseHelper(res, 200, ' Data Item Jenis Pemeriksaan Berhasil Ditampilkan', itemjenispemeriksaan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Item Jenis Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const itemjenispemeriksaan = await createItemjenispemeriksaan({ ...req.body });
    return ResponseHelper(res, 201, 'Data Item Jenis Pemeriksaan Berhasil Ditambahkan', itemjenispemeriksaan);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Item Jenis Pemeriksaan Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check jenis pemeriksaan is exist
    let checkItemjenispemeriksaan = await findItemjenispemeriksaanById(id);
    if (!checkItemjenispemeriksaan) {
      return ResponseHelper(res, 409, 'Item Jenis Pemeriksaan Tidak Tersedia', [
        { message: 'Item Jenis Pemeriksaan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Update jenis pemeriksaan
    await updateItemjenispemeriksaan({ ...req.body }, { where: { id } });

    const result = await findItemjenispemeriksaanById(id);

    return ResponseHelper(res, 201, 'Data Item Jenis Pemeriksaan Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Item Jenis Pemeriksaan Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check jadwal dokter Exist Or Not
    let checkItemjenispemeriksaan = await findItemjenispemeriksaanById(id);
    if (!checkItemjenispemeriksaan) {
      return ResponseHelper(res, 409, 'Item Jenis Pemeriksaan Tidak Tersedia', [
        { message: 'Item Jenis Pemeriksaan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete jenis pemeriksaan
    let users = await findUserById(userId);

    await updateItemjenispemeriksaan({ deletedBy: users }, { where: { id } });

    await deleteItemjenispemeriksaan(id);

    return ResponseHelper(res, 201, 'Data Item Jenis Pemeriksaan Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Item Jenis Pemeriksaan Gagal Dihapus', error.message);
  }
};
    
export { get, add, update, deleted, find };
