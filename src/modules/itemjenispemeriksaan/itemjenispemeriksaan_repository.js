import db from '../../database/models/index.js';
const { Itemjenispemeriksaan } = db;
import { Op } from 'sequelize';

// Find one item jenis pemeriksaan by id
const findItemjenispemeriksaanById = async (id) => {
  try {
    let result = await Itemjenispemeriksaan.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findItemjenispemeriksaanById', error);
    throw new Error(error);
  }
};

// Find one Item jenispemeriksaan by filter
const findOneItemjenispemeriksaan = async (filter) => {
  try {
    let result = await Itemjenispemeriksaan.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneItemjenispemeriksaan', error);
    throw new Error(error);
  }
};

// Find list item Jenis pemeriksaan
const findListItemjenispemeriksaan = async ({ search, status }, page, limit) => {
  try {
    let result = await Itemjenispemeriksaan.findAndCountAll({
      where: {
        item: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListItemjenispemeriksaan', error);
    throw new Error(error);
  }
};

// Find Itemjenispemeriksaan
const findItemjenispemeriksaan = async ({ id, status }, page, limit) => {
  try {
    let result = await Itemjenispemeriksaan.findAndCountAll({
      where: {
        item: id ? { [Op.like]: `%${id}%` } : { [Op.like]: `%%` },
        clinic_id: status ,
      },
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPasien', error);
    throw new Error(error);
  }
};

// Create new Item jenispemeriksaan
const createItemjenispemeriksaan = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Itemjenispemeriksaan.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createItemjenispemeriksaan', error);
    throw new Error(error);
  }
};

// Update item jenis pemeriksaan
const updateItemjenispemeriksaan = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Itemjenispemeriksaan.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateItemjenispemeriksaan', error);
    throw new Error(error);
  }
};

// delete item jenis pemeriksaan
const deleteItemjenispemeriksaan = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Itemjenispemeriksaan.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteItemjenispemeriksaan', error);
    throw new Error(error);
  }
};

export { findItemjenispemeriksaanById, findOneItemjenispemeriksaan, findListItemjenispemeriksaan, createItemjenispemeriksaan, updateItemjenispemeriksaan, deleteItemjenispemeriksaan, findItemjenispemeriksaan };
