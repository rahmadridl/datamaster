import { get, add, update, deleted } from './ruangan_controller.js';
import validator from './ruangan_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const RuanganRoutes = (app, prefix) => {
  app.route(`${prefix}/ruangan`).get(AuthMiddleware, get);
  app.route(`${prefix}/ruangan/add`).post(AuthMiddleware, validator('ruangan'), add);
  app.route(`${prefix}/ruangan/update/:id`).patch(AuthMiddleware, validator('ruangan'), update);
  app.route(`${prefix}/ruangan/delete/:id`).delete(AuthMiddleware, deleted);
};

export { RuanganRoutes };
