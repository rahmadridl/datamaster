import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import {
  createRuangan,
  findListRuangan,
  findRuanganById,
  updateRuangan,
  deleteRuangan
} from './ruangan_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let ruangan = await findListRuangan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(ruangan.count / limit),
      total_data: ruangan.count,
    };

    return ResponseHelper(res, 200, 'Data Ruangan Berhasil Ditampilkan', limit=="all"?ruangan:ruangan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Ruangan Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    function isNumber(n) {
      return !isNaN(parseFloat(n)) && !isNaN(n - 0);
    }

    if (req.body.tarif < 0 ){
      return ResponseHelper(res, 422, 'Tarif Harus Lebih Dari 0', [
        { message: 'Tarif Harus Lebih Dari 0', param: 'tarif' },
      ]);
    }
    if (!isNumber(req.body.tarif)){
      return ResponseHelper(res, 422, 'Tarif Harus Diisi Angka', [
        { message: 'Tarif Harus Diisi Angka', param: 'tarif' },
      ]);
    }

    const ruangan = await createRuangan({ ...req.body });
    await updateRuangan(
      {clinic_id:user.clinic_id},
      {where: {id: ruangan.id}}
    )
    return ResponseHelper(res, 201, 'Data Ruangan Berhasil Ditambahkan', ruangan);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Ruangan Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check ruangan is exist
    let checkRuangan = await findRuanganById(id);
    if (!checkRuangan) {
      return ResponseHelper(res, 409, 'Data Ruangan Tidak Tersedia', [
        { message: 'Data Ruangan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) >2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }
    function isNumber(n) {
      return !isNaN(parseFloat(n)) && !isNaN(n - 0);
    }

    if (req.body.tarif < 0 ){
      return ResponseHelper(res, 422, 'Tarif Harus Lebih Dari 0', [
        { message: 'Tarif Harus Lebih Dari 0', param: 'tarif' },
      ]);
    }
    if (!isNumber(req.body.tarif)){
      return ResponseHelper(res, 422, 'Tarif Harus Diisi Angka', [
        { message: 'Tarif Harus Diisi Angka', param: 'tarif' },
      ]);
    }

    // Update ruangan
    await updateRuangan({ ...req.body }, { where: { id } });

    const result = await findRuanganById(id);

    return ResponseHelper(res, 201, 'Data Ruangan Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Ruangan Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check ruangan Exist Or Not
    let checkRuangan = await findRuanganById(id);
    if (!checkRuangan) {
      return ResponseHelper(res, 409, 'Data Ruangan Tidak Tersedia', [
        { message: 'Data Ruangan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete Ruangan
    let users = await findUserById(userId);

    await updateRuangan({ deletedBy: users }, { where: { id } });

    await deleteRuangan(id);

    return ResponseHelper(res, 201, 'Data Ruangan Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Ruangan Berhasil Dihapus', error.message);
  }
};

export { get, add, update, deleted };
