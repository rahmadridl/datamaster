import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'ruangan': {
      return [
        body('nomor').not().isEmpty().withMessage('Nomor Harus Diisi'),
        body('nomor_ranjang').not().isEmpty().withMessage('Nomor Ranjang Harus Diisi'),
        body('nama_ruangan').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('tarif').not().isEmpty().withMessage('Tarif Harus Diisi'),
        // body('tarif').isNumeric().withMessage('tarif Harus Diisi'),
        // body('creator_id').not().isEmpty().withMessage('creator id Harus Diisi'),
        // body('clinic_id').not().isEmpty().withMessage('clinic id Harus Diisi'),
        body('kelasruangan_id').not().isEmpty().withMessage('Kelas Ruangan Harus Diisi'),
        body('kategoriruangan_id').not().isEmpty().withMessage('Kategori Ruangan Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
