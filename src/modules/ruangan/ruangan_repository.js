import db from '../../database/models/index.js';
const { Ruangan } = db;
import { Op } from 'sequelize';

// Find one ruangan by id
const findRuanganById = async (id) => {
  try {
    let result = await Ruangan.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findRuanganById', error);
    throw new Error(error);
  }
};

// Find one ruangan by filter
const findOneRuangan = async (filter) => {
  try {
    let result = await Ruangan.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneRuangan', error);
    throw new Error(error);
  }
};

// Find list ruangan
const findListRuangan = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Ruangan.findAll({
        where: {
          nama_ruangan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Ruangan.findAndCountAll({
        where: {
          nama_ruangan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListRuangan', error);
    throw new Error(error);
  }
};

// Create new ruangan
const createRuangan = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Ruangan.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createRuangan', error);
    throw new Error(error);
  }
};

// Update ruangan
const updateRuangan = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Ruangan.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateRuangan', error);
    throw new Error(error);
  }
};

//delete ruangan
const deleteRuangan = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Ruangan.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteRuangan', error);
    throw new Error(error);
  }
};

export {
  findRuanganById,
  findOneRuangan,
  findListRuangan,
  createRuangan,
  updateRuangan,
  deleteRuangan,
};
