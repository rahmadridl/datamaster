import { get, get1, add, update, deleted, upload, getid } from './asuransi_controller.js';
import validator from './asuransi_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const AsuransiRoutes = (app, prefix) => {
  app.route(`${prefix}/asuransi`).get(AuthMiddleware, get);
  app.route(`${prefix}/asuransi/import`).post(AuthMiddleware, upload);
  app.route(`${prefix}/asuransinotokenbaru`).get( get);
  app.route(`${prefix}/asuransinotoken`).get( get1);
  app.route(`${prefix}/asuransiidnotoken/:id`).get( getid);
  app.route(`${prefix}/asuransi/add`).post(AuthMiddleware, validator('asuransi'), add);
  app.route(`${prefix}/asuransi/update/:id`).patch(AuthMiddleware, validator('asuransi'), update);
  app.route(`${prefix}/asuransi/delete/:id`).delete(AuthMiddleware, deleted);
};

export { AsuransiRoutes };
