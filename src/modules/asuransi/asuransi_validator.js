import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'asuransi': {
      return [
        body('nama_asuransi').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('kode_asuransi').not().isEmpty().withMessage('Kode Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
