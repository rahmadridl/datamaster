import db from '../../database/models/index.js';
const { Asuransi } = db;
import { Op } from 'sequelize';

// Find one asuransi by id
const findAsuransiById = async (id) => {
  try {
    let result = await Asuransi.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findAsuransiById', error);
    throw new Error(error);
  }
};

// Find one Asuransi by filter
const findOneAsuransi = async (filter) => {
  try {
    let result = await Asuransi.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneAsuransi', error);
    throw new Error(error);
  }
};

// Find list Asuransi
const findListAsuransi = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Asuransi.findAll({
        where: {
          nama_asuransi: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Asuransi.findAndCountAll({
        where: {
          nama_asuransi: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListAsuransi', error);
    throw new Error(error);
  }
};

// Find list Asuransi no token
const findListAsuransinotoken = async ({ search, status }, page, limit) => {
  try {
    let result = await Asuransi.findAndCountAll({
      where: {
        nama_asuransi: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        // clinic_id: status
      },
      order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListAsuransi', error);
    throw new Error(error);
  }
};

// Create new Asuransi
const createAsuransi = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Asuransi.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createAsuransi', error);
    throw new Error(error);
  }
};

// Update Asuransi
const updateAsuransi = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Asuransi.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateAsuransi', error);
    throw new Error(error);
  }
};

//delete Asuransi
const deleteAsuransi = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Asuransi.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteAsuransi', error);
    throw new Error(error);
  }
};

export {
  findAsuransiById,
  findOneAsuransi,
  findListAsuransi,
  createAsuransi,
  updateAsuransi,
  deleteAsuransi,
  findListAsuransinotoken,
};
