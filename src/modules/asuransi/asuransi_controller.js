import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import xlsx from 'node-xlsx';
import { Op } from 'sequelize';
import {
  createAsuransi,
  findListAsuransi,
  findOneAsuransi,
  findAsuransiById,
  updateAsuransi,
  deleteAsuransi,
  findListAsuransinotoken
} from './asuransi_repository.js';

const get = async (req, res) => {
  try {
    console.log(req.app.locals);
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status==0) requirement.status = status;

    let asuransi = await findListAsuransi(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(asuransi.count / limit),
      total_data: asuransi.count,
    };

    return ResponseHelper(res, 200, 'Data Asuransi Berhasil Ditampilkan', limit=="all"?asuransi:asuransi.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Asuransi Gagal Ditampilkan', error.message);
  }
};

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let asuransi = await findListAsuransinotoken(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(asuransi.count / limit),
      total_data: asuransi.count,
    };

    return ResponseHelper(res, 200, 'Data Asuransi Berhasil Ditampilkan', asuransi.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Asuransi Gagal Ditampilkan', error.message);
  }
};

const getid = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let asuransi = await findAsuransiById(id)
    if (asuransi == null) {
      return ResponseHelper(res, 409, 'Data Asuransi Tidak Tersedia', [
        { message: 'Data Asuransi Tidak Tersedia', param: 'id' },
      ]);
    }
    // let asuransi = await findListAsuransinotoken(requirement, page, limit);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(asuransi.count / limit),
      total_data: asuransi.count,
    };

    return ResponseHelper(res, 200, 'Data Asuransi Berhasil Ditampilkan', asuransi, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Asuransi Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check nama_asuransi is exist
    let checkAsuransi = await findOneAsuransi({
      where:{nama_asuransi : req.body.nama_asuransi, clinic_id:user.clinic_id}
    });
    if (checkAsuransi) {
      return ResponseHelper(res, 409, 'Nama Asuransi Sudah Terdaftar', [
        { message: 'Nama Asuransi Sudah Terdaftar', param: 'nama_asuransi' },
      ]);
    }
    // Check kode_asuransi is exist
    let checkAsuransikode = await findOneAsuransi({
      where:{kode_asuransi : req.body.kode_asuransi, clinic_id:user.clinic_id}
    });
    if (checkAsuransikode) {
      return ResponseHelper(res, 409, 'Kode Asuransi Sudah Terdaftar', [
        { message: 'Kode Asuransi Sudah Terdaftar', param: 'kode_asuransi' },
      ]);
    }

    const asuransi = await createAsuransi({ ...req.body });
    await updateAsuransi(
      {clinic_id:user.clinic_id},
      {where: {id: asuransi.id}}
    )
    return ResponseHelper(res, 201, 'Data Asuransi Berhasil Ditambahkan', asuransi);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Asuransi Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check asuransi is exist
    let checkAsuransi = await findAsuransiById(id);
    if (!checkAsuransi) {
      return ResponseHelper(res, 409, 'Data Asuransi Tidak Tersedia', [
        { message: 'Data Asuransi Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check nama_asuransi is exist
    let checkAsuransinama = await findOneAsuransi({
      where:{nama_asuransi : req.body.nama_asuransi, clinic_id:user.clinic_id,
        id:{[Op.not]: id}}
    });
    if (checkAsuransinama) {
      return ResponseHelper(res, 409, 'Nama Asuransi Sudah Terdaftar', [
        { message: 'Nama Asuransi Sudah Terdaftar', param: 'nama_asuransi' },
      ]);
    }
    // Check kode_asuransi is exist
    let checkAsuransikode = await findOneAsuransi({
      where:{kode_asuransi : req.body.kode_asuransi, clinic_id:user.clinic_id,
             id:{[Op.not]: id}}
    });
    if (checkAsuransikode) {
      return ResponseHelper(res, 409, 'Kode Asuransi Sudah Terdaftar', [
        { message: 'Kode Asuransi Sudah Terdaftar', param: 'kode_asuransi' },
      ]);
    }

    // Update asuransi
    await updateAsuransi({ ...req.body }, { where: { id } });

    const result = await findAsuransiById(id);

    return ResponseHelper(res, 201, 'Data Asuransi Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Asuransi Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check asuransi Exist Or Not
    let checkAsuransi = await findAsuransiById(id);
    if (!checkAsuransi) {
      return ResponseHelper(res, 409, 'Data Asuransi Tidak Tersedia', [
        { message: 'Data Asuransi Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete asuransi
    let users = await findUserById(userId);

    await updateAsuransi({ deletedBy: users }, { where: { id } });

    await deleteAsuransi(id);

    return ResponseHelper(res, 201, 'Data Asuransi Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Asuransi Gagal Dihapus', error.message);
  }
};

const upload = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    // console.log(req.files.import);

    // const mantap = req.files.fileName
    // const mantatap = mantap
    // console.log(mantatap.toString('utf8'));

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // console.log(path.dirname(__filename)+"\\myFile.xlsx");
    const workSheetsFromFile = xlsx.parse(req.files.import.data);
    // console.log(workSheetsFromFile[0].data);
    var arr = [];
    var tip = [];
    workSheetsFromFile.forEach((row) => {
      // console.log(row);
      row.data.forEach(async (data, index) => {
        // console.log(index);
        // console.log(data);
        let object = {};
        if (index > 8) {
          row.data[8].forEach(async (header, index) => {
            object[header.replace('*','')] = data[index];
          });
          // console.log('object', object);
          if (object.nama && object.kode) {
            arr.push(object);
          }
        }
      });
    });
    for (let i = 0; i < arr.length; i++) {
      const ceknip = await findOneAsuransi({ where: { kode_asuransi: arr[i].kode , clinic_id:user.clinic_id} });
      if (ceknip) {
        const edit = await updateAsuransi({nama_asuransi: arr[i].nama,
          kode_asuransi: arr[i].kode,
          email: arr[i].email,
          telp: arr[i].telp,
          alamat: arr[i].alamat},{where:{
            id:ceknip.id, clinic_id:user.clinic_id
          }})
        // return ResponseHelper(
        //   res,
        //   409,
        //   'Error: baris ke ' + (i + 2) + ', nip ' + arr[i].nip + ' sudah terdaftar',
        //   arr[i].nip
        // );
      }else{
        const tes = await createAsuransi({
          nama_asuransi: arr[i].nama,
          kode_asuransi: arr[i].kode,
          email: arr[i].email,
          telp: arr[i].telp,
          alamat: arr[i].alamat,
          clinic_id: user.clinic_id,
        });
      }
    }
    // for (let i = 0; i < arr.length; i++) {
    // }
    return ResponseHelper(res, 201, 'Data Asuransi Berhasil Diunduh');
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Asuransi Gagal Diunduh', error.message);
  }
};

export { get, get1, getid, add, update, deleted, upload };
