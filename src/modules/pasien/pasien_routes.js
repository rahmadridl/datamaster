import { get, add, add1, update, deleted, find, findketat, findgender, findketatklinik, findone} from './pasien_controller.js';
import validator from './pasien_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const PasienRoutes = (app, prefix) => {
  app.route(`${prefix}/pasien`).get(AuthMiddleware, get);
  app.route(`${prefix}/pasiennotoken/add`).post(validator('pasien'), add1);
  app.route(`${prefix}/pasien/find/:id`).get(find);
  app.route(`${prefix}/pasien/findone/:id`).get(AuthMiddleware, findone);
  app.route(`${prefix}/pasienketat/find/:id`).get(findketat);
  app.route(`${prefix}/pasienketatklinik/find/:id`).get(findketatklinik);
  app.route(`${prefix}/pasiengender`).get(findgender);
  app.route(`${prefix}/pasien/add`).post(AuthMiddleware, validator('pasien'), add);
  app.route(`${prefix}/pasien/update/:id`).patch(AuthMiddleware, validator('pasien'), update);
  app.route(`${prefix}/pasien/delete/:id`).delete(AuthMiddleware, deleted);
};

export { PasienRoutes };
