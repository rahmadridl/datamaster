import db from '../../database/models/index.js';
const { Pasien } = db;
import { Op } from 'sequelize';

// Find one pasien by id
const findPasienById = async (id) => {
  try {
    let result = await Pasien.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findPasienById', error);
    throw new Error(error);
  }
};

// Find one pasien by filter
const findOnePasien = async (filter) => {
  try {
    let result = await Pasien.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOnePasien', error);
    throw new Error(error);
  }
};

// Find list pasien
const findListPasien = async ({ query, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Pasien.findAll({
        where: { [Op.or]:[
          {nama_lengkap: query ? { [Op.like]: `%${query}%` } : { [Op.like]: `%%` }},
          {nomor_rm: query ? { [Op.like]: `%${query}%` } : { [Op.like]: `%%` }},
          {no_identitas: query ? { [Op.like]: `%${query}%` } : { [Op.like]: `%%` }}],
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Pasien.findAndCountAll({
        where: {[Op.or]:[
          {nama_lengkap: query ? { [Op.like]: `%${query}%` } : { [Op.like]: `%%` }},
          {nomor_rm: query ? { [Op.like]: `%${query}%` } : { [Op.like]: `%%` }},
          {no_identitas: query ? { [Op.like]: `%${query}%` } : { [Op.like]: `%%` }}],
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPasien', error);
    throw new Error(error);
  }
};

// Find list pasien
const findListPasienfilter = async ({ id, clinic }, page, limit) => {
  try {
    let result = await Pasien.findAndCountAll({
      where: {
        identity_number: id ? { [Op.like]: `%${id}%` } : { [Op.like]: `%%` },
        // clinic_id: clinic
      },
      order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPasien', error);
    throw new Error(error);
  }
};

// Create new Pasien
const createPasien = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Pasien.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createPasien', error);
    throw new Error(error);
  }
};

// Update Pasien
const updatePasien = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Pasien.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updatePasien', error);
    throw new Error(error);
  }
};

//delete pasien
const deletePasien = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Pasien.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deletePasien', error);
    throw new Error(error);
  }
};

// Find Pasien
const findPasien = async ({ id, status }, page, limit) => {
  try {
    let result = await Pasien.findAndCountAll({
      where: {
        identity_number: id ? { [Op.like]: `%${id}%` } : { [Op.like]: `%%` },
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPasien', error);
    throw new Error(error);
  }
};

// Find Pasien
const findPasienketat = async ({ id, status }, page, limit) => {
  try {
    let result = await Pasien.findAndCountAll({
      where: {
        identity_number: id,
        // clinic_id:status 
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPasien', error);
    throw new Error(error);
  }
};

// Find Pasien
const findPasienketatklinik = async ({ id, status }, page, limit) => {
  try {
    let result = await Pasien.findAndCountAll({
      where: {
        identity_number: id,
        clinic_id:status 
        // status: status ? { [Op.like]: `%${status}%` } : { [Op.like]: `%%` },
      },
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPasien', error);
    throw new Error(error);
  }
};

// Find Pasien gender
const findPasiengender = async (gender,clinic) => {
  try {
    let result = await Pasien.count({
      where: {
        clinic_id: clinic ,
        gender: gender ? { [Op.like]: `%${gender}%` } : { [Op.like]: `%%` },
      },
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPasien', error);
    throw new Error(error);
  }
};

export {
  findPasienById,
  findOnePasien,
  findListPasien,
  createPasien,
  updatePasien,
  deletePasien,
  findPasien,
  findPasiengender,
  findListPasienfilter,
  findPasienketat,
  findPasienketatklinik
};
