import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'pasien': {
      return [
        body('sureName').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('address').not().isEmpty().withMessage('Alamat Harus Diisi'),
        body('gender').not().isEmpty().withMessage('Jenis Kelamin Harus Diisi'),
        body('date_of_birth').not().isEmpty().withMessage('Tanggal Lahir Harus Diisi'),
        body('place_of_birth').not().isEmpty().withMessage('Tempat Lahir Harus Diisi'),
        body('married').not().isEmpty().withMessage('Status Hubungan Harus Diisi'),
        body('religion').not().isEmpty().withMessage('Agama Harus Diisi'),
        body('email').not().isEmpty().withMessage('Email Harus Diisi'),
        body('email').isEmail().withMessage('Format Email Salah'),
        body('blood').not().isEmpty().withMessage('Golongan Darah Harus Diisi'),
        body('identity_number').not().isEmpty().withMessage('No KTP Harus Diisi'),
        // body('phone_number').not().isEmpty().withMessage('phone_number Harus Diisi'),
        body('telephone').not().isEmpty().withMessage('Nomor Telfon Harus Diisi'),
        // body('company').not().isEmpty().withMessage('company Harus Diisi'),
        body('job').not().isEmpty().withMessage('Pekerjaan Harus Diisi'),
        // body('biological_mother').not().isEmpty().withMessage('biological_mother Harus Diisi'),
        // body('clinic_id').not().isEmpty().withMessage('clinic id Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
