import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import { findClinicById, findOneClinic } from '../clinic/clinic_repository.js';
import { Op } from 'sequelize';
import {
  createPasien,
  findListPasien,
  findPasienById,
  findOnePasien,
  updatePasien,
  deletePasien,
  findPasien,
  findListPasienfilter,
  findPasienketat,
  findPasiengender,
  findPasienketatklinik,
} from './pasien_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const query = req.query.query || '';
    const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (query) requirement.query = query;
    if (status || status == 0) requirement.status = status;

    let pasien = await findListPasien(requirement, page, limit);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(pasien.count / limit),
      total_data: pasien.count,
    };

    return ResponseHelper(
      res,
      200,
      'Data Pasien Berhasil Ditampilkan',
      limit == 'all' ? pasien : pasien.rows,
      meta
    );
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pasien Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);

  //random kode rm
  function getRandomKodetran(length) {
    var randomChars = '0123456789';
    var result = 'RM';
    for (var i = 0; i < length; i++) {
      result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const {
      id,
      pid,
      sureName,
      place_of_birth,
      date_of_birth,
      married,
      religion,
      blood,
      province,
      country,
      district,
      gender,
      identity,
      identity_number,
      phone_number,
      telephone,
      address,
      job,
      status,
      company,
      biological_mother,
      primary_insurance,
      insurance_number,
      email,
      clinic_id,
      nationality,
      creator_id,
      import_id,
    } = req.body;

    if (identity_number == null || identity_number == '' || identity_number == '-') {
      return ResponseHelper(res, 422, 'No KTP Harus Diisi', [
        { message: 'No KTP Harus Diisi', param: 'identity_number' },
      ]);
    }
    if (date_of_birth == null || date_of_birth == '' || date_of_birth == '-') {
      return ResponseHelper(res, 422, 'Tanggal Lahir Harus Diisi', [
        { message: 'Tanggal Lahir Harus Diisi', param: 'date_of_birth' },
      ]);
    }
    if (telephone == null || telephone == '' || telephone == '-') {
      return ResponseHelper(res, 422, 'Nomor Telfon Harus Diisi', [
        { message: 'Nomor Telfon Harus Diisi', param: 'telephone' },
      ]);
    }
    if (sureName == null || sureName == '' || sureName == '-') {
      return ResponseHelper(res, 422, 'Username Harus Diisi', [
        { message: 'Username Harus Diisi', param: 'sureName' },
      ]);
    }
    if (gender == null || gender == '' || gender == '-') {
      return ResponseHelper(res, 422, 'Jenis Kelamin Harus Diisi', [
        { message: 'Jenis Kelamin Harus Diisi', param: 'gender' },
      ]);
    }
    if (address == null || address == '' || address == '-') {
      return ResponseHelper(res, 422, 'Alamat Harus Diisi', [
        { message: 'Alamat Harus Diisi', param: 'address' },
      ]);
    }
    if (email == null || email == '' || email == '-' ) {
      return ResponseHelper(res, 422, 'Email Harus Diisi', [
        { message: 'Email Harus Diisi', param: 'email' },
      ]);
    }
    const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    if (!emailRegexp.test(email)) {
      return ResponseHelper(res, 422, 'Format Email Salah', [
        { message: 'Format Email Salah', param: 'email' },
      ]);
    }

    let check_identity = await findOnePasien({
      where: { identity_number: identity_number },
    });
    if (check_identity) {
      return ResponseHelper(res, 409, 'No KTP Sudah Terdaftar', [
        { message: 'No KTP Sudah Terdaftar', param: 'identity_number' },
      ]);
    }

    function getAge(dateString) {
      var today = new Date();
      var birthDate = new Date(dateString);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      return age;
    }

    if (getAge(date_of_birth) < 0) {
      return ResponseHelper(res, 409, 'Tanggal Lahir Tidak Sesuai', [
        { message: 'Tanggal Lahir Tidak Sesuai', param: 'date_of_birth' },
      ]);
    }
    console.log(getAge(date_of_birth), date_of_birth);

    const norm = getRandomKodetran(5);
    const pasien = await createPasien({
      pid: norm,
      sureName,
      age: getAge(date_of_birth),
      place_of_birth,
      date_of_birth,
      married,
      religion,
      blood,
      province,
      country,
      district,
      gender,
      identity,
      identity_number,
      phone_number,
      telephone,
      address,
      job,
      status,
      company,
      biological_mother,
      primary_insurance,
      insurance_number,
      email,
      clinic_id: user.clinic_id,
      nationality,
    });
    // await updatePasien(
    //   {clinic_id:user.clinic_id},
    //   {where: {id: pasien.id}}
    // )
    const cliname = await findClinicById(user.clinic_id);
    // await updatePasien(
    //   { pid:  norm},
    //   { where: { id: pasien.id } }
    // );

    let check_nomor_rm = await findOnePasien({
      where: { pid: pasien.pid },
    });
    if (check_nomor_rm > 1) {
      return ResponseHelper(res, 409, 'Nomor Rekam Medis Sudah Terdaftar', [
        { message: 'Nomor Rekam Medis Sudah Terdaftar', param: 'pid' },
      ]);
    }

    const hasil = {
      pid: norm,
      sureName: pasien.sureName,
      place_of_birth: pasien.place_of_birth,
      date_of_birth: pasien.date_of_birth,
      age: pasien.age,
      gender: pasien.gender,
      married: pasien.married,
      religion: pasien.religion,
      blood: pasien.blood,
      identity: pasien.identity,
      identity_number: pasien.identity_number,
      phone_number: pasien.phone_number,
      telephone_number: pasien.telephone_number,
      address: pasien.address,
      job: pasien.job,
      company: pasien.company,
      biological_mother: pasien.biological_mother,
      primary_insurance: pasien.primary_insurance,
      insurance_number: pasien.insurance_number,
      email: pasien.email,
      clinic_name: cliname.clinic_name,
    };
    return ResponseHelper(res, 201, 'Data Pasien Berhasil Ditambahkan', hasil);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pasien Gagal Ditambahkan', error.message);
  }
};

const add1 = async (req, res) => {
  const errors = validationResult(req);
  //random kode rm
  function getRandomKodetran(length) {
    var randomChars = '0123456789';
    var result = 'RM';
    for (var i = 0; i < length; i++) {
      result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const {
      id,
      pid,
      sureName,
      age,
      place_of_birth,
      date_of_birth,
      married,
      religion,
      blood,
      province,
      country,
      district,
      gender,
      identity,
      identity_number,
      phone_number,
      telephone,
      address,
      job,
      company,
      biological_mother,
      primary_insurance,
      insurance_number,
      email,
      clinic_code,
      nationality,
      creator_id,
      import_id,
    } = req.body;

    if (identity_number == null || identity_number == '' || identity_number == '-') {
      return ResponseHelper(res, 422, 'No KTP Harus Diisi', [
        { message: 'No KTP Harus Diisi', param: 'identity_number' },
      ]);
    }
    if (date_of_birth == null || date_of_birth == '' || date_of_birth == '-') {
      return ResponseHelper(res, 422, 'Tanggal Lahir Harus Diisi', [
        { message: 'Tanggal Lahir Harus Diisi', param: 'date_of_birth' },
      ]);
    }
    if (telephone == null || telephone == '' || telephone == '-') {
      return ResponseHelper(res, 422, 'Nomor Telfon Harus Diisi', [
        { message: 'Nomor Telfon Harus Diisi', param: 'telephone' },
      ]);
    }
    if (sureName == null || sureName == '' || sureName == '-') {
      return ResponseHelper(res, 422, 'Username Harus Diisi', [
        { message: 'Username Harus Diisi', param: 'sureName' },
      ]);
    }
    const klinik = await findOneClinic({ where: { clinic_code: clinic_code } });
    let check_identity = await findOnePasien({
      where: { identity_number: identity_number ,clinic_id:klinik.id},
    });
    if (check_identity) {
      return ResponseHelper(res, 409, 'No KTP Sudah Terdaftar', [
        { message: 'No KTP Sudah Terdaftar', param: 'identity_number' },
      ]);
    }

    function getAge(dateString) {
      var today = new Date();
      var birthDate = new Date(dateString);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      return age;
    }

    if (getAge(date_of_birth) < 0) {
      return ResponseHelper(res, 409, 'Tanggal Lahir Tidak Sesuai', [
        { message: 'Tanggal Lahir Tidak Sesuai', param: 'date_of_birth' },
      ]);
    }

    
    const norm = getRandomKodetran(5);
    const pasien = await createPasien({
      pid: norm,
      sureName,
      age: getAge(date_of_birth),
      place_of_birth,
      date_of_birth,
      married,
      religion,
      blood,
      province,
      country,
      district,
      gender,
      identity,
      identity_number,
      phone_number,
      telephone,
      address,
      job,
      company,
      biological_mother,
      primary_insurance,
      insurance_number,
      email,
      clinic_id: klinik.id,
      nationality,
    });
    // await updatePasien(
    //   {clinic_id:user.clinic_id},
    //   {where: {id: pasien.id}}
    // )
    // const cliname = await findClinicById(user.clinic_id);
    // await updatePasien(
    //   { pid:  norm},
    //   { where: { id: pasien.id } }
    // );

    let check_nomor_rm = await findOnePasien({
      where: { pid: pasien.pid },
    });
    if (check_nomor_rm > 1) {
      return ResponseHelper(res, 409, 'Nomor Rekam Medis Sudah Terdaftar', [
        { message: 'Nomor Rekam Medis Sudah Terdaftar', param: 'pid' },
      ]);
    }

    const hasil = {
      pid: norm,
      sureName: pasien.sureName,
      place_of_birth: pasien.place_of_birth,
      date_of_birth: pasien.date_of_birth,
      age: pasien.age,
      gender: pasien.gender,
      married: pasien.married,
      religion: pasien.religion,
      blood: pasien.blood,
      identity: pasien.identity,
      identity_number: pasien.identity_number,
      phone_number: pasien.phone_number,
      telephone_number: pasien.telephone_number,
      address: pasien.address,
      job: pasien.job,
      company: pasien.company,
      biological_mother: pasien.biological_mother,
      primary_insurance: pasien.primary_insurance,
      insurance_number: pasien.insurance_number,
      email: pasien.email,
      clinic_name: klinik.clinic_name,
    };
    return ResponseHelper(res, 201, 'Data Pasien Berhasil Ditambahkan', hasil);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pasien Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check pasien is exist
    let checkPasien = await findPasienById(id);
    if (!checkPasien) {
      return ResponseHelper(res, 409, 'Pasien Tidak Tersedia', [
        { message: 'Pasien Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    function getAge(dateString) {
      var today = new Date();
      var birthDate = new Date(dateString);
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      return age;
    }

    if (getAge(req.body.date_of_birth) < 0) {
      return ResponseHelper(res, 409, 'tanggal lahir tidak sesuai', [
        { message: 'tanggal lahir tidak sesuai', param: 'date_of_birth' },
      ]);
    }

    if (
      req.body.identity_number == null ||
      req.body.identity_number == '' ||
      req.body.identity_number == '-'
    ) {
      return ResponseHelper(res, 422, 'No KTP Harus Diisi', [
        { message: 'No KTP Harus Diisi', param: 'identity_number' },
      ]);
    }
    if (
      req.body.date_of_birth == null ||
      req.body.date_of_birth == '' ||
      req.body.date_of_birth == '-'
    ) {
      return ResponseHelper(res, 422, 'date of birth Harus Diisi', [
        { message: 'date of birth Harus Diisi', param: 'date_of_birth' },
      ]);
    }
    if (req.body.telephone == null || req.body.telephone == '' || req.body.telephone == '-') {
      return ResponseHelper(res, 422, 'telephone Harus Diisi', [
        { message: 'telephone Harus Diisi', param: 'telephone' },
      ]);
    }
    if (req.body.sureName == null || req.body.sureName == '' || req.body.sureName == '-') {
      return ResponseHelper(res, 422, 'sureName Harus Diisi', [
        { message: 'sureName Harus Diisi', param: 'sureName' },
      ]);
    }

    let check_identity = await findOnePasien({
      where: { identity_number: req.body.identity_number, id: { [Op.not]: id } },
    });
    if (check_identity) {
      return ResponseHelper(res, 409, 'No KTP Sudah Terdaftar', [
        { message: 'No KTP Sudah Terdaftar', param: 'identity_number' },
      ]);
    }

    // Update pasien
    await updatePasien({ ...req.body }, { where: { id } });

    const result = await findPasienById(id);

    return ResponseHelper(res, 201, 'Data Pasien Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pasien Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check pasien Exist Or Not
    let checkPasien = await findPasienById(id);
    if (!checkPasien) {
      return ResponseHelper(res, 409, 'Pasien Tidak Tersedia', [
        { message: 'Pasien Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete pasien
    let users = await findUserById(userId);

    await updatePasien({ deletedBy: users }, { where: { id } });

    await deletePasien(id);

    return ResponseHelper(res, 201, 'Data Pasien Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pasien Gagal Dihapus', error.message);
  }
};

const find = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // const id = req.params;
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '50');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let pasien = await findPasien(requirement, page, limit);
    // Check pasien is exist
    if (pasien.rows.length == 0) {
      return ResponseHelper(res, 409, 'Pasien Tidak Tersedi', [
        { message: 'Pasien Tidak Tersedia', param: 'id' },
      ]);
    }

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(pasien.count / limit),
      total_data: pasien.count,
    };

    return ResponseHelper(res, 200, 'Data Pasien Berhasil Ditampilkan', pasien.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pasien Gagal Ditampilkan', error.message);
  }
};

const findone = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // const id = req.params;
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '50');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;
    let user = await findUserById(user_id);

    // let pasien = await findPasien(requirement, page, limit);
    var pasien = await findOnePasien({where:{identity_number:id, clinic_id:user.clinic_id}})
    // Check pasien is exist
    if (!pasien) {
      return ResponseHelper(res, 409, 'Pasien tidak ditemukan', [
        { message: 'Pasien tidak ditemukan', param: 'id' },
      ]);
    }

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(pasien.count / limit),
      total_data: pasien.count,
    };

    return ResponseHelper(res, 200, 'success get nik '+id+ ' data pasien', pasien, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed get pasien', error.message);
  }
};

const findketat = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // const id = req.params;
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '50');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let pasien = await findPasienketat(requirement, page, limit);
    // Check pasien is exist
    if (pasien.rows.length == 0) {
      return ResponseHelper(res, 409, 'Data Pasien Tidak Tersedia', [
        { message: 'Data Pasien Tidak Tersedia', param: 'id' },
      ]);
    }

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(pasien.count / limit),
      total_data: pasien.count,
    };

    return ResponseHelper(res, 200, 'Data Pasien Berhasil Ditampilkan', pasien.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pasien Gagal Ditampilkan', error.message);
  }
};

const findketatklinik = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // const id = req.params;
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '50');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let pasien = await findPasienketatklinik(requirement, page, limit);
    // Check pasien is exist
    if (pasien.rows.length == 0) {
      return ResponseHelper(res, 409, 'Pasien Tidak Tersedia', [
        { message: 'Pasien Tidak Tersedia', param: 'id' },
      ]);
    }

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(pasien.count / limit),
      total_data: pasien.count,
    };

    return ResponseHelper(res, 200, 'Data Pasien Berhasil Ditampilkan', pasien.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pasien Gagal Ditampilkan', error.message);
  }
};

const findgender = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    // const { id } = req.params;

    // const id = req.params;
    const gender = req.query.gender || '';
    const clinic = req.query.clinic || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '50');

    let requirement = {};
    // if (id) requirement.id = id;
    // if (status) requirement.status = status;

    // let pasien = await findPasienketat(requirement, page, limit);
    let pasien = await findPasiengender(gender,clinic);
    // Check pasien is exist
    // if (pasien.rows.length == 0) {
    //   return ResponseHelper(res, 409, 'Pasien Tidak Tersed', [
    //     { message: 'Pasien Tidak Tersed', param: 'id' },
    //   ]);
    // }

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(pasien.count / limit),
      total_data: pasien.count,
    };

    return ResponseHelper(res, 200, 'Data Pasien Berhasil Ditampilkan', pasien);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Pasien Gagal Ditampilkan', error.message);
  }
};

export { get, add, add1, update, deleted, find, findone, findketat, findketatklinik, findgender };
