import db from '../../database/models/index.js';
const { Perujuktipe } = db;
import { Op } from 'sequelize';

// Find one perujuk tipe by id
const findPerujuktipeById = async (id) => {
  try {
    let result = await Perujuktipe.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findPerujuktipeById', error);
    throw new Error(error);
  }
};

// Find one perujuk tipe by filter
const findOnePerujuktipe = async (filter) => {
  try {
    let result = await Perujuktipe.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOnePerujuktipe', error);
    throw new Error(error);
  }
};

// Find list perujuktipe
const findListPerujuktipe = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Perujuktipe.findAll({
        where: {
          nama_tipe: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Perujuktipe.findAndCountAll({
        where: {
          nama_tipe: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListSpesialisasi', error);
    throw new Error(error);
  }
};

// Create new perujuk tipe
const createPerujuktipe = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Perujuktipe.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createPerujuktipe', error);
    throw new Error(error);
  }
};

// Update Perujuktipe
const updatePerujuktipe = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Perujuktipe.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updatePerujuktipe', error);
    throw new Error(error);
  }
};

//delete perujuk tipe
const deletePerujuktipe = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Perujuktipe.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deletePerujuktipe', error);
    throw new Error(error);
  }
};

export {
  findPerujuktipeById,
  findOnePerujuktipe,
  findListPerujuktipe,
  createPerujuktipe,
  updatePerujuktipe,
  deletePerujuktipe,
};
