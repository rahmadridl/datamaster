import { get, add, update, deleted, upload } from './perujuktipe_controller.js';
import validator from './perujuktipe_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const PerujuktipeRoutes = (app, prefix) => {
  app.route(`${prefix}/perujuktipe`).get(AuthMiddleware, get);
  app.route(`${prefix}/perujuktipenotoken`).get(get);
  app.route(`${prefix}/perujuktipe/import`).post(AuthMiddleware, upload);
  app.route(`${prefix}/perujuktipe/add`).post(AuthMiddleware, validator('perujuktipe'), add);
  app.route(`${prefix}/perujuktipe/update/:id`).patch(AuthMiddleware, validator('perujuktipe'), update);
  app.route(`${prefix}/perujuktipe/delete/:id`).delete(AuthMiddleware, deleted);
};

export { PerujuktipeRoutes };
