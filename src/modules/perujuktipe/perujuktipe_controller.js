import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import xlsx from 'node-xlsx';
import { Op } from 'sequelize';
import {
  createPerujuktipe,
  findListPerujuktipe,
  findPerujuktipeById,
  findOnePerujuktipe,
  updatePerujuktipe,
  deletePerujuktipe
} from './perujuktipe_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let perujuktipe = await findListPerujuktipe(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(perujuktipe.count / limit),
      total_data: perujuktipe.count,
    };

    return ResponseHelper(res, 200, 'Data Tipe Perujuk Berhasil Ditampilkan', limit=="all"?perujuktipe:perujuktipe.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Tipe Perujuk Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check perujuk tipe is exist
    let checkPerujuktipe = await findOnePerujuktipe({
      where:{nama_tipe : req.body.nama_tipe, clinic_id:user.clinic_id}
    });
    if (checkPerujuktipe) {
      return ResponseHelper(res, 409, 'Nama Tipe Perujuk Sudah Terdaftar', [
        { message: 'Nama Tipe Perujuk Sudah Terdaftar', param: 'nama_tipe' },
      ]);
    }

    const perujuktipe = await createPerujuktipe({ ...req.body });
    await updatePerujuktipe(
      {clinic_id:user.clinic_id},
      {where: {id: perujuktipe.id}}
    )
    return ResponseHelper(res, 201, 'Data Tipe Perujuk Berhasil Ditambahkan', perujuktipe);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Tipe Perujuk Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check perujuk tipe is exist
    let checkPerujuktipe = await findPerujuktipeById(id);
    if (!checkPerujuktipe) {
      return ResponseHelper(res, 409, 'Data Tipe Perujuk Tidak Tersedia', [
        { message: 'Data Tipe Perujuk Tidak Tersedia', param: 'id' },
      ]);
    }


    // Check perujuk tipe is exist
    let checkPerujuktipenama = await findOnePerujuktipe({
      where:{nama_tipe : req.body.nama_tipe, clinic_id:user.clinic_id,
             id:{[Op.not]: id}}
    });
    if (checkPerujuktipenama) {
      return ResponseHelper(res, 409, 'Nama Tipe Perujuk Sudah Terdaftar', [
        { message: 'Nama Tipe Perujuk Sudah Terdaftar', param: 'nama_tipe' },
      ]);
    }

    // Update perujuk tip
    await updatePerujuktipe({ ...req.body }, { where: { id } });

    const result = await findPerujuktipeById(id);

    return ResponseHelper(res, 201, 'Data Tipe Perujuk Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Tipe Perujuk Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check perujuk tip Exist Or Not
    let checkPerujuktipe = await findPerujuktipeById(id);
    if (!checkPerujuktipe) {
      return ResponseHelper(res, 409, 'Data Tipe Perujuk Tidak Tersedia', [
        { message: 'Data Tipe Perujuk Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete perujuk tupe
    let users = await findUserById(userId);

    await updatePerujuktipe({ deletedBy: users }, { where: { id } });

    await deletePerujuktipe(id);

    return ResponseHelper(res, 201, 'Data Tipe Perujuk Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Tipe Perujuk Gagal Dihapus', error.message);
  }
};

const upload = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    
    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }
    
    // console.log(path.dirname(__filename)+"\\myFile.xlsx");
    const workSheetsFromFile = xlsx.parse(req.files.import.data);
    // console.log(workSheetsFromFile[0].data);
    var arr = []
    workSheetsFromFile.forEach((row) => {
      row.data.forEach(async (data, index) => {
        let object = {};
        if (index > 8) {
          row.data[8].forEach(async (header, index) => {
            object[header.replace('*','')] = data[index];
          });
          if (object.nama_tipe) {
            arr.push(object)
          }
        }
      });
      // console.log(arr);
    });
    for (let i = 0; i < arr.length; i++) {
      const cek = await findOnePerujuktipe({where:{nama_tipe:arr[i].nama_tipe, clinic_id:user.clinic_id}})
      if (cek) {
        return ResponseHelper(res, 409, 'Error: baris ke '+(i+2)+', nama tipe '+arr[i].nama_tipe+' sudah terdaftar', arr[i].nama_tipe);
      }else{
        const tes = await createPerujuktipe(
          {nama_tipe:arr[i].nama_tipe, clinic_id:user.clinic_id}
        )
      }
    }
    return ResponseHelper(res, 201, 'Data Tipe Perujuk Berhasil Diimport');
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Tipe Perujuk Berhasil Diimport', error.message);
  }
};

export { get, add, update, deleted, upload };
