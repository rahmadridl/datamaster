import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'perujuktipe': {
      return [
        body('nama_tipe').not().isEmpty().withMessage('Nama Harus Diisi'),
        // body('clinic_id').not().isEmpty().withMessage('clinic id can not be empty'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
