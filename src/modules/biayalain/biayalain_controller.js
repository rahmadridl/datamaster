import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import xlsx from 'node-xlsx';
import { Op } from 'sequelize';
import {
  createBiayalain,
  findListBiayalain,
  findBiayalainById,
  updateBiayalain,
  deleteBiayalain,
  findOneBiayalain,
  createBiayalainpemeriksaan,
  findListBiayalainpemeriksaan,
  findListPaketBiayalainpemeriksaan,
  deleteBiayalainpemeriksaan,
} from './biayalain_repository.js';
import {
  findJenispemeriksaanById,
  findOneJenispemeriksaan,
} from '../jenispemeriksaan/jenispemeriksaan_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let biy = [];
    let jenis = [];
    let jenisnama = [];
    let pem;
    let bi;
    let biayalain = await findListBiayalain(requirement, page, limit);
    if (limit == 'all') {
      for (let i = 0; i < biayalain.length; i++) {
        bi = JSON.parse(biayalain[i].jenis_pemeriksaan_id);
        for (let j = 0; j < bi.length; j++) {
          pem = await findJenispemeriksaanById(bi[j]);
          if (pem.status == true) {
            jenis.push(pem.kode_jenis_pemeriksaan);
            jenisnama.push(pem.nama_jenis_pemeriksaan);
          }
        }
        biy.push({
          id: biayalain[i].id,
          nama: biayalain[i].nama,
          kode: biayalain[i].kode,
          nominal: biayalain[i].nominal,
          biaya_admin: biayalain[i].biaya_admin,
          ppn: biayalain[i].ppn,
          status: biayalain[i].status,
          clinic_id: biayalain[i].clinic_id,
          jenis_pemeriksaan_id: jenis,
          nama_jenis_pemeriksaan: jenisnama,
        });
        jenis = [];
        jenisnama = [];
      }
      const meta = {
        limit: limit,
        page: page,
        total_page: Math.ceil(biayalain.count / limit),
        total_data: biayalain.count,
      };

      return ResponseHelper(res, 200, 'Data Biaya Lain Berhasil Ditampilkan', biy, meta);
    } else {
      for (let i = 0; i < biayalain.rows.length; i++) {
        bi = JSON.parse(biayalain.rows[i].jenis_pemeriksaan_id);
        for (let j = 0; j < bi.length; j++) {
          pem = await findJenispemeriksaanById(bi[j]);
          if (pem.status == true) {
            jenis.push(pem.kode_jenis_pemeriksaan);
            jenisnama.push(pem.nama_jenis_pemeriksaan);
          }
        }
        biy.push({
          id: biayalain.rows[i].id,
          nama: biayalain.rows[i].nama,
          kode: biayalain.rows[i].kode,
          nominal: biayalain.rows[i].nominal,
          biaya_admin: biayalain.rows[i].biaya_admin,
          ppn: biayalain.rows[i].ppn,
          status: biayalain.rows[i].status,
          clinic_id: biayalain.rows[i].clinic_id,
          jenis_pemeriksaan_id: jenis,
          nama_jenis_pemeriksaan: jenisnama,
        });
        jenis = [];
        jenisnama = [];
      }

      const meta = {
        limit: parseInt(limit),
        page: page,
        total_page: Math.ceil(biayalain.count / limit),
        total_data: biayalain.count,
      };

      return ResponseHelper(res, 200, 'Data Biaya Lain Berhasil Ditampilkan', biy, meta);
    }
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Biaya Lain Gagal Ditampilkan', error.message);
  }
};

const findid = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    // const search = req.query.search || '';
    // const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (id) requirement.id = id;
    // if (status) requirement.status = status;

    let biy = [];
    let jenis = [];
    let jenisnama = [];
    let pem;
    let bi;
    // let biayalain = await findListBiayalain(requirement, page, limit);
    let biayalain = await findBiayalainById(id);

    // const meta = {
    //   limit: limit,
    //   page: page,
    //   total_page: Math.ceil(biayalain.count / limit),
    //   total_data: biayalain.count,
    // };

    return ResponseHelper(res, 200, 'Data Biaya Lain Berhasil Ditampilkan', biayalain);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Biaya Lain Gagal Ditampilkan', error.message);
  }
};

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let biayalainpemeriksaan = await findListBiayalainpemeriksaan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(biayalainpemeriksaan.count / limit),
      total_data: biayalainpemeriksaan.count,
    };

    return ResponseHelper(
      res,
      200,
      'Data Biaya Lain Berhasil Ditampilkan Pemeriksaan',
      biayalainpemeriksaan.rows,
      meta
    );
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Biaya Lain Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    function isNumber(n) {
      return !isNaN(parseFloat(n)) && !isNaN(n - 0);
    }

    if (req.body.nominal == '') {
      return ResponseHelper(res, 422, 'Nominal Harus Diisi', [
        { message: 'Nominal Harus Diisi', param: 'nominal' },
      ]);
    }
    if (req.body.nominal <0) {
      return ResponseHelper(res, 422, 'Nominal Harus Lebih Dari 0', [
        { message: 'Nominal Harus Lebih Dari 0', param: 'nominal' },
      ]);
    }
    if ( !isNumber(req.body.nominal)){
      return ResponseHelper(res, 422, 'Nominal Harus Diisi Angka', [
        { message: 'Nominal Harus Diisi Angka', param: 'nominal' },
      ]);
    }
    if (req.body.biaya_admin == '') {
      return ResponseHelper(res, 422, 'Biaya Admin Harus Diisi', [
        { message: 'Biaya Admin Harus Diisi', param: 'biaya_admin' },
      ]);
    }
    if (req.body.biaya_admin <0) {
      return ResponseHelper(res, 422, 'Biaya Admin Harus Lebih Dari 0', [
        { message: 'Biaya Admin Harus Lebih Dari 0', param: 'biaya_admin' },
      ]);
    }
    if (!isNumber(req.body.biaya_admin)) {
      return ResponseHelper(res, 422, 'Biaya Admin Harus Diisi Angka', [
        { message: 'Biaya Admin Harus Diisi Angka', param: 'biaya_admin' },
      ]);
    }
    if (req.body.ppn == '') {
      return ResponseHelper(res, 422, 'PPN Harus Diisi', [
        { message: 'PPN Harus Diisi', param: 'ppn' },
      ]);
    }

    // Check biaya lain is exist
    let checkDiskonnama = await findOneBiayalain({
      where: { nama: req.body.nama, clinic_id: user.clinic_id },
    });
    if (checkDiskonnama) {
      return ResponseHelper(res, 409, 'Nama Biaya Lain Sudah Terdaftar', [
        { message: 'Nama Biaya Lain Sudah Terdaftar', param: 'nama' },
      ]);
    }
    // Check code is exist
    let checkDiskonkode = await findOneBiayalain({
      where: { kode: req.body.kode, clinic_id: user.clinic_id },
    });
    if (checkDiskonkode) {
      return ResponseHelper(res, 409, 'Kode Biaya Lain Sudah Terdaftar', [
        { message: 'Kode Biaya Lain Sudah Terdaftar', param: 'kode' },
      ]);
    }
    var arrkode = [];
    var biayalain;
    for (let i = 0; i < req.body.pemeriksaan.length; i++) {
      const idkode = await findOneJenispemeriksaan({
        where: { kode_jenis_pemeriksaan: req.body.pemeriksaan[i].kode, clinic_id: user.clinic_id },
      });
      if (!idkode) {
        return ResponseHelper(res, 409, 'Kode Pemeriksaan Belum Terdaftar', [
        { message: 'Kode Pemeriksaan Belum Terdaftar', param: 'pemeriksaan.kode' }])
      }
      arrkode.push(idkode.id);
    }

    biayalain = await createBiayalain({
      ...req.body,
      jenis_pemeriksaan_id: arrkode,
      clinic_id: user.clinic_id,
    });

    //create paket biaya lain
    var biypaket;
    for (let i = 0; i < req.body.pemeriksaan.length; i++) {
      const idkode = await findOneJenispemeriksaan({
        where: { kode_jenis_pemeriksaan: req.body.pemeriksaan[i].kode, clinic_id: user.clinic_id },
      });
      biypaket = await createBiayalainpemeriksaan({
        m_biaya_lain_id: biayalain.id,
        m_jenis_pemeriksaan_id: idkode.id,
      });
    }
    return ResponseHelper(res, 201, 'Data Biaya Lain Berhasil Ditambahkan', biayalain);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Biaya Lain Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check dokter is exist
    let checkBiayalain = await findBiayalainById(id);
    if (!checkBiayalain) {
      return ResponseHelper(res, 409, 'Data Biaya Lain Tidak Terdaftar', [
        { message: 'Data Biaya Lain Tidak Terdaftar', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    function isNumber(n) {
      return !isNaN(parseFloat(n)) && !isNaN(n - 0);
    }
    if (req.body.nominal == '') {
      return ResponseHelper(res, 422, 'Nominal Harus Diisi', [
        { message: 'Nominal Harus Diisi', param: 'nominal' },
      ]);
    }
    if (req.body.nominal <0 ) {
      return ResponseHelper(res, 422, 'Nominal Harus Lebih Dari 0', [
        { message: 'Nominal Harus Lebih Dari 0', param: 'nominal' },
      ]);
    }
    if (!isNumber(req.body.nominal)) {
      return ResponseHelper(res, 422, 'Nominal Harus Diisi Angka', [
        { message: 'Nominal Harus Diisi Angka', param: 'nominal' },
      ]);
    }
    if (req.body.biaya_admin == '') {
      return ResponseHelper(res, 422, 'Biaya Admin Harus Diisi', [
        { message: 'Biaya Admin Harus Diisi', param: 'biaya_admin' },
      ]);
    }
    if (req.body.biaya_admin <0 ) {
      return ResponseHelper(res, 422, 'Biaya Admin Harus Lebih Dari 0', [
        { message: 'Biaya Admin Harus Lebih Dari 0', param: 'biaya_admin' },
      ]);
    }
    if (!isNumber(req.body.biaya_admin)) {
      return ResponseHelper(res, 422, 'Biaya Admin Harus Diisi Angka', [
        { message: 'Biaya Admin Harus Diisi Angka', param: 'biaya_admin' },
      ]);
    }
    if (req.body.ppn == '') {
      return ResponseHelper(res, 422, 'PPN Harus Diisi', [
        { message: 'PPN Harus Diisi', param: 'ppn' },
      ]);
    }

    // Check biaya lain is exist
    let checkBiayalainnama = await findOneBiayalain({
      where: { nama: req.body.nama, clinic_id: user.clinic_id, id: { [Op.not]: id } },
    });
    if (checkBiayalainnama) {
      return ResponseHelper(res, 409, 'Nama Biaya Lain Sudah Terdaftar', [
        { message: 'Nama Biaya Lain Sudah Terdaftar', param: 'nama' },
      ]);
    }
    // Check code is exist
    let checkBiayalainkode = await findOneBiayalain({
      where: { kode: req.body.kode, clinic_id: user.clinic_id, id: { [Op.not]: id } },
    });
    if (checkBiayalainkode) {
      return ResponseHelper(res, 409, 'Kode Biaya Lain Sudah Terdaftar', [
        { message: 'Kode Biaya Lain Sudah Terdaftar', param: 'kode' },
      ]);
    }

    var arrkode = [];
    for (let i = 0; i < req.body.pemeriksaan.length; i++) {
      const idkode = await findOneJenispemeriksaan({
        where: { kode_jenis_pemeriksaan: req.body.pemeriksaan[i].kode, clinic_id: user.clinic_id },
      });
      if (!idkode) {
        return ResponseHelper(res, 409, 'Kode Pemeriksaan Belum Terdaftar', [
        { message: 'Kode Pemeriksaan Belum Terdaftar', param: 'pemeriksaan.kode' }])
      }
      arrkode.push(idkode.id);
    }
    let mapping;

    const pak = await findListPaketBiayalainpemeriksaan(id);
    for (let i = 0; i < pak.length; i++) {
      await deleteBiayalainpemeriksaan(pak[i].id);
    }

    // Update biaya lain
    await updateBiayalain({ ...req.body, jenis_pemeriksaan_id: arrkode }, { where: { id } });
    for (let i = 0; i < req.body.pemeriksaan.length; i++) {
      const idkode = await findOneJenispemeriksaan({
        where: { kode_jenis_pemeriksaan: req.body.pemeriksaan[i].kode, clinic_id: user.clinic_id},
      });
      mapping = await createBiayalainpemeriksaan({
        m_biaya_lain_id: id,
        m_jenis_pemeriksaan_id: idkode.id,
      });
    }

    const result = await findBiayalainById(id);

    return ResponseHelper(res, 201, 'Data Biaya Lain Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Biaya Lain Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check Dokter Exist Or Not
    let checkBiayalain = await findBiayalainById(id);
    if (!checkBiayalain) {
      return ResponseHelper(res, 409, 'Biaya Lain Belum Terdaftar', [
        { message: 'Biaya Lain Belum Terdaftar', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete biayalain
    let users = await findUserById(userId);

    await updateBiayalain({ deletedBy: users }, { where: { id } });

    await deleteBiayalain(id);

    return ResponseHelper(res, 201, 'Data Biaya Lain Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Biaya Lain Gagal Dihapus', error.message);
  }
};

const upload = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    // console.log(req.files.import);

    // const mantap = req.files.fileName
    // const mantatap = mantap
    // console.log(mantatap.toString('utf8'));
    
    // Check role admin
    let user = await findUserById(user_id);

    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }
    
    // console.log(path.dirname(__filename)+"\\myFile.xlsx");
    const workSheetsFromFile = xlsx.parse(req.files.import.data);
    // console.log(workSheetsFromFile[0].data);
    var arr = []
    workSheetsFromFile.forEach((row) => {
      // console.log(row);
      row.data.forEach(async (data, index) => {
        // console.log(index);
        // console.log(data);
        let object = {};
        if (index > 8) {
          row.data[8].forEach(async (header, index) => {
            object[header.replace('*','')] = data[index];
          });
          console.log('object', object);
          if (object.kode && object.nominal && object.biaya_admin && object.ppn) {
            arr.push(object)
          }
        }
      });
      console.log(arr);
    });
    for (let i = 0; i < arr.length; i++) {
      console.log(arr[i].kode);
      const cek = await findOneBiayalain({where:{kode:arr[i].kode, clinic_id:user.clinic_id}})
      if (!cek) {
        return ResponseHelper(res, 409, 'Error: baris ke '+(i+2)+', kode '+arr[i].kode+' tidak ditemukan', arr[i].kode);
      }
    }
    for (let i = 0; i < arr.length; i++) {
      const tes = await updateBiayalain(
        {nominal:arr[i].nominal, biaya_admin:arr[i].biaya_admin, ppn:arr[i].ppn},{where:{kode:arr[i].kode, clinic_id:user.clinic_id}}
      )
    }
    return ResponseHelper(res, 201, 'Nominal Biaya Lain Berhasil Diubah');
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Nominal Biaya Lain Gagal Diubah', error.message);
  }
};

export { get, findid, get1, add, update, deleted, upload };
