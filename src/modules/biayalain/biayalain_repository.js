import db from '../../database/models/index.js';
const { Biayalain } = db;
const { Biayalainpemeriksaan } = db;
import { Op } from 'sequelize';

// Find one biaya lain by id
const findBiayalainById = async (id) => {
  try {
    let result = await Biayalain.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findBiayalainById', error);
    throw new Error(error);
  }
};

// Find one Biayalain by filter
const findOneBiayalain = async (filter) => {
  try {
    let result = await Biayalain.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneBiayalain', error);
    throw new Error(error);
  }
};

// Find list Biayalain
const findListBiayalain = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Biayalain.findAll({
        where: {
          nama: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Biayalain.findAndCountAll({
        where: {
          nama: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListBiayalain', error);
    throw new Error(error);
  }
};

// Find list Biayalainpemeriksaan
const findListBiayalainpemeriksaan = async (filter, page, limit) => {
  try {
    let result = await Biayalainpemeriksaan.findAndCountAll({
      ...filter,
      order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPaketpemeriksaan', error);
    throw new Error(error);
  }
};

// Find list Biayalainpemeriksaan
const findListPaketBiayalainpemeriksaan = async (id) => {
  try {
    let result = await Biayalainpemeriksaan.findAll({
      where: {
        m_biaya_lain_id: id,
        // clinic_id: status
      },
      order: [['id', 'DESC']],
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPaketpemeriksaan', error);
    throw new Error(error);
  }
};

// Create new Biayalain
const createBiayalain = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Biayalain.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createBiayalain', error);
    throw new Error(error);
  }
};

// Create new Biayalain pemeriksaan
const createBiayalainpemeriksaan = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Biayalainpemeriksaan.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createBiayalain', error);
    throw new Error(error);
  }
};

// Update Biayalain
const updateBiayalain = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Biayalain.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateBiayalain', error);
    throw new Error(error);
  }
};

//delete Biayalain
const deleteBiayalain = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Biayalain.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteBiayalain', error);
    throw new Error(error);
  }
};

//delete Biayalain
const deleteBiayalainpemeriksaan = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Biayalainpemeriksaan.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteBiayalain', error);
    throw new Error(error);
  }
};

export {
  findBiayalainById,
  findListPaketBiayalainpemeriksaan,
  findOneBiayalain,
  findListBiayalain,
  findListBiayalainpemeriksaan,
  createBiayalain,
  createBiayalainpemeriksaan,
  updateBiayalain,
  deleteBiayalain,
  deleteBiayalainpemeriksaan,
};
