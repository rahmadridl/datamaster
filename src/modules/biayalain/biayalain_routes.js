import { get, findid, get1, add, update, deleted, upload } from './biayalain_controller.js';
import validator from './biayalain_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const BiayalainRoutes = (app, prefix) => {
  app.route(`${prefix}/biayalain`).get(AuthMiddleware, get);
  app.route(`${prefix}/biayalain/findid/:id`).get(findid);
  app.route(`${prefix}/biayalainpaketnotoken`).get(get1);
  app.route(`${prefix}/biayalain/import`).post(AuthMiddleware, upload);
  app.route(`${prefix}/biayalain/add`).post(AuthMiddleware, validator('biayalain'), add);
  app.route(`${prefix}/biayalain/update/:id`).patch(AuthMiddleware, validator('biayalain'), update);
  app.route(`${prefix}/biayalain/delete/:id`).delete(AuthMiddleware, deleted);
};

export { BiayalainRoutes };
