import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'biayalain': {
      return [
        body('kode').not().isEmpty().withMessage('Kode Harus Diisi'),
        body('nama').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('nominal').not().isEmpty().withMessage('Nominal Harus Diisi'),
        body('biaya_admin').not().isEmpty().withMessage('Biaya Admin Harus Diisi'),
        body('ppn').not().isEmpty().withMessage('PPN Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
