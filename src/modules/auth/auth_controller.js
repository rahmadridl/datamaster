import { validationResult } from 'express-validator';
import { generatePassword, generateToken, comparePassword } from '../../helpers/authentication.js';
import { createUser } from './auth_repository.js';
import { findOneUser, updateUser } from '../user/user_repository.js';
import { findClinicById } from '../clinic/clinic_repository.js';
import ResponseHelper from '../../helpers/response_helper.js';

import bcrypt from 'bcryptjs';

// Register new user
const register = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const {
      id,
      email,
      password,
      confirm_password,
      name,
      role,
      phone,
      clinic_id
    } = req.body;

    // Checking available user
    let check_email = await findOneUser({
      where: { email },
    });

    let check_phone = await findOneUser({
      where: { phone },
    });

    if (check_email) {
      return ResponseHelper(res, 409, 'Email Sudah Terdaftar', [
        { message: 'Email Sudah Terdaftar', param: 'email' },
      ]);
    }

    if (check_phone) {
      return ResponseHelper(res, 409, 'Nomor Telfon Sudah Terdaftar', [
        { message: 'Nomor Telfon Sudah Terdaftar', param: 'phone' },
      ]);
    }

    // Check Confirm Password
    if (password !== confirm_password) {
      return ResponseHelper(res, 422, 'Password Tidak Sama', [
        { message: 'Password Tidak Sama', param: 'password' },
      ]);
    }

    const password_hash = await generatePassword(password);
    const new_user = await createUser({
      id,
      email,
      password: password_hash,
      name,
      role,
      phone,
      clinic_id
    });

//console.log(new_user)
  //  let user = await findOneUser({ where: { id: new_user.id } });

//    const token = generateToken(user.id);

  //  await updateUser(
  //    { token: token, last_login: new Date(Date.now()) },
  //    { where: { id: new_user.id } }
   // );

//    delete user.password;

    return ResponseHelper(res, 200, `Registrasi Pengguna Baru Sukses`);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Registrasi Pengguna Baru Gagal', error.message);
  }
};

// Register new user admin
const registersuperadmin = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'role' },
      ]);
    }

    const {
      id,
      email,
      password,
      confirm_password,
      name,
      role,
      phone,
      clinic_id = user.clinic_id
    } = req.body;

    // Checking available user
    let check_email = await findOneUser({
      where: { email },
    });

    let check_phone = await findOneUser({
      where: { phone },
    });

    if (check_email) {
      return ResponseHelper(res, 409, 'Email Sudah Terdaftar', [
        { message: 'Email Sudah Terdaftar', param: 'email' },
      ]);
    }

    if (check_phone) {
      return ResponseHelper(res, 409, 'Nomor Telfon Sudah Terdaftar', [
        { message: 'Nomor Telfon Sudah Terdaftar', param: 'phone' },
      ]);
    }

    // Check Confirm Password
    if (password !== confirm_password) {
      return ResponseHelper(res, 422, 'Password Tidak Sama', [
        { message: 'Password Tidak Sama', param: 'confirm_password' },
      ]);
    }

    const password_hash = await generatePassword(password);
    const new_user = await createUser({
      id,
      email,
      password: password_hash,
      name,
      role,
      phone,
      clinic_id: user.clinic_id
    });

//console.log(new_user)
  //  let user = await findOneUser({ where: { id: new_user.id } });

//    const token = generateToken(user.id);

  //  await updateUser(
  //    { token: token, last_login: new Date(Date.now()) },
  //    { where: { id: new_user.id } }
   // );

//    delete user.password;

    return ResponseHelper(res, 200, `Registrasi Pengguna Baru Sukses`);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Registrasi Pengguna Baru Gagal', error.message);
  }
};

// Login user
const login = async (req, res) => {
  console.log(req.body);
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { email, password } = req.body;

    let user = await findOneUser({ where: { email } });

    // Check Email
    if (!user) {
      return ResponseHelper(res, 401, 'Email Belum Terdaftar', [
        { message: 'Email Belum Terdaftar', param: 'email' },
      ]);
    }

    const isMatch = await comparePassword(password, user.password);
    const validPassword = await bcrypt.compare(password, user.password);

    // Check Password
    if (!isMatch) {
      return ResponseHelper(res, 401, 'Password Salah', [
        { message: 'Password Salah', param: 'password' },
      ]);
    }

    const token = generateToken(user.id,user.clinic_id);
    delete user.password;
    const clinic = await findClinicById(user.clinic_id);
    var data
    if (clinic) {
      data ={
        "clinic_name": clinic.clinic_name
      }
    }

    return ResponseHelper(res, 200, 'Login Sukses', { token, data
       });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Login Gagal', error.message);
  }
};

export { register, registersuperadmin, login };
