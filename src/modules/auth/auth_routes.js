import { register, login, registersuperadmin } from './auth_controller.js';
import validator from './auth_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const AuthRoutes = (app, prefix) => {
  app.route(`${prefix}/register`).post(validator('register'), register);
  app.route(`${prefix}/register/superadmin`).post(AuthMiddleware, validator('register'), registersuperadmin);
  app.route(`${prefix}/login`).post(validator('login'), login);
};

export { AuthRoutes };
