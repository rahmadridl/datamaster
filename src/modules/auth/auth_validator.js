import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'register': {
      return [
        body('email').not().isEmpty().withMessage('Email Harus Diisi'),
        body('email').isEmail().withMessage('Format Email Salah'),
        body('password').not().isEmpty().withMessage('Password Harus Diisi'),
        body('confirm_password').not().isEmpty().withMessage('Password Harus Diisi'),
        body('role').not().isEmpty().withMessage('Role Harus Diisi'),
        body('name').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('phone').not().isEmpty().withMessage('Nomor Telfon Harus Diisi'),
        body('clinic_id').not().isEmpty().withMessage('Klinik Harus Diisi'),
      ];
    }
    case 'login': {
      return [
        body('email').not().isEmpty().withMessage('Email Harus Diisi'),
        body('email').isEmail().withMessage('Format Email Salah'),
        body('password').not().isEmpty().withMessage('Password Harus Diisi'),
      ];
    }

    default:
      return [];
  }
};

export default validate;
