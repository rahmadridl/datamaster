import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'supplier': {
      return [
        body('kode_supplier').not().isEmpty().withMessage('kode Harus Diisi'),
        body('nama_supplier').not().isEmpty().withMessage('name Harus Diisi'),
        // body('creator_id').not().isEmpty().withMessage('creator id Harus Diisi'),
        // body('clinic_id').not().isEmpty().withMessage('clinic id Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
