import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import {
  createSupplier,
  findListSupplier,
  findSupplierById,
  updateSupplier,
  deleteSupplier
} from './supplier_repository';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let supplier = await findListSupplier(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(supplier.count / limit),
      total_data: supplier.count,
    };

    return ResponseHelper(res, 200, 'success get list data supplier', supplier.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed get supplier', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    // Check supplier kode is exist
    let checkSupplier = await findOneLayananpoli({
      where:{kode_supplier : req.body.kode_supplier}
    });
    if (checkSupplier) {
      return ResponseHelper(res, 409, 'kode_supplier already registered', [
        { message: 'kode_supplier already registered', param: 'kode_supplier' },
      ]);
    }

    const supplier = await createSupplier({ ...req.body });
    await updateSupplier(
      {clinic_id:user.clinic_id},
      {where: {id: supplier.id}}
    )
    return ResponseHelper(res, 201, 'success create new supplier', supplier);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed create new supplier', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check supplier is exist
    let checkSupplier = await findSupplierById(id);
    if (!checkSupplier) {
      return ResponseHelper(res, 409, 'Supplier is not exist', [
        { message: 'Supplier is not exist', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check supplier kode is exist
    let checkSupplierkode = await findOneLayananpoli({
      where:{kode_supplier : req.body.kode_supplier,
        id:{[Op.not]: id}}
    });
    if (checkSupplierkode) {
      return ResponseHelper(res, 409, 'kode_supplier already registered', [
        { message: 'kode_supplier already registered', param: 'kode_supplier' },
      ]);
    }

    // Update supplier
    await updateSupplier({ ...req.body }, { where: { id } });

    const result = await findSupplierById(id);

    return ResponseHelper(res, 201, 'success updated selected Supplier', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed updated selected Supplier', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check supplier Exist Or Not
    let checkSupplier = await findSupplierById(id);
    if (!checkSupplier) {
      return ResponseHelper(res, 409, 'Supplier Is Not Exist In Database', [
        { message: 'Supplier Is Not Exist In Database', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete supplier
    let users = await findUserById(userId);

    await updateSupplier({ deletedBy: users }, { where: { id } });

    await deleteSupplier(id);

    return ResponseHelper(res, 201, 'Success Deleted Selected Supplier', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Failed Deleted Selected Supplier', error.message);
  }
};

export { get, add, update, deleted };
