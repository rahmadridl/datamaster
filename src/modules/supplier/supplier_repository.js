import db from '../../database/models/index.js';
const { Supplier } = db;
import { Op } from 'sequelize';

// Find one supplier by id
const findSupplierById = async (id) => {
  try {
    let result = await Supplier.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findSupplierById', error);
    throw new Error(error);
  }
};

// Find one Supplier by filter
const findOneSupplier = async (filter) => {
  try {
    let result = await Supplier.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneSupplier', error);
    throw new Error(error);
  }
};

// Find list Supplier
const findListSupplier = async ({ search, status }, page, limit) => {
  try {
    let result = await Supplier.findAndCountAll({
      where: {
        nama_supplier: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        clinic_id: status
      },order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListSupplier', error);
    throw new Error(error);
  }
};

// Create new Supplier
const createSupplier = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Supplier.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createSupplier', error);
    throw new Error(error);
  }
};

// Update Supplier
const updateSupplier = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Supplier.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateSupplier', error);
    throw new Error(error);
  }
};

//delete Supplier
const deleteSupplier = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Supplier.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteSupplier', error);
    throw new Error(error);
  }
};

export { findSupplierById, findOneSupplier, findListSupplier, createSupplier, updateSupplier, deleteSupplier };
