import { get, add, update, deleted } from './supplier_controller.js';
import validator from './supplier_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const SupplierRoutes = (app, prefix) => {
  app.route(`${prefix}/supplier`).get(AuthMiddleware, get);
  app.route(`${prefix}/supplier/add`).post(AuthMiddleware, validator('supplier'), add);
  app.route(`${prefix}/supplier/update/:id`).patch(AuthMiddleware, validator('supplier'), update);
  app.route(`${prefix}/supplier/delete/:id`).delete(AuthMiddleware, deleted);
};

export { SupplierRoutes };
