import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import xlsx from 'node-xlsx';
import { Op } from 'sequelize';
import {
  createDokter,
  findListDokter,
  findOneDokter,
  findDokterById,
  updateDokter,
  deleteDokter,
  findAllDokter,
  findListDokternotoken,
} from './dokter_repository.js';
import { findOneSpesialisasi } from '../spesialisasi/spesialisasi_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const nip = req.query.nip || '';
    const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';
    // let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (nip) requirement.nip = nip;
    if (status||status==0) requirement.status = status;

    let dokter = await findListDokter(requirement, page, limit);
    // const dokall = await findAllDokter({where:{status:'aktif'}});
    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit) == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(dokter.count / limit),
      total_data: dokter.count,
    };

    return ResponseHelper(
      res,
      200,
      'Data Dokter Berhasil Ditampilkan',
      limit == 'all' ? dokter : dokter.rows,
      meta
    );
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Gagal Ditampilkan', error.message);
  }
};

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let dokter = await findListDokternotoken(requirement, page, limit);
    // const dokall = await findAllDokter({where:{status:'aktif'}});
    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(dokter.count / limit),
      total_data: dokter.count,
    };

    return ResponseHelper(res, 200, 'Data Dokter Berhasil Ditampilkan', dokter.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Gagal Ditampilkan', error.message);
  }
};

const getid = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;

    let dokter = await findDokterById(id)
    if (dokter == null) {
      return ResponseHelper(res, 409, 'Data Dokter Tidak Tersedia', [
        { message: 'Data Dokter Tidak Tersedia', param: 'id' },
      ]);
    }
    // let dokter = await findListDokternotoken(requirement, page, limit);
    // const dokall = await findAllDokter({where:{status:'aktif'}});
    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(dokter.count / limit),
      total_data: dokter.count,
    };

    return ResponseHelper(res, 200, 'Data Dokter Berhasil Ditampilkan', dokter, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);
    console.log(user.role);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check dokter is exist
    let checkDokter = await findOneDokter({
      where: { nip: req.body.nip, clinic_id:user.clinic_id },
    });
    if (checkDokter) {
      return ResponseHelper(res, 409, 'NIP Sudah Terdaftar', [
        { message: 'NIP Sudah Terdaftar', param: 'nip' },
      ]);
    }

    const dokter = await createDokter({ ...req.body });
    await updateDokter({ clinic_id: user.clinic_id }, { where: { id: dokter.id } });
    return ResponseHelper(res, 201, 'Data Dokter Berhasil Ditambahkan', dokter);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }
    // Check dokter is exist
    let checkDokter = await findDokterById(id);
    if (!checkDokter) {
      return ResponseHelper(res, 409, 'Data Dokter Tidak Tersedia', [
        { message: 'Data Dokter Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check dokter is exist
    let checkDokternip = await findOneDokter({
      where: { nip: req.body.nip, clinic_id:user.clinic_id, id: { [Op.not]: id } },
    });
    console.log(checkDokternip);
    if (checkDokternip) {
      return ResponseHelper(res, 409, 'NIP Dokter Sudah Terdaftar', [
        { message: 'NIP Dokter Sudah Terdaftar', param: 'nip' },
      ]);
    }


    // Update dokter
    await updateDokter({ ...req.body }, { where: { id } });

    const result = await findDokterById(id);

    return ResponseHelper(res, 201, 'Data Dokter Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check Dokter Exist Or Not
    let checkDokter = await findDokterById(id);
    if (!checkDokter) {
      return ResponseHelper(res, 409, 'Data Dokter Tidak Tersedia', [
        { message: 'Data Dokter Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete dokter
    let users = await findUserById(userId);

    await updateDokter({ deletedBy: users }, { where: { id } });

    await deleteDokter(id);

    return ResponseHelper(res, 201, 'Data Dokter Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Gagal Dihapus', error.message);
  }
};

const upload = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    // console.log(req.files.import);

    // const mantap = req.files.fileName
    // const mantatap = mantap
    // console.log(mantatap.toString('utf8'));

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // console.log(path.dirname(__filename)+"\\myFile.xlsx");
    const workSheetsFromFile = xlsx.parse(req.files.import.data);
    // console.log(workSheetsFromFile[0].data);
    var arr = [];
    var spesialis = [];
    workSheetsFromFile.forEach((row) => {
      // console.log(row);
      row.data.forEach(async (data, index) => {
        // console.log(index);
        // console.log(data);
        let object = {};
        if (index > 8) {
          row.data[8].forEach(async (header, index) => {
            console.log(header);
            object[header.replace('*','')] = data[index];
          });
          // console.log('object', object);
          if (object.nama && object.nip && object.spesialis) {
            arr.push(object);
          }
        }
      });
    });
    for (let i = 0; i < arr.length; i++) {
      const cek = await findOneSpesialisasi({ where: { nama_spesialisasi: arr[i].spesialis, clinic_id:user.clinic_id, status:true } });
      const ceknip = await findOneDokter({ where: { nip: arr[i].nip.toString(), clinic_id:user.clinic_id } });
      if (!cek) {
        return ResponseHelper(
          res,
          409,
          'Error: baris ke ' +
            (i + 2) +
            ', nama spesialisasi ' +
            arr[i].spesialis +
            ' tidak ditemukan',
          arr[i].spesialis
        );
      }
      spesialis.push(cek.id);
      if (ceknip) {
        const edit = await updateDokter({nama_dokter: arr[i].nama,
          nip: arr[i].nip,
          spesialisasi: spesialis[i],
          position: arr[i].jabatan},{where:{
            id:ceknip.id
          }})
        // return ResponseHelper(
        //   res,
        //   409,
        //   'Error: baris ke ' + (i + 2) + ', nip ' + arr[i].nip + ' sudah terdaftar',
        //   arr[i].nip
        // );
      }else{
        const tes = await createDokter({
          nama_dokter: arr[i].nama,
          nip: arr[i].nip,
          spesialisasi: spesialis[i],
          position: arr[i].jabatan,
          notelp: arr[i].notelp,
          idbpjs: arr[i].bpjs,
          clinic_id: user.clinic_id,
        });
      }
    }
    // for (let i = 0; i < arr.length; i++) {
    // }
    return ResponseHelper(res, 201, 'Data Dokter Berhasil Diimport');
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Gagal Diimport', error.message);
  }
};

export { get, get1, getid, add, update, deleted, upload };
