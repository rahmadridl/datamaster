import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'dokter': {
      return [
        body('spesialisasi').not().isEmpty().withMessage('Spesialisasi Harus Diisi'),
        body('nama_dokter').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('nip').not().isEmpty().withMessage('NIP Harus Diisi'),
        body('position').not().isEmpty().withMessage('Posisi Harus Diisi'),
        // body('clinic_id').not().isEmpty().withMessage('clinic id Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
