import db from '../../database/models/index.js';
const { Dokter } = db;
import { Op } from 'sequelize';

// Find one dokter by id
const findDokterById = async (id) => {
  try {
    let result = await Dokter.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findDokterById', error);
    throw new Error(error);
  }
};

// Find one dokter by filter
const findOneDokter = async (filter) => {
  try {
    let result = await Dokter.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneDokter', error);
    throw new Error(error);
  }
};

// Find all dokter by filter
const findAllDokter = async (filter) => {
  try {
    let result = await Dokter.findAll({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneDokter', error);
    throw new Error(error);
  }
};

// Find list dokter
const findListDokter = async ({ nip, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Dokter.findAll({
        where: {
          nip: nip ? { [Op.like]: `%${nip}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit=parseInt(limit)
      result = await Dokter.findAndCountAll({
        where: {
          nip: nip ? { [Op.like]: `%${nip}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListDokter', error);
    throw new Error(error);
  }
};

// Find list dokter no token
const findListDokternotoken = async ({ search, status }, page, limit) => {
  try {
    let result = await Dokter.findAndCountAll({
      where: {
        nama_dokter: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        // clinic_id: status ,
      },
      order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListDokter', error);
    throw new Error(error);
  }
};

// Create new dokter
const createDokter = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Dokter.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createDokter', error);
    throw new Error(error);
  }
};

// Update dokter
const updateDokter = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Dokter.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateDokter', error);
    throw new Error(error);
  }
};

//delete dokter
const deleteDokter = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Dokter.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteDokter', error);
    throw new Error(error);
  }
};

export {
  findDokterById,
  findOneDokter,
  findAllDokter,
  findListDokter,
  createDokter,
  updateDokter,
  deleteDokter,
  findListDokternotoken,
};
