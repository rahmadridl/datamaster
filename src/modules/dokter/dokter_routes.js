import { get, get1, add, update, deleted, upload, getid } from './dokter_controller.js';
import validator from './dokter_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const DokterRoutes = (app, prefix) => {
  app.route(`${prefix}/dokter`).get(AuthMiddleware, get);
  app.route(`${prefix}/dokter/import`).post(AuthMiddleware, upload);
  app.route(`${prefix}/dokternotoken`).get( get1);
  app.route(`${prefix}/dokteridnotoken/:id`).get( getid);
  app.route(`${prefix}/dokter/add`).post(AuthMiddleware, validator('dokter'), add);
  app.route(`${prefix}/dokter/update/:id`).patch(AuthMiddleware, validator('dokter'), update);
  app.route(`${prefix}/dokter/delete/:id`).delete(AuthMiddleware, deleted);
};

export { DokterRoutes };
