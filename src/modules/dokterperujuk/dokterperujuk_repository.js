import db from '../../database/models/index.js';
const { Dokterperujuk } = db;
import { Op } from 'sequelize';

// Find one dokter perujuk by id
const findDokterperujukById = async (id) => {
  try {
    let result = await Dokterperujuk.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findDokterperujukById', error);
    throw new Error(error);
  }
};

// Find one dokter perujuk by filter
const findOneDokterperujuk = async (filter) => {
  try {
    let result = await Dokterperujuk.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneDokterperujuk', error);
    throw new Error(error);
  }
};

// Find all dokter perujuk by filter
const findAllDokterperujuk = async (filter) => {
  try {
    let result = await Dokterperujuk.findAll({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findAllDokterperujuk', error);
    throw new Error(error);
  }
};

// Find list dokter perujuk
const findListDokterperujuk = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Dokterperujuk.findAll({
        where: {
          nama_dokter_perujuk: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Dokterperujuk.findAndCountAll({
        where: {
          nama_dokter_perujuk: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListDokterperujuk', error);
    throw new Error(error);
  }
};
// Find list dokter perujuk
const findListDokterperujuknotoken = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Dokterperujuk.findAll({
        where: {
          nama_dokter_perujuk: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Dokterperujuk.findAndCountAll({
        where: {
          nama_dokter_perujuk: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          // clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListDokterperujuk', error);
    throw new Error(error);
  }
};

// Find list dokter perujuk no token
// const findListDokterperujuknotokenfilter = async ({ search, status, tipe }, page, limit) => {
//   try {
//     let result;
//     if (limit == 'all') {
//       result = await Dokterperujuk.findAll({
//         where: {
//           nama_dokter_perujuk: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
//           clinic_id: status,
//           tipe: tipe,
//           status: true,
//         },
//         order: [['id', 'DESC']],
//       });
//     } else {
//       limit = parseInt(limit);
//       result = await Dokterperujuk.findAndCountAll({
//         where: {
//           nama_perujuk: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
//           clinic_id: status,
//           tipe: tipe
//         },
//         order: [['id', 'DESC']],
//         offset: limit * (page - 1),
//         limit: limit,
//       });
//     }
//     return result;
//   } catch (error) {
//     console.error('[EXCEPTION] findListDokterperujuk', error);
//     throw new Error(error);
//   }
// };

// Create new perujuk
const createDokterperujuk = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Dokterperujuk.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createDokterperujuk', error);
    throw new Error(error);
  }
};

// Update perujuk
const updateDokterperujuk = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Dokterperujuk.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateDokterperujuk', error);
    throw new Error(error);
  }
};

//delete perujuk
const deleteDokterperujuk = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Dokterperujuk.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteDokterperujuk', error);
    throw new Error(error);
  }
};

export {
  findDokterperujukById,
  findOneDokterperujuk,
  findAllDokterperujuk,
  findListDokterperujuk,
  createDokterperujuk,
  updateDokterperujuk,
  deleteDokterperujuk,
  findListDokterperujuknotoken,
  // findListDokterperujuknotokenfilter
};
