import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import xlsx from 'node-xlsx';
import { Op } from 'sequelize';
import {
  createDokterperujuk,
  findListDokterperujuk,
  findOneDokterperujuk,
  findDokterperujukById,
  updateDokterperujuk,
  deleteDokterperujuk,
  findListDokterperujuknotoken,
  findListDokterperujuknotokenfilter,
  findAllDokterperujuk,
} from './dokterperujuk_repository.js';
// import { findOneDokterperujuktipe } from '../perujuktipe/perujuktipe_repository.js';
import { findOneClinic } from '../clinic/clinic_repository.js';
import { findOnePerujuk, findPerujukById } from '../perujuk/perujuk_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let perujuk = await findListDokterperujuk(requirement, page, limit);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(perujuk.count / limit),
      total_data: perujuk.count,
    };
    
    var id_perujuk
    var dokter_perujuk = []
    if (limit == 'all') {
      for (let i = 0; i < perujuk.length; i++) {
        id_perujuk = await findPerujukById(perujuk[i].id_perujuk)
        delete perujuk.id_perujuk
        dokter_perujuk.push({...perujuk[i].dataValues, nama_perujuk:id_perujuk.nama_perujuk})
      }
    } else {
      for (let i = 0; i < perujuk.rows.length; i++) {
        id_perujuk = await findPerujukById(perujuk.rows[i].id_perujuk)
        delete perujuk.rows[i].id_perujuk
        dokter_perujuk.push({...perujuk.rows[i].dataValues, nama_perujuk:id_perujuk.nama_perujuk})
      }
    }

    return ResponseHelper(
      res,
      200,
      'Data Dokter Perujuk Berhasil Ditampilkan',
      // limit == 'all' ? perujuk : perujuk.rows,
      dokter_perujuk,
      meta
    );
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Perujuk Gagal Ditampilkan', error.message);
  }
};

const find = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;
    const { clinic_id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    // const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    // if (status) requirement.status = status;

    // let perujuk = await findListPerujuk(requirement, page, limit);
    let perujuk = await findOneDokterperujuk({where:{kode_dokter_perujuk:id, clinic_id: clinic_id}})    
    if (!perujuk) {
      return ResponseHelper(res, 409, ' Data Dokter Perujuk Tidak Tersedia', id)
    }

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(perujuk.count / limit),
      total_data: perujuk.count,
    };

    return ResponseHelper(
      res,
      200,
      'Data Dokter Perujuk Berhasil Ditampilkan',
      perujuk,
    );
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Perujuk Gagal Ditampilkan', error.message);
  }
};

const getperujukid = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    // const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    // if (status) requirement.status = status;

    // let perujuk = await findListPerujuk(requirement, page, limit);
    let perujuk = await findPerujukById(id)
    let dokterperujuk = await findAllDokterperujuk({where:{id_perujuk:perujuk.id, status:true, clinic_id:perujuk.clinic_id}})
    // let perujuk = await findOneDokterperujuk({kode_perujuk:id})

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(dokterperujuk.count / limit),
      total_data: dokterperujuk.count,
    };

    return ResponseHelper(
      res,
      200,
      'Data Dokter Perujuk Berhasil Ditampilkan',
      dokterperujuk,
    );
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Perujuk Gagal Ditampilkan', error.message);
  }
};

const getperujuk = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    // const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    // if (status) requirement.status = status;

    // let perujuk = await findListPerujuk(requirement, page, limit);
    // let perujuk = await findPerujukById(id)
    let perujuk = await findOnePerujuk({where:{kode_perujuk:id, clinic_id:user.clinic_id}})
    if (!perujuk) {
      return ResponseHelper(res, 401, 'Data Kode Dokter Perujuk Tidak Tersedia', [
        { message: 'Data Kode Dokter Perujuk Tidak Tersedia', param: id },
      ]);
    }
    let dokterperujuk = await findAllDokterperujuk({where:{id_perujuk:perujuk.id, status:true, clinic_id:perujuk.clinic_id}})
    // let perujuk = await findOneDokterperujuk({kode_perujuk:id})

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(dokterperujuk.count / limit),
      total_data: dokterperujuk.count,
    };

    return ResponseHelper(
      res,
      200,
      'Data Dokter Perujuk Berhasil Ditampilkan',
      dokterperujuk,
    );
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Perujuk Gagal Ditampilkan', error.message);
  }
};

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    
    const search = req.query.search || '';
    let status = req.query.status || 'RS10';
    let tipe = req.query.tipe || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';
    const clinic = await findOneClinic({where:{clinic_code:status}})
    status = clinic.id
    const tipeperujuk = await findOneDokterperujuktipe({where:{nama_tipe:tipe}})
    tipe = tipeperujuk.id
    
    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;
    if (tipe) requirement.tipe = tipe;
    
    let perujuk = await findListDokterperujuknotokenfilter(requirement, page, limit);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(perujuk.count / limit),
      total_data: perujuk.count,
    };

    return ResponseHelper(res, 200, 'Data Dokter Perujuk Berhasil Ditampilkan', perujuk, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Perujuk Gagal Ditampilkan', error.message);
  }
};

const getno = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    
    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';
    
    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;
    
    let perujuk = await findListDokterperujuknotoken(requirement, page, limit);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(perujuk.count / limit),
      total_data: perujuk.count,
    };

    return ResponseHelper(res, 200, 'Data Dokter Perujuk Berhasil Ditampilkan', perujuk, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Perujuk Gagal Ditampilkan', error.message);
  }
};

const getid = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    
    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';
    
    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;
    
    let perujuk = await findDokterperujukById(id)
    if (perujuk == null) {
      return ResponseHelper(res, 409, 'Perujuk Tidak Tersedia', [
        { message: 'Perujuk Tidak Tersedia', param: 'id' },
      ]);
    }
    // let Perujuk = await findListDokterperujuknotoken(requirement, page, limit);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(perujuk.count / limit),
      total_data: perujuk.count,
    };

    return ResponseHelper(res, 200, 'Data Dokter Perujuk Berhasil Ditampilkan', perujuk, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Perujuk Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    if (req.body.kode_dokter_perujuk == null || req.body.kode_dokter_perujuk == '' || req.body.kode_dokter_perujuk == '-') {
      return ResponseHelper(res, 422, 'Kode Dokter Perujuk Harus Diisi', [
        { message: 'Kode Dokter Perujuk Harus Diisi', param: 'kode_dokter_perujuk' },
      ]);
    }
    
    // Check nama_dokter_perujuk is exist
    let checkDokterperujuk = await findOneDokterperujuk({
      where: { nama_dokter_perujuk: req.body.nama_dokter_perujuk, clinic_id:user.clinic_id },
    });
    if (checkDokterperujuk) {
      return ResponseHelper(res, 409, 'nama_dokter_perujuk Sudah Terdaftar', [
        { message: 'nama_dokter_perujuk Sudah Terdaftar', param: 'nama_dokter_perujuk' },
      ]);
    }

    // Check kode_perujuk is exist
    let checkDokterperujukkode = await findOneDokterperujuk({
      where: { kode_dokter_perujuk: req.body.kode_dokter_perujuk, clinic_id:user.clinic_id },
    });
    if (checkDokterperujukkode) {
      return ResponseHelper(res, 409, 'kode_dokter_perujuk Sudah Terdaftar', [
        { message: 'kode_dokter_perujuk Sudah Terdaftar', param: 'kode_dokter_perujuk' },
      ]);
    }

    const perujuk = await createDokterperujuk({ ...req.body });
    await updateDokterperujuk({ clinic_id: user.clinic_id }, { where: { id: perujuk.id } });
    return ResponseHelper(res, 201, 'success create new perujuk', perujuk);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed create new perujuk', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check perujuk is exist
    let checkDokterperujuk = await findDokterperujukById(id);
    if (!checkDokterperujuk) {
      return ResponseHelper(res, 409, 'Dokter Perujuk Tidak Tersedia', [
        { message: 'Dokter Perujuk Tidak Tersedia', param: 'id' },
      ]);
    }


    if (req.body.kode_dokter_perujuk == null ||req.body.kode_dokter_perujuk == '' || req.body.kode_dokter_perujuk == '-') {
      return ResponseHelper(res, 422, 'Kode Perujuk Harus Diisi', [
        { message: 'Kode Perujuk Harus Diisi', param: 'kode_dokter_perujuk' },
      ]);
    }

    // Check nama_dokter_perujuk is exist
    let checkDokterperujuknama = await findOneDokterperujuk({
      where: { nama_dokter_perujuk: req.body.nama_dokter_perujuk, clinic_id:user.clinic_id, id: { [Op.not]: id } },
    });
    if (checkDokterperujuknama) {
      return ResponseHelper(res, 409, 'nama_dokter_perujuk Sudah Terdaftar', [
        { message: 'nama_dokter_perujuk Sudah Terdaftar', param: 'nama_dokter_perujuk' },
      ]);
    }

    // Check kode_dokter_perujuk is exist
    let checkDokterperujukkode = await findOneDokterperujuk({
      where: { kode_dokter_perujuk: req.body.kode_dokter_perujuk, clinic_id:user.clinic_id, id: { [Op.not]: id } },
    });
    if (checkDokterperujukkode) {
      return ResponseHelper(res, 409, 'Kode Dokter Perujuk Sudah Terdaftar', [
        { message: 'Kode Dokter Perujuk Sudah Terdaftar', param: 'kode_dokter_perujuk' },
      ]);
    }

    // Update perujuk
    await updateDokterperujuk({ ...req.body }, { where: { id } });

    const result = await findDokterperujukById(id);

    return ResponseHelper(res, 201, 'Data Dokter Perujuk Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Perujuk Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check Dokter Perujuk Exist Or Not
    let checkDokterperujuk = await findDokterperujukById(id);
    if (!checkDokterperujuk) {
      return ResponseHelper(res, 409, 'Dokter Perujuk Tidak Tersedia', [
        { message: 'Dokter Perujuk Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete perujuk
    let users = await findUserById(userId);

    await updateDokterperujuk({ deletedBy: users }, { where: { id } });

    await deleteDokterperujuk(id);

    return ResponseHelper(res, 201, 'Data Dokter Perujuk Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Perujuk Gagal Dihapus', error.message);
  }
};

const upload = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    // console.log(req.files.import);

    // const mantap = req.files.fileName
    // const mantatap = mantap
    // console.log(mantatap.toString('utf8'));

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // console.log(path.dirname(__filename)+"\\myFile.xlsx");
    const workSheetsFromFile = xlsx.parse(req.files.import.data);
    // console.log(workSheetsFromFile[0].data);
    var arr = [];
    var tip = [];
    workSheetsFromFile.forEach((row) => {
      // console.log(row);
      row.data.forEach(async (data, index) => {
        // console.log(index);
        // console.log(data);
        let object = {};
        if (index > 8) {
          row.data[8].forEach(async (header, index) => {
            object[header.replace('*','')] = data[index];
          });
          // console.log('object', object);
          if (object.nama && object.tipe && object.kode) {
            arr.push(object);
          }
        }
      });
    });
    for (let i = 0; i < arr.length; i++) {
      // const cek = await findOneDokterperujuktipe({ where: { nama_tipe: arr[i].tipe , clinic_id:user.clinic_id} });
      const ceknip = await findOneDokterperujuk({ where: { kode_dokter_perujuk: arr[i].kode , clinic_id:user.clinic_id} });
      // if (!cek) {
      //   return ResponseHelper(
      //     res,
      //     409,
      //     'Error: baris ke ' +
      //       (i + 2) +
      //       ', nama tipe ' +
      //       arr[i].tipe +
      //       ' tidak ditemukan',
      //     arr[i].tipe
      //   );
      // }
      // tip.push(cek.id);
      if (ceknip) {
        const edit = await updateDokterperujuk({nama_dokter_perujuk: arr[i].nama,
          kode_dokter_perujuk: arr[i].kode},{where:{
            id:ceknip.id, clinic_id:user.clinic_id
          }})
        // return ResponseHelper(
        //   res,
        //   409,
        //   'Error: baris ke ' + (i + 2) + ', nip ' + arr[i].nip + ' sudah terdaftar',
        //   arr[i].nip
        // );
      }else{
        const tes = await createDokterperujuk({
          nama_dokter_perujuk: arr[i].nama,
          kode_dokter_perujuk: arr[i].kode,
          clinic_id: user.clinic_id,
        });
      }
    }
    // for (let i = 0; i < arr.length; i++) {
    // }
    return ResponseHelper(res, 201, 'Data Dokter Perujuk Berhasil Diimport');
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Dokter Perujuk Gagal Diimport', error.message);
  }
};

export { get, find, getperujukid, getperujuk, get1, getno, getid, add, update, deleted, upload };
