import { get, get1, add, update, deleted, find, upload, getno, getid, getperujuk, getperujukid } from './dokterperujuk_controller.js';
import validator from './dokterperujuk_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const DokterperujukRoutes = (app, prefix) => {
  app.route(`${prefix}/dokterperujuk`).get(AuthMiddleware, get);
  app.route(`${prefix}/dokterperujuk/import`).post(AuthMiddleware, upload);
  // app.route(`${prefix}/dokterperujuknotokenfilter`).get( get1);
  app.route(`${prefix}/dokterperujuknotoken`).get( getno);
  app.route(`${prefix}/dokterperujukidnotoken/:id`).get( getid);
  app.route(`${prefix}/dokterperujuknotokenkode/:id/:clinic_id`).get( find);
  app.route(`${prefix}/dokterperujuk/kodeperujuk/:id`).get(AuthMiddleware, getperujuk);
  app.route(`${prefix}/dokterperujuk/perujuk/:id`).get( getperujukid);
  app.route(`${prefix}/dokterperujuk/add`).post(AuthMiddleware, validator('dokterperujuk'), add);
  app.route(`${prefix}/dokterperujuk/update/:id`).patch(AuthMiddleware, validator('dokterperujuk'), update);
  app.route(`${prefix}/dokterperujuk/delete/:id`).delete(AuthMiddleware, deleted);
};

export { DokterperujukRoutes };
