import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'dokterperujuk': {
      return [
        body('nama_dokter_perujuk').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('kode_dokter_perujuk').not().isEmpty().withMessage('Kode Harus Diisi'),
        body('id_perujuk').not().isEmpty().withMessage('Perujuk Harus Diisi'),
        // body('creator_id').not().isEmpty().withMessage('creator id can not be empty'),
        // body('clinic_id').not().isEmpty().withMessage('clinic id can not be empty'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
