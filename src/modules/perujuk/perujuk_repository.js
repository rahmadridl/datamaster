import db from '../../database/models/index.js';
const { Perujuk } = db;
import { Op } from 'sequelize';

// Find one perujuk by id
const findPerujukById = async (id) => {
  try {
    let result = await Perujuk.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findPerujukById', error);
    throw new Error(error);
  }
};

// Find one perujuk by filter
const findOnePerujuk = async (filter) => {
  try {
    let result = await Perujuk.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOnePerujuk', error);
    throw new Error(error);
  }
};

// Find list perujuk
const findListPerujuk = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Perujuk.findAll({
        where: {
          nama_perujuk: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Perujuk.findAndCountAll({
        where: {
          nama_perujuk: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPerujuk', error);
    throw new Error(error);
  }
};
// Find list perujuk
const findListPerujuknotoken = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Perujuk.findAll({
        where: {
          nama_perujuk: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Perujuk.findAndCountAll({
        where: {
          nama_perujuk: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          // clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPerujuk', error);
    throw new Error(error);
  }
};

// Find list perujuk no token
const findListPerujuknotokenfilter = async ({ search, status, tipe }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Perujuk.findAll({
        where: {
          nama_perujuk: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          tipe: tipe,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Perujuk.findAndCountAll({
        where: {
          nama_perujuk: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
          clinic_id: status,
          tipe: tipe
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPerujuk', error);
    throw new Error(error);
  }
};

// Create new perujuk
const createPerujuk = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Perujuk.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createPerujuk', error);
    throw new Error(error);
  }
};

// Update perujuk
const updatePerujuk = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Perujuk.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updatePerujuk', error);
    throw new Error(error);
  }
};

//delete perujuk
const deletePerujuk = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Perujuk.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deletePerujuk', error);
    throw new Error(error);
  }
};

export {
  findPerujukById,
  findOnePerujuk,
  findListPerujuk,
  createPerujuk,
  updatePerujuk,
  deletePerujuk,
  findListPerujuknotoken,
  findListPerujuknotokenfilter
};
