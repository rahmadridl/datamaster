import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import xlsx from 'node-xlsx';
import { Op } from 'sequelize';
import {
  createPerujuk,
  findListPerujuk,
  findOnePerujuk,
  findPerujukById,
  updatePerujuk,
  deletePerujuk,
  findListPerujuknotoken,
  findListPerujuknotokenfilter,
} from './perujuk_repository.js';
import { findOnePerujuktipe } from '../perujuktipe/perujuktipe_repository.js';
import { findOneClinic } from '../clinic/clinic_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let perujuk = await findListPerujuk(requirement, page, limit);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(perujuk.count / limit),
      total_data: perujuk.count,
    };

    return ResponseHelper(
      res,
      200,
      'Data Perujuk Berhasil Ditampilkan',
      limit == 'all' ? perujuk : perujuk.rows,
      meta
    );
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Perujuk Gagal Ditampilkan', error.message);
  }
};

const find = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    // const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    // if (status) requirement.status = status;

    // let perujuk = await findListPerujuk(requirement, page, limit);
    let perujuk = await findOnePerujuk({kode_perujuk:id})

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(perujuk.count / limit),
      total_data: perujuk.count,
    };

    return ResponseHelper(
      res,
      200,
      'Data Perujuk Berhasil Ditampilkan',
      perujuk,
    );
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Perujuk Gagal Ditampilkan', error.message);
  }
};

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    
    const search = req.query.search || '';
    let status = req.query.status || 'RS10';
    let tipe = req.query.tipe || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';
    const clinic = await findOneClinic({where:{clinic_code:status}})
    status = clinic.id
    const tipeperujuk = await findOnePerujuktipe({where:{nama_tipe:tipe}})
    tipe = tipeperujuk.id
    
    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;
    if (tipe) requirement.tipe = tipe;
    
    let perujuk = await findListPerujuknotokenfilter(requirement, page, limit);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(perujuk.count / limit),
      total_data: perujuk.count,
    };

    return ResponseHelper(res, 200, 'Data Perujuk Berhasil Ditampilkan', perujuk, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Perujuk Gagal Ditampilkan', error.message);
  }
};

const getno = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    
    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';
    
    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;
    
    let perujuk = await findListPerujuknotoken(requirement, page, limit);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(perujuk.count / limit),
      total_data: perujuk.count,
    };

    return ResponseHelper(res, 200, 'Data Perujuk Berhasil Ditampilkan', perujuk, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Perujuk Gagal Ditampilkan', error.message);
  }
};

const getid = async (req, res) => {
  try {
    const { user_id } = req.app.locals;
    const { id } = req.params;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    
    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';
    
    let requirement = {};
    if (id) requirement.id = id;
    if (status) requirement.status = status;
    
    let perujuk = await findPerujukById(id)
    if (perujuk == null) {
      return ResponseHelper(res, 409, 'Data Perujuk Tidak Tersedia', [
        { message: 'Data Perujuk Tidak Tersedia', param: 'id' },
      ]);
    }
    // let perujuk = await findListPerujuknotoken(requirement, page, limit);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(perujuk.count / limit),
      total_data: perujuk.count,
    };

    return ResponseHelper(res, 200, 'Data Perujuk Berhasil Ditampilkan', perujuk, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Perujuk Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    if (req.body.kode_perujuk == null || req.body.kode_perujuk == '' || req.body.kode_perujuk == '-') {
      return ResponseHelper(res, 422, 'Kode Perujuk Harus Diisi', [
        { message: 'Kode Perujuk Harus Diisi', param: 'kode_perujuk' },
      ]);
    }
    if (req.body.tipe == null || req.body.tipe == '' || req.body.tipe == '-') {
      return ResponseHelper(res, 422, 'Tipe Perujuk Harus Diisi', [
        { message: 'Tipe Perujuk Harus Diisi', param: 'tipe' },
      ]);
    }

    // Check nama_perujuk is exist
    let checkPerujuk = await findOnePerujuk({
      where: { nama_perujuk: req.body.nama_perujuk, clinic_id:user.clinic_id },
    });
    if (checkPerujuk) {
      return ResponseHelper(res, 409, 'Nama Perujuk SUdah Terdaftar', [
        { message: 'Nama Perujuk SUdah Terdaftar', param: 'nama_perujuk' },
      ]);
    }

    // Check kode_perujuk is exist
    let checkPerujukkode = await findOnePerujuk({
      where: { kode_perujuk: req.body.kode_perujuk, clinic_id:user.clinic_id },
    });
    if (checkPerujukkode) {
      return ResponseHelper(res, 409, 'Kode Perujuk SUdah Terdaftar', [
        { message: 'Kode Perujuk SUdah Terdaftar', param: 'kode_perujuk' },
      ]);
    }

    const perujuk = await createPerujuk({ ...req.body });
    await updatePerujuk({ clinic_id: user.clinic_id }, { where: { id: perujuk.id } });
    return ResponseHelper(res, 201, 'Data Perujuk Berhasil Ditambahkan', perujuk);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Perujuk Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Check perujuk is exist
    let checkPerujuk = await findPerujukById(id);
    if (!checkPerujuk) {
      return ResponseHelper(res, 409, 'Perujuk Tidak Tersedia', [
        { message: 'Perujuk Tidak Tersedia', param: 'id' },
      ]);
    }


    if (req.body.kode_perujuk == null ||req.body.kode_perujuk == '' || req.body.kode_perujuk == '-') {
      return ResponseHelper(res, 422, 'Kode Perujuk Harus Diisi', [
        { message: 'Kode Perujuk Harus Diisi', param: 'kode_perujuk' },
      ]);
    }
    if (req.body.tipe == null || req.body.tipe == '' || req.body.tipe == '-') {
      return ResponseHelper(res, 422, 'Tipe Perujuk Harus Diisi', [
        { message: 'Tipe Perujuk Harus Diisi', param: 'tipe' },
      ]);
    }

    // Check nama_perujuk is exist
    let checkPerujuknama = await findOnePerujuk({
      where: { nama_perujuk: req.body.nama_perujuk, clinic_id:user.clinic_id, id: { [Op.not]: id } },
    });
    if (checkPerujuknama) {
      return ResponseHelper(res, 409, 'Nama Perujuk SUdah Terdaftar', [
        { message: 'Nama Perujuk SUdah Terdaftar', param: 'nama_perujuk' },
      ]);
    }

    // Check kode_perujuk is exist
    let checkPerujukkode = await findOnePerujuk({
      where: { kode_perujuk: req.body.kode_perujuk, clinic_id:user.clinic_id, id: { [Op.not]: id } },
    });
    if (checkPerujukkode) {
      return ResponseHelper(res, 409, 'Kode Perujuk SUdah Terdaftar', [
        { message: 'Kode Perujuk SUdah Terdaftar', param: 'kode_perujuk' },
      ]);
    }

    // Update perujuk
    await updatePerujuk({ ...req.body }, { where: { id } });

    const result = await findPerujukById(id);

    return ResponseHelper(res, 201, 'Data Perujuk Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Perujuk Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check Perujuk Exist Or Not
    let checkPerujuk = await findPerujukById(id);
    if (!checkPerujuk) {
      return ResponseHelper(res, 409, 'Perujuk Tidak Tersedia', [
        { message: 'Perujuk Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete perujuk
    let users = await findUserById(userId);

    await updatePerujuk({ deletedBy: users }, { where: { id } });

    await deletePerujuk(id);

    return ResponseHelper(res, 201, 'Data Perujuk Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Perujuk Gagal Dihapus', error.message);
  }
};

const upload = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    // console.log(req.files.import);

    // const mantap = req.files.fileName
    // const mantatap = mantap
    // console.log(mantatap.toString('utf8'));

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // console.log(path.dirname(__filename)+"\\myFile.xlsx");
    const workSheetsFromFile = xlsx.parse(req.files.import.data);
    // console.log(workSheetsFromFile[0].data);
    var arr = [];
    var tip = [];
    workSheetsFromFile.forEach((row) => {
      // console.log(row);
      row.data.forEach(async (data, index) => {
        // console.log(index);
        // console.log(data);
        let object = {};
        if (index > 8) {
          row.data[8].forEach(async (header, index) => {
            object[header.replace('*','')] = data[index];
          });
          // console.log('object', object);
          if (object.nama && object.tipe && object.kode) {
            arr.push(object);
          }
        }
      });
    });
    for (let i = 0; i < arr.length; i++) {
      const cek = await findOnePerujuktipe({ where: { nama_tipe: arr[i].tipe , clinic_id:user.clinic_id} });
      const ceknip = await findOnePerujuk({ where: { kode_perujuk: arr[i].kode , clinic_id:user.clinic_id} });
      if (!cek) {
        return ResponseHelper(
          res,
          409,
          'Error: baris ke ' +
            (i + 2) +
            ', nama tipe ' +
            arr[i].tipe +
            ' tidak ditemukan',
          arr[i].tipe
        );
      }
      tip.push(cek.id);
      if (ceknip) {
        const edit = await updatePerujuk({nama_perujuk: arr[i].nama,
          kode_perujuk: arr[i].kode,
          tipe: tip[i]},{where:{
            id:ceknip.id, clinic_id:user.clinic_id
          }})
        // return ResponseHelper(
        //   res,
        //   409,
        //   'Error: baris ke ' + (i + 2) + ', nip ' + arr[i].nip + ' sudah terdaftar',
        //   arr[i].nip
        // );
      }else{
        const tes = await createPerujuk({
          nama_perujuk: arr[i].nama,
          kode_perujuk: arr[i].kode,
          tipe: tip[i],
          clinic_id: user.clinic_id,
        });
      }
    }
    // for (let i = 0; i < arr.length; i++) {
    // }
    return ResponseHelper(res, 201, 'Data Perujuk Berhasil Diimport');
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Perujuk Gagal Diimport', error.message);
  }
};

export { get, find, get1, getno, getid, add, update, deleted, upload };
