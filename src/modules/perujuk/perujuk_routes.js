import { get, get1, add, update, deleted, find, upload, getno, getid } from './perujuk_controller.js';
import validator from './perujuk_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const PerujukRoutes = (app, prefix) => {
  app.route(`${prefix}/perujuk`).get(AuthMiddleware, get);
  app.route(`${prefix}/perujuk/import`).post(AuthMiddleware, upload);
  app.route(`${prefix}/perujuknotokenfilter`).get( get1);
  app.route(`${prefix}/perujuknotoken`).get( getno);
  app.route(`${prefix}/perujukidnotoken/:id`).get( getid);
  app.route(`${prefix}/perujuknotokenkode/:id`).get( find);
  app.route(`${prefix}/perujuk/add`).post(AuthMiddleware, validator('perujuk'), add);
  app.route(`${prefix}/perujuk/update/:id`).patch(AuthMiddleware, validator('perujuk'), update);
  app.route(`${prefix}/perujuk/delete/:id`).delete(AuthMiddleware, deleted);
};

export { PerujukRoutes };
