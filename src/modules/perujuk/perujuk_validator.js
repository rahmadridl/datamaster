import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'perujuk': {
      return [
        body('tipe').not().isEmpty().withMessage('Tipe Perujuk Harus Diisi'),
        body('nama_perujuk').not().isEmpty().withMessage('Nama Harus Diisi'),
        body('kode_perujuk').not().isEmpty().withMessage('Kode Harus Diisi'),
        // body('creator_id').not().isEmpty().withMessage('creator id Harus Diisi'),
        // body('clinic_id').not().isEmpty().withMessage('clinic id Harus Diisi'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
