import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findOneDokter } from '../dokter/dokter_repository.js';
import { findUserById } from '../user/user_repository.js';
import xlsx from 'node-xlsx';
import {
  createJadwaldokter,
  findListJadwaldokter,
  findJadwaldokterById,
  updateJadwaldokter,
  deleteJadwaldokter,
} from './jadwaldokter_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) > 3) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id;
    let page = parseInt(req.query.page || '1');
    let limit = req.query.limit || '10';

    let requirement = {};
    if (search) requirement.search = search;
    if (status || status == 0) requirement.status = status;

    let jadwaldokter = await findListJadwaldokter(requirement, page, limit);

    const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(jadwaldokter.count / limit),
      total_data: jadwaldokter.count,
    };

    return ResponseHelper(
      res,
      200,
      'Data Jadwal Dokter Berhasil Ditampilkan',
      limit == 'all' ? jadwaldokter : jadwaldokter.rows,
      meta
    );
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Dokter Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const jadwaldokter = await createJadwaldokter({ ...req.body });
    await updateJadwaldokter({ clinic_id: user.clinic_id }, { where: { id: jadwaldokter.id } });
    return ResponseHelper(res, 201, 'Data Jadwal Dokter Berhasil Ditambahkan', jadwaldokter);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Dokter Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check jadwaldokter is exist
    let checkJadwaldokter = await findJadwaldokterById(id);
    if (!checkJadwaldokter) {
      return ResponseHelper(res, 409, 'Jadwal Dokter Tidak Tersedia', [
        { message: 'Jadwal Dokter Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) > 2) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Update jadwaldokter
    await updateJadwaldokter({ ...req.body }, { where: { id } });

    const result = await findJadwaldokterById(id);

    return ResponseHelper(res, 201, 'Data Jadwal Dokter Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Dokter Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check jadwal dokter Exist Or Not
    let checkJadwaldokter = await findJadwaldokterById(id);
    if (!checkJadwaldokter) {
      return ResponseHelper(res, 409, 'Jadwaldokter Tidak Tersedia', [
        { message: 'Jadwaldokter Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete jadwaldokter
    let users = await findUserById(userId);

    await updateJadwaldokter({ deletedBy: users }, { where: { id } });

    await deleteJadwaldokter(id);

    return ResponseHelper(res, 201, 'Data Jadwal Dokter Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Dokter Gagal Dihapus', error.message);
  }
};

const upload = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;
    // console.log(req.files.import);

    // const mantap = req.files.fileName
    // const mantatap = mantap
    // console.log(mantatap.toString('utf8'));

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // console.log(path.dirname(__filename)+"\\myFile.xlsx");
    const workSheetsFromFile = xlsx.parse(req.files.import.data);
    // console.log(workSheetsFromFile[0].data);
    var arr = [];
    var dokter = [];
    workSheetsFromFile.forEach((row) => {
      // console.log(row);
      row.data.forEach(async (data, index) => {
        // console.log(index);
        // console.log(data);
        let object = {};
        if (index > 8) {
          row.data[8].forEach(async (header, index) => {
            object[header.replace('*','')] = data[index];
          });
          // console.log('object', object);
          if (object.nama && object.hari && object.dari_jam && object.sampai_jam) {
            arr.push(object);
          }
        }
      });
    });
    var hari = ['senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu']
    var arrhari = []
    for (let i = 0; i < arr.length; i++) {
      for (let j = 0; j < hari.length; j++) {
        if (arr[i].hari == hari[j]) {
          arrhari.push(j+1)
        }
      }
      const cek = await findOneDokter({ where: { nama_dokter: arr[i].nama , clinic_id:user.clinic_id} });
      // const ceknip = await findOneDokter({ where: { nip: arr[i].nip.toString() } });
      if (!cek) {
        return ResponseHelper(
          res,
          409,
          'Error: baris ke ' + (i + 2) + ', nama dokter ' + arr[i].nama + ' tidak ditemukan',
          arr[i].nama
        );
      }
      dokter.push(cek.id);
      // if (ceknip) {
      //   const edit = await updateDokter({nama_dokter: arr[i].nama,
      //     nip: arr[i].nip,
      //     dokter: dokter[i],
      //     position: arr[i].jabatan},{where:{
      //     id:ceknip.id
      //     }})
      // return ResponseHelper(
      //   res,
      //   409,
      //   'Error: baris ke ' + (i + 2) + ', nip ' + arr[i].nip + ' sudah terdaftar',
      //   arr[i].nip
      // );
      // }

      const tes = await createJadwaldokter({
        dokter: dokter[i],
        hari: arrhari[i],
        dari_jam: arr[i].dari_jam,
        sampai_jam: arr[i].sampai_jam,
        clinic_id: user.clinic_id,
      });
    }
    // for (let i = 0; i < arr.length; i++) {
    // }
    return ResponseHelper(res, 201, 'Data Jadwal Dokter Berhasil Diimport');
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Jadwal Dokter Gagal Diimport', error.message);
  }
};

export { get, add, update, deleted, upload };
