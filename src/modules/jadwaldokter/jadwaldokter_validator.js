import { body } from 'express-validator';

const validate = (method) => {
  switch (method) {
    case 'jadwaldokter': {
      return [
        body('dokter').not().isEmpty().withMessage('Dokter Harus Diisi'),
        body('hari').not().isEmpty().withMessage('Hari Harus Diisi'),
        body('dari_jam').not().isEmpty().withMessage('Waktu Mulai Harus Diisi'),
        body('sampai_jam').not().isEmpty().withMessage('Waktu Selesai Harus Diisi'),
        // body('creator_id').not().isEmpty().withMessage('creator id can not be empty'),
      ];
    }
    default:
      return [];
  }
};

export default validate;
