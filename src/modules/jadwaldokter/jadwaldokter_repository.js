import db from '../../database/models/index.js';
const { Jadwaldokter } = db;
import { Op } from 'sequelize';

// Find one jadwaldokter by id
const findJadwaldokterById = async (id) => {
  try {
    let result = await Jadwaldokter.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findJadwaldokterById', error);
    throw new Error(error);
  }
};

// Find one jadwaldokter by filter
const findOneJadwaldokter = async (filter) => {
  try {
    let result = await Jadwaldokter.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneJadwaldokter', error);
    throw new Error(error);
  }
};

// Find list jadwaldokter
const findListJadwaldokter = async ({ search, status }, page, limit) => {
  try {
    let result;
    if (limit == 'all') {
      result = await Jadwaldokter.findAll({
        where: {
          // dokter: search,
          clinic_id: status,
          status: true,
        },
        order: [['id', 'DESC']],
      });
    } else {
      limit = parseInt(limit);
      result = await Jadwaldokter.findAndCountAll({
        where: {
          // dokter: search ,
          clinic_id: status,
        },
        order: [['id', 'DESC']],
        offset: limit * (page - 1),
        limit: limit,
      });
    }
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListJadwaldokter', error);
    throw new Error(error);
  }
};

// Create new jadwaldokter
const createJadwaldokter = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Jadwaldokter.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createJadwaldokter', error);
    throw new Error(error);
  }
};

// Update jadwaldokter
const updateJadwaldokter = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Jadwaldokter.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateJadwaldokter', error);
    throw new Error(error);
  }
};

//delete jadwaldokter
const deleteJadwaldokter = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Jadwaldokter.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteJadwaldokter', error);
    throw new Error(error);
  }
};

export {
  findJadwaldokterById,
  findOneJadwaldokter,
  findListJadwaldokter,
  createJadwaldokter,
  updateJadwaldokter,
  deleteJadwaldokter,
};
