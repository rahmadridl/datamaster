import { get, add, update, deleted, upload } from './jadwaldokter_controller.js';
import validator from './jadwaldokter_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const JadwaldokterRoutes = (app, prefix) => {
  app.route(`${prefix}/jadwaldokter`).get(AuthMiddleware, get);
  app.route(`${prefix}/jadwaldokter/import`).post(AuthMiddleware, upload);
  app.route(`${prefix}/jadwaldokter/add`).post(AuthMiddleware, validator('jadwaldokter'), add);
  app.route(`${prefix}/jadwaldokter/update/:id`).patch(AuthMiddleware, validator('jadwaldokter'), update);
  app.route(`${prefix}/jadwaldokter/delete/:id`).delete(AuthMiddleware, deleted);
};

export { JadwaldokterRoutes };
