import db from '../../database/models/index.js';
const { Paketpemeriksaan } = db;
import { Op } from 'sequelize';

// Find one paketpemeriksaan by id
const findPaketpemeriksaanById = async (id) => {
  try {
    let result = await Paketpemeriksaan.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findPaketpemeriksaanById', error);
    throw new Error(error);
  }
};

// Find one paketpemeriksaan by filter
const findOnePaketpemeriksaan = async (filter) => {
  try {
    let result = await Paketpemeriksaan.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOnePaketpemeriksaan', error);
    throw new Error(error);
  }
};

// Find list paketpemeriksaan
const findListPaketpemeriksaan = async (id) => {
  try {
    let result = await Paketpemeriksaan.findAll({
      where: {
        m_jenis_pemeriksaan_id: id ,
        // clinic_id: status 
      },order: [['id', 'DESC']],
      // offset: limit * (page - 1),
      // limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPaketpemeriksaan', error);
    throw new Error(error);
  }
};

// Find list paketpemeriksaan no token
const findListPaketpemeriksaannotoken = async ({ search, status }, page, limit) => {
  try {
    let result = await Paketpemeriksaan.findAndCountAll({
      where: {
        nama_paketpemeriksaan: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        // clinic_id: status 
      },order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListPaketpemeriksaan', error);
    throw new Error(error);
  }
};

// Create new paketpemeriksaan
const createPaketpemeriksaan = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Paketpemeriksaan.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createPaketpemeriksaan', error);
    throw new Error(error);
  }
};

// Update paketpemeriksaan
const updatePaketpemeriksaan = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Paketpemeriksaan.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updatePaketpemeriksaan', error);
    throw new Error(error);
  }
};

//delete paketpemeriksaan
const deletePaketpemeriksaan = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Paketpemeriksaan.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deletePaketpemeriksaan', error);
    throw new Error(error);
  }
};

export { findPaketpemeriksaanById, findOnePaketpemeriksaan, findListPaketpemeriksaan, createPaketpemeriksaan, updatePaketpemeriksaan, deletePaketpemeriksaan, findListPaketpemeriksaannotoken };
