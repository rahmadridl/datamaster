import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import { Op } from 'sequelize';
import {
  createPaketpemeriksaan,
  findListPaketpemeriksaan,
  findOnePaketpemeriksaan,
  findPaketpemeriksaanById,
  updatePaketpemeriksaan,
  deletePaketpemeriksaan,
  findListPaketpemeriksaannotoken
} from './paketpemeriksaan_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let paketpemeriksaan = await findListPaketpemeriksaan(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(paketpemeriksaan.count / limit),
      total_data: paketpemeriksaan.count,
    };

    return ResponseHelper(res, 200, 'Data Paket Pemeriksaan Berhasil Ditampilkan', paketpemeriksaan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Paket Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const get1 = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const search = req.query.search || '';
    const status = req.query.status || '';
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let paketpemeriksaan = await findListPaketpemeriksaannotoken(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(paketpemeriksaan.count / limit),
      total_data: paketpemeriksaan.count,
    };

    return ResponseHelper(res, 200, 'Data Paket Pemeriksaan Berhasil Ditampilkan', paketpemeriksaan.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Paket Pemeriksaan Gagal Ditampilkan', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    // if (Number(user.role) !== 1) {
    //   return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
    //     { message: 'Anda Tidak Memiliki Akses', param: 'id' },
    //   ]);
    // }

    const paketpemeriksaan = await createPaketpemeriksaan({ ...req.body });
    await updatePaketpemeriksaan(
      {clinic_id:user.clinic_id},
      {where: {id: paketpemeriksaan.id}}
    )
    return ResponseHelper(res, 201, 'Data Paket Pemeriksaan Berhasil Ditambahkan', paketpemeriksaan);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Paket Pemeriksaan Gagal Ditambahkan', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check paketpemeriksaan is exist
    let checkPaketpemeriksaan = await findPaketpemeriksaanById(id);
    if (!checkPaketpemeriksaan) {
      return ResponseHelper(res, 409, 'Data Paket Pemeriksaan Tidak Tersedia', [
        { message: 'Data Paket Pemeriksaan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Update paketpemeriksaan
    await updatePaketpemeriksaan({ ...req.body }, { where: { id } });

    const result = await findPaketpemeriksaanById(id);

    return ResponseHelper(res, 201, 'Data Paket Pemeriksaan Berhasil Diubah', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Paket Pemeriksaan Gagal Diubah', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check Paketpemeriksaan Exist Or Not
    let checkPaketpemeriksaan = await findPaketpemeriksaanById(id);
    if (!checkPaketpemeriksaan) {
      return ResponseHelper(res, 409, 'Data Paket Pemeriksaan Tidak Tersedia', [
        { message: 'Data Paket Pemeriksaan Tidak Tersedia', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'Anda Tidak Memiliki Akses', [
        { message: 'Anda Tidak Memiliki Akses', param: 'id' },
      ]);
    }

    // Delete paketpemeriksaan
    let users = await findUserById(userId);

    await updatePaketpemeriksaan({ deletedBy: users }, { where: { id } });

    await deletePaketpemeriksaan(id);

    return ResponseHelper(res, 201, 'Data Paket Pemeriksaan Berhasil Dihapus', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Data Paket Pemeriksaan Gagal Dihapus', error.message);
  }
};

export { get, get1, add, update, deleted };
