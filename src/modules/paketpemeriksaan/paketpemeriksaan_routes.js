import { get, get1, add, update, deleted } from './paketpemeriksaan_controller.js';
import validator from './paketpemeriksaan_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const PaketpemeriksaanRoutes = (app, prefix) => {
  app.route(`${prefix}/paketpemeriksaan`).get(AuthMiddleware, get);
  app.route(`${prefix}/paketpemeriksaannotoken`).get( get1);
  app.route(`${prefix}/paketpemeriksaan/add`).post(AuthMiddleware, validator('paketpemeriksaan'), add);
  app.route(`${prefix}/paketpemeriksaan/update/:id`).patch(AuthMiddleware, validator('paketpemeriksaan'), update);
  app.route(`${prefix}/paketpemeriksaan/delete/:id`).delete(AuthMiddleware, deleted);
};

export { PaketpemeriksaanRoutes };
