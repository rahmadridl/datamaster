import { get, add, update, deleted } from './karyawanbidang_controller.js';
import validator from './karyawanbidang_validator.js';
import AuthMiddleware from '../../middlewares/authentication.js';

const KaryawanbidangRoutes = (app, prefix) => {
  app.route(`${prefix}/karyawanbidang`).get(AuthMiddleware, get);
  app.route(`${prefix}/karyawanbidang/add`).post(AuthMiddleware, validator('karyawanbidang'), add);
  app.route(`${prefix}/karyawanbidang/update/:id`).patch(AuthMiddleware, validator('karyawanbidang'), update);
  app.route(`${prefix}/karyawanbidang/delete/:id`).delete(AuthMiddleware, deleted);
};

export { KaryawanbidangRoutes };
