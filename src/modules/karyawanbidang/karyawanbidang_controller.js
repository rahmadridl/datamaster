import { validationResult } from 'express-validator';
import ResponseHelper from '../../helpers/response_helper.js';
import { findUserById } from '../user/user_repository.js';
import {
  createKaryawanbidang,
  findListKaryawanbidang,
  findKaryawanbidangById,
  updateKaryawanbidang,
  deleteKaryawanbidang
} from './karyawanbidang_repository.js';

const get = async (req, res) => {
  try {
    const { user_id } = req.app.locals;

    // Check role admin
    let user = await findUserById(user_id);
    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'not allowed to access', [
        { message: 'not allowed to access', param: 'id' },
      ]);
    }

    const search = req.query.search || '';
    const status = user.clinic_id
    let page = parseInt(req.query.page || '1');
    let limit = parseInt(req.query.limit || '10');

    let requirement = {};
    if (search) requirement.search = search;
    if (status) requirement.status = status;

    let karyawanbidang = await findListKaryawanbidang(requirement, page, limit);

        const meta = {
      limit: limit == 'all' ? limit : parseInt(limit),
      page: page,
      total_page: Math.ceil(karyawanbidang.count / limit),
      total_data: karyawanbidang.count,
    };

    return ResponseHelper(res, 200, 'success get list data karyawanbidang', karyawanbidang.rows, meta);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed get karyawanbidang', error.message);
  }
};

const add = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { user_id } = req.app.locals;

    // Check role admin
    const user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'not allowed to access', [
        { message: 'not allowed to access', param: 'id' },
      ]);
    }

    const karyawanbidang = await createKaryawanbidang({ ...req.body });
    await updateKaryawanbidang(
      {clinic_id:user.clinic_id},
      {where: {id: karyawanbidang.id}}
    )
    return ResponseHelper(res, 201, 'success create new karyawanbidang', karyawanbidang);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed create new karyawanbidang', error.message);
  }
};

const update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }

  try {
    const { id } = req.params;
    const { user_id } = req.app.locals;

    // Check karyawan bidang is exist
    let checkKaryawanbidang = await findKaryawanbidangById(id);
    if (!checkKaryawanbidang) {
      return ResponseHelper(res, 409, 'Karyawanbidang is not exist', [
        { message: 'Karyawanbidang is not exist', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'not allowed to access', [
        { message: 'not allowed to access', param: 'id' },
      ]);
    }

    // Update karyawan bidang
    await updateKaryawanbidang({ ...req.body }, { where: { id } });

    const result = await findKaryawanbidangById(id);

    return ResponseHelper(res, 201, 'success updated selected Karyawanbidang', result);
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'failed updated selected Karyawanbidang', error.message);
  }
};

const deleted = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return ResponseHelper(res, 422, 'Validation Error', errors.array());
  }
  try {
    const { id } = req.params;
    const { userId } = req.app.locals;
    const { user_id } = req.app.locals;

    // Check karyawan bidang Exist Or Not
    let checkKaryawanbidang = await findKaryawanbidangById(id);
    if (!checkKaryawanbidang) {
      return ResponseHelper(res, 409, 'Karyawanbidang Is Not Exist In Database', [
        { message: 'Karyawanbidang Is Not Exist In Database', param: 'id' },
      ]);
    }

    // Check role admin
    let user = await findUserById(user_id);

    if (Number(user.role) !== 1) {
      return ResponseHelper(res, 401, 'not allowed to access', [
        { message: 'not allowed to access', param: 'id' },
      ]);
    }

    // Delete karyawan bidang
    let users = await findUserById(userId);

    await updateKaryawanbidang({ deletedBy: users }, { where: { id } });

    await deleteKaryawanbidang(id);

    return ResponseHelper(res, 201, 'Success Deleted Selected Karyawanbidang', { id: Number(id) });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 500, 'Failed Deleted Selected Karyawanbidang', error.message);
  }
};

export { get, add, update, deleted };
