import db from '../../database/models/index.js';
const { Karyawanbidang } = db;
import { Op } from 'sequelize';

// Find one karyawan bidang by id
const findKaryawanbidangById = async (id) => {
  try {
    let result = await Karyawanbidang.findByPk(id);
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findKaryawanbidangById', error);
    throw new Error(error);
  }
};

// Find one karyawan bidang by filter
const findOneKaryawanbidang = async (filter) => {
  try {
    let result = await Karyawanbidang.findOne({
      ...filter,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findOneKaryawanbidang', error);
    throw new Error(error);
  }
};

// Find list karyawan bidang
const findListKaryawanbidang = async ({ search, status }, page, limit) => {
  try {
    let result = await Karyawanbidang.findAndCountAll({
      where: {
        nama: search ? { [Op.like]: `%${search}%` } : { [Op.like]: `%%` },
        clinic_id: status
      },order: [['id', 'DESC']],
      offset: limit * (page - 1),
      limit: limit,
    });
    return result;
  } catch (error) {
    console.error('[EXCEPTION] findListKaryawanbidang', error);
    throw new Error(error);
  }
};

// Create new karyawan bidang
const createKaryawanbidang = async (data, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Karyawanbidang.create(data, { transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] createKaryawanbidang', error);
    throw new Error(error);
  }
};

// Update karyawan bidang
const updateKaryawanbidang = async (data, filter, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Karyawanbidang.update(data, { ...filter, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] updateKaryawanbidang', error);
    throw new Error(error);
  }
};

//delete karyawan bidang
const deleteKaryawanbidang = async (productId, transaction) => {
  const t = transaction ? transaction : await db.sequelize.transaction();
  try {
    let result = await Karyawanbidang.destroy({ where: { id: productId }, transaction });
    if (!transaction) t.commit();
    return result;
  } catch (error) {
    if (!transaction) t.rollback();
    console.error('[EXCEPTION] deleteKaryawanbidang', error);
    throw new Error(error);
  }
};

export { findKaryawanbidangById, findOneKaryawanbidang, findListKaryawanbidang, createKaryawanbidang, updateKaryawanbidang, deleteKaryawanbidang };
