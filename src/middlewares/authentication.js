import jwt from 'jsonwebtoken';
import { jwtSecret } from '../config/env.js';
import ResponseHelper from '../helpers/response_helper.js';
import { getApikey, getToken } from '../helpers/authentication.js';

const authentication = async (req, res, next) => {
  try {
    // console.log(req.headers.authorization)
    const noauth = req.query.noauth;
    if (noauth) return next();

    const token = getToken(req.headers.authorization);
    const apikey = getApikey(req.headers.authorization);
    if (!token || !apikey) {
      return ResponseHelper(res, 401, "Silahkan Login Terlebih Dahulu", null);
    }
    
    jwt.verify(token, jwtSecret, (error, decoded) => {
      if (
        (error && error.name === 'JsonWebTokenError') ||
        (error && error.name === 'TokenExpiredError')
      ) {
        return ResponseHelper(res, 401, "Silahkan Melakukan Login Ulang", null);
      }
      req.app.locals.token = token;
      req.app.locals.user_id = decoded.user_id;
      req.app.locals.clinic_id = decoded.clinic_id;
      next();
    });
  } catch (error) {
    console.error(error);
    return ResponseHelper(res, 401, "Silahkan Login Terlebih Dahulu", null);
  }
};

export default authentication;
