'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Pasien extends Model {
    static associate(models) {
        Pasien.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Pasien.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      pid: {
        allowNull: true,
        unique: true,
        defaultValue: null,
        type: DataTypes.STRING(200),
        field: 'nomor_rm',
      },
      sureName: {
        allowNull: false,
        type: DataTypes.STRING(255),
        field: 'nama_lengkap',
      },
      age: {
        allowNull: true,
        type: DataTypes.STRING(100),
        field: 'umur',
      },
      married: {
        allowNull: true,
        type: DataTypes.STRING(100),
        field: 'status_nikah',
      },
      religion: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'agama',
      },
      blood: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'gol_darah',
      },
      province: {
        allowNull: true,
        type: DataTypes.STRING(200),
        field: 'provinsi',
      },
      country: {
        allowNull: true,
        type: DataTypes.STRING(100),
        field: 'kabupaten',
      },
      district: {
        allowNull: true,
        type: DataTypes.STRING(100),
        field: 'kecamatan',
      },
      gender: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'jenis_kelamin',
      },
      place_of_birth: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'tempat_lahir',
      },
      date_of_birth: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'tgl_lahir',
      },
      identity: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'identitas',
      },
      identity_number: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'no_identitas',
      },
      phone_number: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'no_hp',
      },
      telephone: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'no_telp',
      },
      address: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'alamat',
      },
      job: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'pekerjaan',
      },
      company: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'perusahaan',
      },
      biological_mother: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'ibu_kandung',
      },
      primary_insurance: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'asuransi_utama',
      },
      insurance_number: {
        allowNull: true,
        type: DataTypes.STRING(200),
        field: 'no_asuransi',
      },
      email: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'email',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      nationality: {
        allowNull: false,
        type: DataTypes.STRING(50),
        defaultValue: 'Indonesia',
        field: 'kewarganegaraan',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Pasien',
      tableName: 'm_pasien',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Pasien;
};
