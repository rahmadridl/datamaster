'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Layananpoli extends Model {
    static associate(models) {
        Layananpoli.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Layananpoli.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_layanan_poli: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_layanan_poli',
      },
      kode_layanan_poli: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'kode_layanan_poli',
      },
      harga_layanan_poli: {
        allowNull: true,
        type: DataTypes.DOUBLE,
        field: 'harga_layanan_poli',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      creator_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'creator_id',
      },
      import_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'import_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Layananpoli',
      tableName: 'm_layanan_poli',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Layananpoli;
};
