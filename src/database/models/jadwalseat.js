'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Jadwalseat extends Model {
    static associate(models) {
    }
  }
  Jadwalseat.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      dari_jam: {
        allowNull: false,
        type: DataTypes.TIME,
        field: 'dari_jam',
      },
      sampai_jam: {
        allowNull: false,
        type: DataTypes.TIME,
        field: 'sampai_jam',
      },
      status: {
        allowNull: false,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      seat_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'seat_id',
      },
      kode_seat: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'kode_seat',
      },
      clinic_code: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'clinic_code',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Jadwalseat',
      tableName: 'm_jadwalseat',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Jadwalseat;
};
