'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Jenispemeriksaan extends Model {
    static associate(models) {    }
  }
  Jenispemeriksaan.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      kode_jenis_pemeriksaan: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'kode_jenis_pemeriksaan',
      },
      nama_jenis_pemeriksaan: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'nama_jenis_pemeriksaan',
      },
      status: {
        allowNull: true,
        defaultValue:true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      tarif: {
        allowNull: true,
        type: DataTypes.DOUBLE,
        field: 'tarif',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Jenispemeriksaan',
      tableName: 'm_jenis_pemeriksaan',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Jenispemeriksaan;
};
