'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Rekanan extends Model {
    static associate(models) {
        Rekanan.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Rekanan.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_rekanan: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_rekanan',
      },
      telp: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'telp',
      },
      alamat: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'alamat',
      },
      email: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'email',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      creator_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'creator_id',
      },
      import_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'import_id',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Rekanan',
      tableName: 'm_rekanan',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Rekanan;
};
