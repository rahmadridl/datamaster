'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Diagnosis extends Model {
    static associate(models) {
      Diagnosis.belongsTo(models.Clinic, {
        foreignKey: 'clinic_id',
        as: 'clinic',
      });
    }
  }
  Diagnosis.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_diagnosis: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_diagnosis',
      },
      kode_diagnosis: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'kode_diagnosis',
      },
      deskripsi: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'deskripsi',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      creator_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'creator_id',
      },
      import_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'import_id',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Diagnosis',
      tableName: 'm_diagnosis',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Diagnosis;
};
