'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Kecamatan extends Model {
  }
  Kecamatan.init(
    {
      kode: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
        field: 'id'
      },
      nama: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama',
      },
      kabupaten_kode: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'kabupaten_id',
      },
    },
    {
      sequelize,
      modelName: 'Kecamatan',
      tableName: 'm_kecamatan',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Kecamatan;
};
