'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Ruangankelas extends Model {
    static associate(models) {
        Ruangankelas.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Ruangankelas.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_kelas: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_kelas',
      },
      status: {
        allowNull: false,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Ruangankelas',
      tableName: 'm_ruangan_kelas',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Ruangankelas;
};
