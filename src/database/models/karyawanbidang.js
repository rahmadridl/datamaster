'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Karyawanbidang extends Model {
    static associate(models) {
        Karyawanbidang.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Karyawanbidang.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Karyawanbidang',
      tableName: 'm_karyawan_bidang',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Karyawanbidang;
};
