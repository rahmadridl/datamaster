'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Ruangan extends Model {
    static associate(models) {
        Ruangan.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Ruangan.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_ruangan: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_ruangan',
      },
      kelasruangan_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'kelasruangan_id',
      },
      kategoriruangan_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'kategoriruangan_id',
      },
      nomor: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nomor',
      },
      nomor_ranjang: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nomor_ranjang',
      },
      status: {
        allowNull: true,
        type: DataTypes.BOOLEAN,
        defaultValue:true,
        field: 'status',
      },
      tarif: {
        allowNull: false,
        type: DataTypes.DOUBLE,
        field: 'tarif',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      creator_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'creator_id',
      },
      import_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'import_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Ruangan',
      tableName: 'm_ruangan',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Ruangan;
};
