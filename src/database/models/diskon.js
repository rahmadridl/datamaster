'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Diskon extends Model {
    static associate(models) {
        
    }
  }
  Diskon.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama',
      },
      kode: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'kode',
      },
      nominal: {
        allowNull: true,
        type: DataTypes.DOUBLE,
        field: 'nominal',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      jenis_pemeriksaan_id: {
        allowNull: true,
        type: DataTypes.JSON,
        field: 'jenis_pemeriksaan_id',
      },
      start: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'start',
      },
      expired: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'expired',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Diskon',
      tableName: 'm_diskon',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Diskon;
};
