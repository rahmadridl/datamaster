'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Catatan extends Model {
    static associate(models) {
    }
  }
  Catatan.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      catatan: {
        allowNull: true,
        type: DataTypes.TEXT,
        field: 'catatan',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Catatan',
      tableName: 'm_catatan',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Catatan;
};
