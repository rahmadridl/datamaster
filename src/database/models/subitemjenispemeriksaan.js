'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Subitemjenispemeriksaan extends Model {
    static associate(models) {
        Subitemjenispemeriksaan.belongsTo(models.Jenispemeriksaan, {
        foreignKey: 'jenis_id',
        as: 'jenis',},
        models.Itemjenispemeriksaan, {
        foreignKey: 'item_id',
        as: 'item',
      });
    }
  }
  Subitemjenispemeriksaan.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      jenis_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'jenis_id',
      },
      item_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'item_id',
      },
      item: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'item',
      },
      nilai_rujukan: {
        allowNull: true,
        defaultValue:true,
        type: DataTypes.STRING(50),
        field: 'nilai_rujukan',
      },
      satuan: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'satuan',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Subitemjenispemeriksaan',
      tableName: 'm_subitem_jenis_pemeriksaan',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Subitemjenispemeriksaan;
};
