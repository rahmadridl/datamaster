'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Dokterperujuk extends Model {
    // static associate(models) {
    //     Dokterperujuk.belongsTo(models.Clinic, {
    //       foreignKey: 'clinic_id',
    //       as: 'clinic',
    //     });
    // }
  }
  Dokterperujuk.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_dokter_perujuk: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_dokter_perujuk',
      },
      kode_dokter_perujuk: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'kode_dokter_perujuk',
      },
      id_perujuk: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'id_perujuk',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Dokterperujuk',
      tableName: 'm_dokter_perujuk',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Dokterperujuk;
};
