'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Karyawan extends Model {
    static associate(models) {
        Karyawan.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Karyawan.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_karyawan: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_karyawan',
      },
      kode_karyawan: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'kode_karyawan',
      },
      email: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'email',
      },
      bidang: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'bidang',
      },
      notelp: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'notelp',
      },
      alamat: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'alamat',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      creator_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'creator_id',
      },
      import_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'import_id',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Karyawan',
      tableName: 'm_karyawan',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Karyawan;
};
