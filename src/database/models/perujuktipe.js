'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Perujuktipe extends Model {
    static associate(models) {
        Perujuktipe.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Perujuktipe.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_tipe: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_tipe',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Perujuktipe',
      tableName: 'm_perujuk_tipe',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Perujuktipe;
};
