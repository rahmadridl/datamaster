'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      // User.belongsTo(models.Clinic, {
      //   foreignKey: 'clinic_id',
      //   as: 'clinic',
      // });
    }
  }
  User.init(
    {
      id: {
        allowNull: true,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      email: {
        allowNull: false,
        type: DataTypes.STRING(25),
        unique: true,
        field: 'email',
      },
      password: {
        allowNull: false,
        type: DataTypes.STRING(255),
        field: 'password',
      },
      name: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'name',
      },
      role: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValue: 2,
        field: 'role',
      },
      status: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValue: 1,
        field: 'status',
      },
      clinic_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      phone: {
        allowNull: false,
        type: DataTypes.STRING(25),
        unique: true,
        field: 'phone',
      },
      token: {
        allowNull: true,
        type: DataTypes.TEXT,
        field: 'token',
      },
      last_login: {
        allowNull: true,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'last_login',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Users',
      tableName: 'cre_users',
      underscored: true,
      freezeTableName: true,
    }
  );

  return User;
};
