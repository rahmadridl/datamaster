'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Supplier extends Model {
    static associate(models) {
        Supplier.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Supplier.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_supplier: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_supplier',
      },
      kode_supplier: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'kode_supplier',
      },
      notelp: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'notelp',
      },
      alamat: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'alamat',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      creator_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'creator_id',
      },
      import_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'import_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Supplier',
      tableName: 'm_supplier',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Supplier;
};
