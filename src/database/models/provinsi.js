'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Provinsi extends Model {
  }
  Provinsi.init(
    {
      kode: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
        field: 'id'
      },
      nama: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama',
      },
    },
    {
      sequelize,
      modelName: 'Provinsi',
      tableName: 'm_provinsi',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Provinsi;
};
