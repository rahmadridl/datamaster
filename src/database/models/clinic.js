'use strict';

const { Model } = require('sequelize');
function convertTZ(date, tzString) {
  return new Date((typeof date === "string" ? new Date(date) : date).toLocaleString("en-US", {timeZone: tzString}));   
}

module.exports = (sequelize, DataTypes) => {
  class Clinic extends Model {}
  Clinic.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      clinic_code: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'clinic_code',
      },
      clinic_name: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'clinic_name',
      },
      logo: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'logo',
      },
      remarks: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'remarks',
      },
      email: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'email',
      },
      phone: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'phone',
      },
      alamat: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'alamat',
      },
      license_type: {
        allowNull: false,
        type: DataTypes.STRING(50),
        defaultValue:'month',
        field: 'license_type',
      },
      license_duration: {
        allowNull: false,
        type: DataTypes.INTEGER,
        defaultValue:1,
        field: 'license_duration',
      },
      account_type: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'account_type',
      },
      created_by: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'created_by',
      },
      status: {
        allowNull: false,
        type: DataTypes.BOOLEAN,
        defaultValue: true,
        field: 'status',
      },
      start_license: {
        allowNull: false,
        defaultValue: convertTZ(new Date(Date.now()), "Asia/Jakarta"),
        type: DataTypes.DATE,
        field: 'start_license',
      },
      renewal_license: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'renewal_license',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Clinic',
      tableName: 'm_clinic',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Clinic;
};
