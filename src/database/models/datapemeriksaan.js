'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Datapemeriksaan extends Model {
    static associate(models) { 
        Datapemeriksaan.belongsTo(models.Clinic, {
            foreignKey: 'clinic_id',
            as: 'clinic',
        });
        Datapemeriksaan.belongsTo(models.Pasien, {
            foreignKey: 'id_pasien',
            as: 'pasien',
        });
        Datapemeriksaan.belongsTo(models.Dokter, {
            foreignKey: 'id_dokter',
            as: 'dokter',
        });
    }
  }
  Datapemeriksaan.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      no_test: {
        allowNull: true,
        type: DataTypes.DOUBLE,
        field: 'no_test',
      },
      id_pasien: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'id_pasien',
      },
      id_provider: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'id_provider',
      },
      id_dokter: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'id_dokter',
      },
      nama_pasien: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'nama_pasien',
      },
      tgl_periksa: {
        allowNull: true,
        defaultValue:true,
        type: DataTypes.DATE,
        field: 'tgl_periksa',
      },
      tgl_sampling: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.DATE,
        field: 'tgl_sampling',
      },
      keluhan: {
        allowNull: true,
        type: DataTypes.STRING(100),
        field: 'keluhan',
      },
      jenis_sample: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'jenis_sample',
      },
      jenis_pemeriksaan: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'jenis_pemeriksaan',
      },
      clinic_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      hasil: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'hasil',
      },
      masa_berlaku: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
        field: 'masa_berlaku',
      },
      masa_berlaku_opt: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'masa_berlaku_opt',
      },
      status: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'status',
      },
      id_notes: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'id_notes',
      },
      cancel_remarks: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'cancel_remarks',
      },
      import_id: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'import_id',
      },
      qr_check: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
        field: 'qr_check',
      },
      qr_checkdate: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.DATE,
        field: 'qr_checkdate',
      },
      src_check: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'src_check',
      },
      biaya: {
        allowNull: false,
        type: DataTypes.DOUBLE,
        field: 'biaya',
      },
      bayar: {
        allowNull: false,
        type: DataTypes.DOUBLE,
        field: 'bayar',
      },
      metode_bayar: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'metode_bayar',
      },
      referensi_bayar: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'referensi_bayar',
      },
      self_register: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
        field: 'self_register',
      },
      verified: {
        allowNull: false,
        defaultValue: 1,
        type: DataTypes.INTEGER,
        field: 'verified',
      },
      verified_by: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'verified_by',
      },
      verified_date: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.DATE,
        field: 'verified_date',
      },
      update_hasil_at: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.DATE,
        field: 'update_hasil_at',
      },
      plan_tgl_periksa: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.DATE,
        field: 'plan_tgl_periksa',
      },
      plan_jam_periksa: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'plan_jam_periksa',
      },
      kode_sales: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'kode_sales',
      },
      update_hasil_by: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'update_hasil_by',
      },
      created_by: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'created_by',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
      deleted: {
        allowNull: true,
        defaultValue: 0,
        type: DataTypes.INTEGER,
        field: 'deleted',
      },
      deleted_by: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'deleted_by',
      },
    },
    {
      sequelize,
      modelName: 'Datapemeriksaan',
      tableName: 'm_data_pemeriksaan',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Datapemeriksaan;
};
