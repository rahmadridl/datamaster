'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Poli extends Model {
    static associate(models) {
      Poli.belongsTo(models.Clinic, {
        foreignKey: 'clinic_id',
        as: 'clinic',
      });
    }
  }
  Poli.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      kode_poli: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'kode_poli',
      },
      nama_poli: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_poli',
      },
      label_antrian: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'label_antrian',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      creator_id: {
        allowNull: true,
        type: DataTypes.STRING(30),
        defaultValue: 1,
        field: 'creator_id',
      },
      import_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        defaultValue: 1,
        field: 'import_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Poli',
      tableName: 'm_poli',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Poli;
};
