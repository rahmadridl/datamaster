'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Biayalain extends Model {
    static associate(models) {
        
    }
  }
  Biayalain.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama',
      },
      kode: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'kode',
      },
      nominal: {
        allowNull: false,
        type: DataTypes.DOUBLE,
        field: 'nominal',
      },
      biaya_admin: {
        allowNull: false,
        type: DataTypes.DOUBLE,
        field: 'biaya_admin',
      },
      ppn: {
        allowNull: true,
        type: DataTypes.DOUBLE,
        field: 'ppn',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      jenis_pemeriksaan_id: {
        allowNull: true,
        type: DataTypes.JSON,
        field: 'jenis_pemeriksaan_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Biayalain',
      tableName: 'm_biaya_lain',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Biayalain;
};
