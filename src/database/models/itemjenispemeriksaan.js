'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Itemjenispemeriksaan extends Model {
    static associate(models) {
        
    }
  }
  Itemjenispemeriksaan.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      item: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'item',
      },
      kode: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'kode',
      },
      nilai_rujukan: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'nilai_rujukan',
      },
      satuan: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'satuan',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Itemjenispemeriksaan',
      tableName: 'm_item_jenis_pemeriksaan',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Itemjenispemeriksaan;
};
