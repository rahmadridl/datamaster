'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Jadwaldokter extends Model {
    static associate(models) {
        Jadwaldokter.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Jadwaldokter.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      dokter: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'dokter',
      },
      hari: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'hari',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      dari_jam: {
        allowNull: false,
        type: DataTypes.TIME,
        field: 'dari_jam',
      },
      sampai_jam: {
        allowNull: false,
        type: DataTypes.TIME,
        field: 'sampai_jam',
      },
      creator_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'creator_id',
      },
      import_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'import_id',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Jadwaldokter',
      tableName: 'm_jadwaldokter',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Jadwaldokter;
};
