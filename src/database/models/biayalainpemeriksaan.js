'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Biayalainpemeriksaan extends Model {
    static associate(models) {
        
    }
  }
  Biayalainpemeriksaan.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      m_biaya_lain_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'm_biaya_lain_id',
      },
      m_jenis_pemeriksaan_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'm_jenis_pemeriksaan_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Biayalainpemeriksaan',
      tableName: 'm_biaya_lain_pemeriksaan',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Biayalainpemeriksaan;
};
