'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Ruangankategori extends Model {
    static associate(models) {
        Ruangankategori.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Ruangankategori.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_kategori: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_kategori',
      },
      status: {
        allowNull: false,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Ruangankategori',
      tableName: 'm_ruangan_kategori',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Ruangankategori;
};
