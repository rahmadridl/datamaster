'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Dokter extends Model {
    static associate(models) {
        Dokter.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Dokter.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_dokter: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_dokter',
      },
      nip: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nip',
      },
      spesialisasi: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'spesialisasi',
      },
      position: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'position',
      },
      notelp: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'notelp',
      },
      idbpjs: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'idbpjs',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      creator_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'creator_id',
      },
      import_id: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'import_id',
      },
      status: {
        allowNull: true,
        type: DataTypes.BOOLEAN,
        defaultValue: true,
        field: 'status',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Dokter',
      tableName: 'm_dokter',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Dokter;
};
