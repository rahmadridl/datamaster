'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Seat extends Model {
    static associate(models) {   
        Seat.belongsTo(models.Poli, {
            foreignKey: 'poli_id',
            as: 'poli',
        });
        Seat.belongsTo(models.Clinic, {
            foreignKey: 'clinic_id',
            as: 'clinic',
        });
    }
  }
  Seat.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_seat: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_seat',
      },
      status: {
        allowNull: false,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      poli_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'poli_id',
      },
      clinic_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Seat',
      tableName: 'm_seat',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Seat;
};
