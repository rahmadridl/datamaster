'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Kabupaten extends Model {
  }
  Kabupaten.init(
    {
      kode: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
        field: 'id'
      },
      nama: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama',
      },
      provinsi_kode: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'provinsi_id',
      },
    },
    {
      sequelize,
      modelName: 'Kabupaten',
      tableName: 'm_kabupaten',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Kabupaten;
};
