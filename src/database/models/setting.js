'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Setting extends Model {
    static associate(models) {
        Setting.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Setting.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      gambar_header: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'gambar_header',
      },
      body_background: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'body_background',
      },
      qrcode: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'qrcode',
      },
      footer: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'footer',
      },
      api_key: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'api_key',
      },
      page_size: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'page_size',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Setting',
      tableName: 'm_setting',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Setting;
};
