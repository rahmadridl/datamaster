'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Asuransi extends Model {
    static associate(models) {
        Asuransi.belongsTo(models.Clinic, {
          foreignKey: 'clinic_id',
          as: 'clinic',
        });
    }
  }
  Asuransi.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      nama_asuransi: {
        allowNull: false,
        type: DataTypes.STRING(50),
        field: 'nama_asuransi',
      },
      kode_asuransi: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'kode_asuransi',
      },
      email: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'email',
      },
      telp: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'telp',
      },
      alamat: {
        allowNull: true,
        type: DataTypes.STRING(50),
        field: 'alamat',
      },
      clinic_id: {
        allowNull: true,
        type: DataTypes.INTEGER,
        field: 'clinic_id',
      },
      status: {
        allowNull: true,
        defaultValue: true,
        type: DataTypes.BOOLEAN,
        field: 'status',
      },
      created_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'created_at',
      },
      updated_at: {
        allowNull: false,
        defaultValue: new Date(Date.now()),
        type: DataTypes.DATE,
        field: 'updated_at',
      },
      deleted_at: {
        allowNull: true,
        type: DataTypes.DATE,
        field: 'deleted_at',
      },
    },
    {
      sequelize,
      modelName: 'Asuransi',
      tableName: 'm_asuransi',
      underscored: true,
      freezeTableName: true,
    }
  );

  return Asuransi;
};
