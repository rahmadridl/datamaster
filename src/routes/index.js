import { prefix } from '../helpers/version_control.js';
import { AuthRoutes } from '../modules/auth/auth_routes.js';
import { UserRoutes } from '../modules/user/user_routes.js';
import { KaryawanRoutes } from '../modules/karyawan/karyawan_routes.js';
import { KaryawanbidangRoutes } from '../modules/karyawanbidang/karyawanbidang_routes.js';
import { AsuransiRoutes } from '../modules/asuransi/asuransi_routes.js';
import { SupplierRoutes } from '../modules/supplier/supplier_routes.js';
import { ProvinsiRoutes } from '../modules/provinsi/provinsi_routes.js';
import { KabupatenRoutes } from '../modules/kabupaten/kabupaten_routes.js';
import { KecamatanRoutes } from '../modules/kecamatan/kecamatan_routes.js';
import { PoliRoutes } from '../modules/poli/poli_routes.js';
import { SeatRoutes } from '../modules/seat/seat_routes.js';
import { JadwalseatRoutes } from '../modules/jadwalseat/jadwalseat_routes.js';
import { LayananpoliRoutes } from '../modules/layananpoli/layananpoli_routes.js';
import { PasienRoutes } from '../modules/pasien/pasien_routes.js';
import { DokterRoutes } from '../modules/dokter/dokter_routes.js';
import { DiagnosisRoutes } from '../modules/diagnosis/diagnosis_routes.js';
import { ClinicRoutes } from '../modules/clinic/clinic_routes.js';
import { PerujukRoutes } from '../modules/perujuk/perujuk_routes.js';
import { JadwaldokterRoutes } from '../modules/jadwaldokter/jadwaldokter_routes.js';
import { RekananRoutes } from '../modules/rekanan/rekanan_routes.js';
import { RuanganRoutes } from '../modules/ruangan/ruangan_routes.js';
import { RuangankategoriRoutes } from '../modules/ruangankategori/ruangankategori_routes.js';
import { RuangankelasRoutes } from '../modules/ruangankelas/ruangankelas_routes.js';
import { SpesialisasiRoutes } from '../modules/spesialisasi/spesialisasi_routes.js';
import { PerujuktipeRoutes } from '../modules/perujuktipe/perujuktipe_routes.js';
import { DatapemeriksaanRoutes } from '../modules/datapemeriksaan/datapemeriksaan_routes.js';
import { JenispemeriksaanRoutes } from '../modules/jenispemeriksaan/jenispemeriksaan_routes.js';
import { ItemjenispemeriksaanRoutes } from '../modules/itemjenispemeriksaan/itemjenispemeriksaan_routes.js';
import { SubitemjenispemeriksaanRoutes } from '../modules/subitemjenispemeriksaan/subitemjenispemeriksaan_routes.js';
import { PaketpemeriksaanRoutes } from '../modules/paketpemeriksaan/paketpemeriksaan_routes.js';
import { BiayalainRoutes } from '../modules/biayalain/biayalain_routes.js';
import { DiskonRoutes } from '../modules/diskon/diskon_routes.js';
import { CatatanRoutes } from '../modules/catatan/catatan_routes.js';
import { DokterperujukRoutes } from '../modules/dokterperujuk/dokterperujuk_routes.js';

const MainRoutes = (app) => {
  // Test the connection api
  app.route(`${prefix}/hello`).get((req, res) => {
    res.send({
      message: 'Hello World',
    });
  });

  // authentication routes
  AuthRoutes(app, prefix);

  // user routes
  UserRoutes(app, prefix);
  
  // karyawan route
  KaryawanRoutes(app, prefix);

  // karyawan bidang route
  KaryawanbidangRoutes(app, prefix);

  // asuransi route
  AsuransiRoutes(app, prefix);

  // supplier route
  SupplierRoutes(app, prefix);

  // provinsi route
  ProvinsiRoutes(app, prefix);
  
  // Kabupaten route
  KabupatenRoutes(app, prefix);
  
  // Kecamatan route
  KecamatanRoutes(app, prefix);

  // poli route
  PoliRoutes(app, prefix);
  
  // Seat route
  SeatRoutes(app, prefix);
  
  // Jadwal seat route
  JadwalseatRoutes(app, prefix);
  
  // layananpoli route
  LayananpoliRoutes(app, prefix);

  // pasien route
  PasienRoutes(app, prefix);

  // dokter route
  DokterRoutes(app, prefix);
 
  // diagnosis route
  DiagnosisRoutes(app, prefix);
  
  // clinic route
  ClinicRoutes(app, prefix);
 
  // perujuk route
  PerujukRoutes(app, prefix);
  
  // jadwaldokter route
  JadwaldokterRoutes(app, prefix);
  
  // rekanan route
  RekananRoutes(app, prefix);
  
  // ruangan route
  RuanganRoutes(app, prefix);
  
  // ruangan kategori route
  RuangankategoriRoutes(app, prefix);
 
  // ruangan kelas route
  RuangankelasRoutes(app, prefix);
  
  // spesialisasi route
  SpesialisasiRoutes(app, prefix);
 
  // perujuk tipe route
  PerujuktipeRoutes(app, prefix);
  
  // data pemeriksaan tipe route
  DatapemeriksaanRoutes(app, prefix);

  // jenis pemeriksaan tipe route
  JenispemeriksaanRoutes(app, prefix);
  
  // item jenis pemeriksaan tipe route
  ItemjenispemeriksaanRoutes(app, prefix);
  
  // subitem jenis pemeriksaan tipe route
  SubitemjenispemeriksaanRoutes(app, prefix);
  
  // paket pemeriksaan tipe route
  PaketpemeriksaanRoutes(app, prefix);

  // Biayalain routes
  BiayalainRoutes(app, prefix);
  
  // Diskon routes
  DiskonRoutes(app, prefix);
  
  // Catatan routes
  CatatanRoutes(app, prefix);
  
  // Dokterperujuk routes
  DokterperujukRoutes(app, prefix);
};

export default MainRoutes;
